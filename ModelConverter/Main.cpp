#define DEBUG
#include <Base/Application/Binary.hpp>
#include <Base/Application/Log.hpp>
#include <Base/Application/File.hpp>
#include <Base/Application/Timer.hpp>
#include <Base/Application/DataFormat.hpp>


int main(int argc, char* argv[]) {

	Log::Init();

	if (argc < 2) {
		Log::Info("ModelConverter: Usage: modelconverter outputDir [model1.dmf] [model2.dmf] [model3.dmf]");
		return 1;
	}

	std::string outputDir(argv[1]);

	for (int i = 2; i < argc; ++i) {

		Timer time;

		std::string inputFile(argv[i]);
		std::string outputFile = outputDir + "/" + File::ReplaceExtension(File::GetFilenameWithoutPath(inputFile), ".bdm");

		if (File::Exists(outputFile) && File::IsOlderThan(inputFile, outputFile)) {
			Log::Info("ModelConverter: Skipping '%%%'", outputFile);
			continue;
		}
		
		Object model = Object::FromFile(inputFile);

		int version = model["version"].As<int>();
		if (version != 0) {
			Log::Info("ModelConverter: Model file must be version 0! Error converting '%%%'", inputFile);
		}

		std::vector<Vertex> verts = model["verts"].As<std::vector<Vertex>>();
		std::vector<Triangle> tris = model["tris"].As<std::vector<Triangle>>();

		int totalFileSize = 5 + verts.size() * sizeof(Vertex) + tris.size() * sizeof(Triangle);

		Blob binary(new uint8_t[totalFileSize], totalFileSize);
		BinaryWriter writer(&binary);

		writer << (uint8_t)0;
		writer << (uint16_t)verts.size();
		writer << (uint16_t)tris.size();

		for (auto& vert : verts) {
			writer << vert;
		}

		for (auto& tri : tris) {
			writer << tri;
		}

		File::WriteBinary(outputFile, binary);
		binary.Release();
		
		Log::Info("ModelConverter: Exported file %%%/%%% '%%%' in %%%s", i - 1, argc - 2, outputFile, time.Time());
	}

	Log::Shutdown();

	return 0;
}