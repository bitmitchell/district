
import os
from subprocess import check_output

def GetBuildNumber():
    count = check_output(["git", "rev-list", "HEAD", "--count"])
    return int(count)

if __name__ == "__main__":
    print("Build number: %i" % GetBuildNumber())
