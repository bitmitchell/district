
import os, glob
from subprocess import call
from GetBuildNumber import *

def Package():
    build = GetBuildNumber()

    archive = "District_%i.zip" % build

    deleteFiles = [
        "*.ilk", "*.pdb",
        "*.config", "Editor.vshost*",
        "perf.txt",
        "Editor*", # This is temporary until the editor starts being useful
        "Surface*"
    ]
    
    for pattern in deleteFiles:
        for file in glob.glob("bin/" + pattern):
            print(file)
            os.remove(file)
    if os.path.isfile(archive):
        os.remove(archive)
    
    call(["Tools/7z", "a", archive, "./bin/*"])


if __name__ == "__main__":
    Package()
