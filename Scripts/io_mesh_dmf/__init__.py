bl_info = {
	"name": "DMF Engine Model Exporter",
	"author": "Ben Mitchell, Liam Oldershaw",
	"blender": (2, 71, 0),
	"location": "File > Import-Export",
	"description": "Export DMF mesh data",
	"warning": "",
	"wiki_url": "",
	"category": "Import-Export"}    

VERSION = 0

import os
import bpy
from bpy.props import (CollectionProperty,
					   StringProperty,
					   BoolProperty,
					   EnumProperty,
					   FloatProperty,
					   )
from bpy_extras.io_utils import (ImportHelper,
								 ExportHelper,
								 axis_conversion,
								 )

class ExportDMF(bpy.types.Operator, ExportHelper):
	bl_idname = "export_mesh.dmf"
	bl_label = "Export DMF"

	filename_ext = ".dmf"
	filter_glob = StringProperty(default="*.dmf", options={'HIDDEN'})

	@classmethod
	def poll(cls, context):
		return context.active_object != None and 'MESH' == context.active_object.type

	def execute(self, context):
		filepath = self.filepath
		filepath = bpy.path.ensure_ext(filepath, self.filename_ext)
		return self.export(filepath, context)
		
	def export(self, filepath, c):
		obj = c.active_object
		mesh = obj.data
		
		# check for a uv layer
		if len(mesh.uv_layers) != 1:
			raise Exception("The mesh must have one (and only one) uv layer")
		
		out = open(filepath, "w")
		out.write("{\n")
		self.writeHeader(out)
		
		# here we will create a list of verts and indices for writing
		verts = []
		inds = []
		
		print("Building output lists")
		self.addFaces(mesh, verts, inds)
		
		print("Writing verts")
		out.write("verts = [%s]\n" % ",".join(map(lambda v: "\n{ pos = [%f, %f, %f] nor = [%f, %f, %f] uv = [%f, %f] }" % (v[0].x, v[0].y, v[0].z, \
																						v[1].x, v[1].y, v[1].z, \
																						v[2].x, v[2].y), verts)))

		tris = [(inds[i], inds[i + 1], inds[i + 2]) for i in range(0, len(inds), 3)]
		out.write("\ntris = [%s]\n" % ",".join(map(lambda t: "\n[%i, %i, %i]" % t, tris)))
				
		out.write("}")
		out.close()
		
		return {'FINISHED'} # blender return success tag
		
	def compareVectors(self, a, b, e = 0.001):
		return (abs(a.x - b.x) < e and \
				abs(a.y - b.y) < e and \
				abs(a.z - b.z) < e)
				
	def compareVector2s(self, a, b, e = 0.001):
		return (abs(a.x - b.x) < e and \
				abs(a.y - b.y) < e)
		
	def compareVerts(self, a, b):
		return (self.compareVectors(a[0], b[0]) and \
				self.compareVectors(a[1], b[1]) and \
				self.compareVector2s(a[2], b[2]))	# position, normal, texture
	
	def findMatchingVertex(self, verts, v):
		id = -1
		for i, c in zip(range(len(verts)), verts):
			if self.compareVerts(v, c):
				id = i
				break
		return id
		
	def addVertex(self, v, u, verts, inds, useSmooth, faceNormal):
		vert = [v.co, v.normal, u.uv]
		
		if not useSmooth:
			vert[1] = faceNormal
		
		id = self.findMatchingVertex(verts, vert)
		
		if id == -1: # no vertex found in the list, add a new one
			inds.append(len(verts))
			verts.append(vert)
		else:
			inds.append(id) # vertex was found, just add that index again
			
	def addFaces(self, m, verts, inds):
		mv = m.vertices
		uv = m.uv_layers.active.data
		
		i = 0
		c = len(m.polygons)
		for p in m.polygons:
			i += 1
			if i % 10 == 0:
				print("%0.2f%%" % (100.0 * i / c))
		
			pv = p.vertices
			pl = p.loop_indices
			
			# now add the verts for a triangle
			if len(p.vertices) == 3:
				self.addVertex(mv[pv[0]], uv[pl[0]], verts, inds, p.use_smooth, p.normal)
				self.addVertex(mv[pv[2]], uv[pl[2]], verts, inds, p.use_smooth, p.normal)
				self.addVertex(mv[pv[1]], uv[pl[1]], verts, inds, p.use_smooth, p.normal)
				
			# now handle a quad
			elif len(p.vertices) == 4:
				self.addVertex(mv[pv[0]], uv[pl[0]], verts, inds, p.use_smooth, p.normal)
				self.addVertex(mv[pv[2]], uv[pl[2]], verts, inds, p.use_smooth, p.normal)
				self.addVertex(mv[pv[1]], uv[pl[1]], verts, inds, p.use_smooth, p.normal)
				
				self.addVertex(mv[pv[0]], uv[pl[0]], verts, inds, p.use_smooth, p.normal)
				self.addVertex(mv[pv[3]], uv[pl[3]], verts, inds, p.use_smooth, p.normal)
				self.addVertex(mv[pv[2]], uv[pl[2]], verts, inds, p.use_smooth, p.normal)
			
			# raise an exception, what kind of polygon is this?
			else:
				raise Exception("Unknown polygon of %i points!" % len(p.vertices))
		
	def writeHeader(self, out):    
		out.write("version = %s\n" % VERSION)

def menu_func_export(self, context):
	self.layout.operator(ExportDMF.bl_idname, text="DMF Mesh Format (.dmf)")

def register():
	bpy.utils.register_module(__name__)
	bpy.types.INFO_MT_file_export.append(menu_func_export)

def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.INFO_MT_file_export.remove(menu_func_export)

if __name__ == "__main__":
	register()