﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor {
    public static class WeaponData {
        public class Weapon {
            public string Name { get; set; }
            public string Ammo { get; set; }
            public string Description { get; set; }
        }

        private static Dictionary<string, Weapon> weapons;

        public static Weapon Get(string name) {
            return weapons[name];
        }

        public static IEnumerable<string> Weapons {
            get { return weapons.Keys; }
        }

        public static Weapon Add(string name) {
            weapons.Add(name, new Weapon());
            return Get(name);
        }

        public static void Load(string yamlFile) {
            throw new NotImplementedException();
        }

        public static void Save(string yamlFile) {
            throw new NotImplementedException();
        }

    }
}
