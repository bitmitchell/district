﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using EditorWorld;
using System.Timers;

namespace Editor {
    public partial class MainWindow : Window {
        D3D11D3DImage d3dImage;
        World editorWorld;

        Dictionary<string, object> editingObjects = new Dictionary<string,object>();

        class WeaponEditTab {
            public WeaponData.Weapon Weapon { get; set; }
            public TabItem Tab { get; set; }
        }

        public MainWindow() {
            this.CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, this.OnCloseWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, this.OnMaximizeWindow, this.OnCanResizeWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, this.OnMinimizeWindow, this.OnCanMinimizeWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, this.OnRestoreWindow, this.OnCanResizeWindow));

            InitializeComponent();

            Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e) {
            SetupEditorWorld();

            WeaponData.Load("weapons.yml");
            foreach (string weapon in WeaponData.Weapons) {
                var item = new TreeViewItem() {
                    Header = weapon
                };

                item.MouseDoubleClick += (s, a) => {
                    OpenWeaponEditTab(weapon);
                };

                WeaponTree.Items.Add(item);
            }
        }

        void OpenWeaponEditTab(string weapon) {
            if (editingObjects.ContainsKey(weapon)) {
                (editingObjects[weapon] as WeaponEditTab).Tab.IsSelected = true;
                return;
            }

            StackPanel header = new StackPanel() {
                Orientation = Orientation.Horizontal
            };

            Button tabClose = new Button() { Style = FindResource("CrossButtonStyle") as Style };
            tabClose.Click += (s, a) => {
                WeaponEditTab tabToClose = editingObjects[weapon] as WeaponEditTab;
                TabEditor.Items.Remove(tabToClose.Tab);
                editingObjects.Remove(weapon);
            };

            header.Children.Add(new TextBlock() { Text = weapon });
            header.Children.Add(tabClose);

            TabItem tab = new TabItem() {
                Header = header,
                IsSelected = true
            };

            editingObjects.Add(weapon, new WeaponEditTab() {
                Weapon = WeaponData.Get(weapon),
                Tab = tab
            });

            TabEditor.Items.Add(tab);
        }

        void SetupEditorWorld() {
            int width = (int)GameWorld.Width;
            int height = (int)GameWorld.Height;

            d3dImage = new D3D11D3DImage();
            d3dImage.SetPixelSize(width, height);
            GameWorld.SizeChanged += GameWorld_SizeChanged;
            d3dImage.WindowOwner = (new System.Windows.Interop.WindowInteropHelper(this)).Handle;
            d3dImage.OnRender = Render;

            editorWorld = new World();

            GameWorld.Source = d3dImage;

            // setup the rendering calls
            CompositionTarget.Rendering += (s, a) => { d3dImage.RequestRender(); };
            d3dImage.RequestRender();

            Yaw.ValueChanged += (s, a) => { editorWorld.ModelYaw = MathHelper.ToRadians((float)a.NewValue); };
        }

        void GameWorld_SizeChanged(object sender, SizeChangedEventArgs e) {
            int width = (int)e.NewSize.Width;
            int height = (int)e.NewSize.Height;
            d3dImage.SetPixelSize(width, height);
        }

        private void Render(IntPtr resource) {
            d3dImage.Lock();
            editorWorld.Render(resource);
            d3dImage.Unlock();
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e) {
            e.CanExecute = this.ResizeMode == ResizeMode.CanResize || this.ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e) {
            e.CanExecute = this.ResizeMode != ResizeMode.NoResize;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e) {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e) {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e) {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e) {
            SystemCommands.RestoreWindow(this);
        }
    }
}
