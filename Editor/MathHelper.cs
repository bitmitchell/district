﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor {
    public static class MathHelper {

        public static float ToRadians(float degrees) {
            return degrees * (float)Math.PI / 180.0f;
        }


        public static float ToDegrees(float radians) {
            return radians * 180.0f / (float)Math.PI;
        }

    }
}
