#pragma once
#pragma managed(push, off)

#include <Base/Graphics/Graphics.hpp>
#include <Base/Graphics/RenderTarget2D.hpp>

#include <Base/Graphics/Buffer.hpp>
#include <Base/Graphics/PixelShader.hpp>
#include <Base/Graphics/VertexShader.hpp>
#include <Base/Graphics/Vertex.hpp>
#include <Base/Graphics/Geometry.hpp>
#include <Base/Graphics/Camera.hpp>
#include <Base/Graphics/Transform.hpp>

#pragma managed(pop)

using namespace System;

namespace EditorWorld {

	public ref class World {
		Graphics* graphics;

		Camera* camera;
		PixelShader* pixel;
		VertexShader* vertex;
		Geometry* model;
		Texture2D* texture;

		RenderTarget2D* backBuffer;
		IntPtr renderSurface;

		RenderTarget2D* GetRenderTargetFromSurface(IntPtr surface);

		void UpdateBackBuffer(IntPtr surface);

	public:

		World();
		~World();
		!World();

		property float ModelYaw;

		void Render(IntPtr surface);

	};

}