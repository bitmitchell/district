#include "World.hpp"
#include <Base/Application/Log.hpp>
#include <Base/Application/Math.hpp>
#include <Base/Graphics/RenderState.hpp>

namespace EditorWorld {

	World::World() {
		graphics = Graphics::Create();

		pixel = PixelShader::Create(graphics, "Shaders/pixel.cso");
		vertex = VertexShader::Create(graphics, "Shaders/vertex.cso");

		model = Geometry::CreateFromBDM(graphics, "Models/box.bdm");
		texture = Texture2D::FromFile(graphics, "Materials/test.png");

		camera = new Camera(glm::vec3(-2, 3, -5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

		backBuffer = nullptr;
		renderSurface = IntPtr(nullptr);

		ModelYaw = 0.0f;
	}


	World::~World() {
		this->!World();
	}


	World::!World() {
		graphics->Shutdown();
	}


	void World::Render(IntPtr surface) {
		UpdateBackBuffer(surface);

		backBuffer->Apply(graphics);
		backBuffer->Clear(graphics, 0.0f, 0.0f, 0.0f, 1.0f);

		RenderState::SetDepth(graphics, RenderState::DepthMode::None);

		Transform transform(camera->GetTransposeProjectionViewMatrix(), glm::rotate(ModelYaw, glm::vec3(0, 1, 0)));
		//graphics->Draw(model, vertex, pixel, transform, texture);

		graphics->Context->Flush();
	}


	void World::UpdateBackBuffer(IntPtr surface) {
		if (surface == renderSurface) {
			return;
		}

		if (backBuffer != nullptr) {
			backBuffer->Release();
			delete backBuffer;
		}

		backBuffer = GetRenderTargetFromSurface(surface);
		renderSurface = surface;

		const D3D11_TEXTURE2D_DESC& desc = backBuffer->GetDesc();
		camera->SetResolution(desc.Width, desc.Height);
	}


	RenderTarget2D* World::GetRenderTargetFromSurface(IntPtr surface) {
		IUnknown* unknown = (IUnknown*)surface.ToPointer();
		IDXGIResource* resource;
		
		HRESULT res = unknown->QueryInterface(__uuidof(IDXGIResource), (void**)&resource);
		if (FAILED(res)) {
			Log::Error("Error getting IDXGIResource from surface");
		}

		HANDLE sharedHandle;
		res = resource->GetSharedHandle(&sharedHandle);
		if (FAILED(res)) {
			Log::Error("Error getting shared handle from resource");
		}

		resource->Release();

		IUnknown* tempResource;
		res = graphics->Device->OpenSharedResource(sharedHandle, __uuidof(ID3D11Resource), (void**)&tempResource);
		if (FAILED(res)) {
			Log::Error("Error opening shared resource");
		}

		ID3D11Texture2D* renderTexture;
		res = tempResource->QueryInterface(__uuidof(ID3D11Texture2D), (void**)&renderTexture);
		if (FAILED(res)) {
			Log::Error("Error getting render texture from shared resource");
		}

		tempResource->Release();

		return RenderTarget2D::FromTexture(graphics, renderTexture);
	}

}