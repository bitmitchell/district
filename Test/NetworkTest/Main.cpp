#include <Base/Includes.hpp>
#include <Base/Application/Log.hpp>
#include <Base/Network/Network.hpp>
#include <thread>

int main(int argc, char** argv) {
	Log::Init();

	bool hostRunning = true, clientRunning = true;
	Network* host = Network::Create();
	Network* client = Network::Create();

	client->Connected += [&]() {
		Log::Info("Client Connected Event!");

		Message message(9);
		message << uint16_t(1);
		message << "Hello";

		client->Send(message, Network::Events);
	};
	client->Disconnected += []() {
		Log::Info("Server disconnected client event!");
	};
	client->ConnectionTimedOut += []() {
		Log::Info("Connection to server timed out");
	};
	client->TimedOut += []() {
		Log::Info("Lost connection to server");
	};

	client->MessageReceived += [client](Message& msg) {
		uint16_t messageType;
		msg >> messageType;

		if (messageType == 1) {
			std::this_thread::sleep_for(std::chrono::seconds(1));
			std::string str;
			msg >> str;
			client->Send(msg, Network::Events);
			Log::Info("Server said %%%", str);
		}
	};

	host->MessageReceived += [host](Message& msg) {
		uint16_t messageType;
		msg >> messageType;

		if (messageType == 1) {
			std::this_thread::sleep_for(std::chrono::seconds(1));
			std::string str;
			msg >> str;
			host->Send(msg, Network::Events);
			Log::Info("Client said %%%", str);
		}
	};

	std::thread hostThread([&]() {
		if (!host->Host(2514)) {
			Log::Error("Error hosting network");
		}

		while (hostRunning) {
			host->Update();
		}

		Log::Info("Host::Disconnect");
		host->Disconnect();
		Log::Info("Done");
	});


	std::thread clientThread([&]() {
		if (!client->Connect("127.0.0.1", 2514)) {
			Log::Error("Error connecting to network");
		}

		while (clientRunning) {
			client->Update();
		}

		Log::Info("Client::Disconnect");
		client->Disconnect();
		Log::Info("Done");
	});

	std::this_thread::sleep_for(std::chrono::seconds(15));

	clientRunning = false;
	clientThread.join();

	hostRunning = false;
	hostThread.join();

	Log::Shutdown();
	return 0;
}