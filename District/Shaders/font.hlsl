struct PixelInput {
	float4 ScreenPosition : SV_POSITION;
	float3 Position : POSITION0;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float4 LightPosition : POSITION1;
};

cbuffer FontBuffer: register(b4) {
	float4 FontColor;
}

Texture2D character : register(t0);
SamplerState linearSampler : register(s0);

float4 main(PixelInput input) : SV_TARGET
{
	float char = character.Sample(linearSampler, input.TexCoord).x;
	clip(char - 0.1);
	return float4(1.0f, 1.0f, 1.0f, char) * FontColor;
}