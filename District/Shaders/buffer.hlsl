#include "gamma.hlsli"

struct PixelInput {
	float4 ScreenPosition : SV_POSITION;
	float3 Position : POSITION0;
	float3 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
	float4 LightPosition : POSITION1;
};

cbuffer CornerBuffer : register(b1) {
	float4 Corner[4];
	float3 CameraPosition;
	float3 CameraDirection;
}

Texture2D color : register(t0);
Texture2D properties : register(t1);

SamplerState clampSampler : register(s0);
SamplerState wrapSampler : register(s1);

struct GBuffer {
	float4 Albedo : SV_TARGET0;
	float4 Normal : SV_TARGET1;
	float4 Properties : SV_TARGET2;
};

GBuffer main(PixelInput input) {
	GBuffer output = (GBuffer)0;

	output.Albedo.xyz = CorrectTextureGamma(color.Sample(wrapSampler, input.TexCoord).xyz);
	output.Albedo.w = 1.0f;
	output.Normal.xyz = input.Normal;
	output.Normal.w = length(input.Position - CameraPosition);
	output.Properties = properties.Sample(wrapSampler, input.TexCoord);

	return output;
}