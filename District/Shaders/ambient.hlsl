#include "lighting.hlsli"
#include "gamma.hlsli"


float4 main(PixelInput input) : SV_TARGET{
	float3 albedo, normal, position, properties;
	float foreground, depth, roughness, metalness;

	ReadSample(input.TexCoord, input.Direction, albedo, foreground, normal, depth, position, roughness, metalness);

	return float4(CorrectSceneGamma(albedo * 0.02), foreground);
}