struct PixelInput {
	float4 ScreenPosition : SV_POSITION;
	float3 Position : POSITION0;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float4 LightPosition : POSITION1;
};

cbuffer FontBuffer: register(b4) {
	float4 FontColor;
}

float4 main(PixelInput input) : SV_TARGET
{
	return FontColor;
}