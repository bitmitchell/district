cbuffer CameraBuffer : register(b0) {
	float4x4 ProjectionView;
	float4x4 World;
}


cbuffer PerLight : register(b3) {
	float4x4 LightProjectionView;
	float4 LightPosition;
	float4 LightDirection;
}


struct VertexInput {
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD;
};


struct VertexOutput {
	float4 ScreenPosition : SV_POSITION;
	float3 Position : POSITION0;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float4 LightPosition : POSITION1;
};


VertexOutput main(VertexInput input)
{
	VertexOutput vert;

	float4 position = mul(float4(input.Position.xyz, 1), World);
	vert.Position = position.xyz;
	vert.ScreenPosition = mul(position, ProjectionView);
	vert.LightPosition = mul(position, LightProjectionView);
	vert.Normal = mul(float4(input.Normal, 0), World).xyz;
	vert.TexCoord = input.TexCoord;

	return vert;
}