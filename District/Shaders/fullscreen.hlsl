
struct VertexOutput {
	float4 ScreenPosition : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Direction : TEXCOORD1;
};


cbuffer CornerBuffer : register(b1) {
	float4 Corner[4];
	float3 CameraPosition;
	float3 CameraDirection;
}


float3 GetCorner(float2 uv) {
	float4 top = lerp(Corner[0], Corner[1], uv.x);
	float4 bot = lerp(Corner[2], Corner[3], uv.x);

	return lerp(top, bot, uv.y).xyz;
}


VertexOutput main(uint id : SV_VertexID)
{
	VertexOutput vert;
	vert.ScreenPosition.x = (float)(id / 2) * 4.0 - 1.0;
	vert.ScreenPosition.y = (float)(id % 2) * 4.0 - 1.0;
	vert.ScreenPosition.z = 0;
	vert.ScreenPosition.w = 1;
	
	vert.TexCoord.x = (float)(id / 2) * 2.0;
	vert.TexCoord.y = 1.0 - (float)(id % 2) * 2.0;

	vert.Direction = GetCorner(vert.TexCoord);

	return vert;
}