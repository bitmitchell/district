
float3 CorrectTextureGamma(float3 rgb) {
	return pow(rgb, 2.2);
}


float3 CorrectSceneGamma(float3 rgb) {
	return pow(rgb, 1 / 2.2);
}