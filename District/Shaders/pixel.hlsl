#define MULTISAMPLE
#define USE_VSM

cbuffer CameraBuffer : register(b0) {
	float4x4 ProjectionView;
	float4x4 World;
}

cbuffer PerLight : register(b3) {
	float4x4 LightProjectionView;
	float3 LightPosition;
	float3 LightDirection;
	float LightStrength;
}

struct PixelInput {
	float4 ScreenPosition : SV_POSITION;
	float3 Position : POSITION0;
	float3 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
	float4 LightPosition : POSITION1;
};

Texture2D material : register(t0);
Texture2D shadowMap : register(t1);
SamplerState clampSampler : register(s0);
SamplerState wrapSampler : register(s1);


float2 SampleShadowMap(float2 samplePosition) {
#if defined(MULTISAMPLE)
	float2 offset = float2(1, 1) / 512;

	float2 samples = 0;

	[unroll]
	for (float x = -1.5; x < 2.0; x += 1.0) {
		[unroll]
		for (float y = -1.5; y < 2.0; y += 1.0) {
			samples += shadowMap.SampleLevel(clampSampler, samplePosition + offset * float2(x, y), 2).xy;
		}
	}

	return samples / 16.0;
#else
	return shadowMap.SampleLevel(clampSampler, samplePosition, 3).xy;
#endif
}


float SmoothRegion(float x, float start, float end, float border) {
	float left = smoothstep(start, start + border, x);
	float right = smoothstep(end - border, end, x);
	return left - right;
}


float GetShadowing(float2 samplePosition, float depth) {
	float fade = SmoothRegion(samplePosition.x, 0, 1, 0.1) * SmoothRegion(samplePosition.y, 0, 1, 0.1);
	
	float2 moments = SampleShadowMap(samplePosition) * float2(LightStrength, LightStrength * LightStrength);
#if defined(USE_VSM)
	float sigma = max(moments.y - moments.x * moments.x, 0.1);
	float delta = max(depth - moments.x, 0);

	float light= smoothstep(0.75, 1.0, sigma / (sigma + pow(delta, 2)));
#else
	float light = saturate(exp(max(-1.0f, 3.0f * (moments.x - depth))));
#endif

	return lerp(1.0, light, fade);
}


float4 main(PixelInput input) : SV_TARGET {

	float3 lightDirection = LightPosition - input.Position;
	float lightPosDepth = dot(-lightDirection, LightDirection);
	float2 lightSamplePos = (input.LightPosition.xy * float2(1, -1) / input.LightPosition.w) * 0.5 + 0.5;

	float light = GetShadowing(lightSamplePos, lightPosDepth);
	float falloff = LightStrength / dot(lightDirection, lightDirection);
	float cutoff = 1.0 / LightStrength;
	falloff = max((falloff - cutoff) / (1 - cutoff), 0);
	float diffuse = dot(normalize(lightDirection), normalize(input.Normal)) * falloff;

	return float4(material.Sample(wrapSampler, input.TexCoord).xyz * (saturate(light * diffuse) * 0.8 + 0.2), 1);
}