struct PixelInput {
	float4 ScreenPosition : SV_POSITION;
	float3 Position : POSITION0;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float4 LightPosition : POSITION1;
};

cbuffer PerLight : register(b3) {
	float4x4 LightProjectionView;
	float3 LightPosition;
	float3 LightDirection;
	float LightStrength;
	float3 LightColor;
	float LightDistance;
}

float4 main(PixelInput input) : SV_TARGET
{
	float d = dot(input.Position - LightPosition, LightDirection) / LightDistance;
	return float4(d, d * d, 0, 1);
}