#include "lighting.hlsli"
#include "gamma.hlsli"

float4 main(PixelInput input) : SV_TARGET {
	float3 albedo, normal, position, properties;
	float foreground, depth, roughness, metalness;

	ReadSample(input.TexCoord, input.Direction, albedo, foreground, normal, depth, position, roughness, metalness);

	float2 lightSamplePosition = ShadowSamplePosition(position);
	float2 shadowMoments = SampleShadowMap(lightSamplePosition);

	float2 moments = SampleShadowMap(lightSamplePosition);
	float lightDepth = GetLightingDepth(position);
	float shadows = VarianceShadowMapping(shadowMoments, lightSamplePosition, position);

	float3 light = normalize(LightPosition - position);
	float3 view = normalize(CameraPosition - position);
	float3 m = normalize(light + view);

	float dist = length(position - LightPosition);
	float falloff = LightStrength / (dist * dist);
	float cutoff = 1.0 / LightStrength;
	falloff = max((falloff - cutoff) / (1 - cutoff), 0);

	float alpha = roughness * roughness;
	float alpha2 = alpha * alpha;
	float k = alpha / 2;

	float nDotL = dot(normal, light);
	float nDotV = dot(normal, view);
	float lDotV = dot(light, view);

	// Oren-Nayar http://mimosa-pudica.net/improved-oren-nayar.html

	float s = lDotV - nDotL * nDotV;
	float t = (s <= 0 ? 1 : max(nDotL, nDotV));
	float p = length(albedo);

	float A = 1 / (PI + 0.9041 * roughness);
	float B = roughness / (PI + 0.9041 * roughness);

	float oren = nDotL * (A + B * s / t);

	// Specular BRDF http://jmonkeyengine.org/300495/physically-based-rendering-part-two/
	// Schlick Fresnel / GGX / Schlick GGX
	float F = Fresnel(metalness, nDotV);
	float D = alpha2 / (PI * pow(pow(dot(normal, m), 2) * (alpha2 - 1) + 1, 2));
	float G = nDotV / (nDotV * (1 - k) + k);

	float f = D * F * G / (4 * nDotV) * max(0, nDotL);

	return float4(CorrectSceneGamma((oren * albedo * (1 - F) + f) * LightColor * shadows * falloff), foreground);
}