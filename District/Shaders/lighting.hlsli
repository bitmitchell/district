
struct PixelInput {
	float4 ScreenPosition : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Direction : TEXCOORD1;
};

cbuffer CornerBuffer : register(b1) {
	float4 Corner[4];
	float3 CameraPosition;
	float3 CameraDirection;
}

cbuffer LightBuffer : register(b3) {
	float4x4 LightProjectionView;
	float3 LightPosition;
	float3 LightDirection;
	float LightStrength;
	float3 LightColor;
	float LightDistance;
}

Texture2D albedoMap : register(t5);
Texture2D normalMap : register(t6);
Texture2D propertiesMap : register(t7);
Texture2D shadowMap : register(t2);

SamplerState clampSampler : register(s0);
SamplerState wrapSampler : register(s1);

#define SHADOW_LEVEL 2
#define PI 3.14159

float SmoothRegion(float x, float start, float end, float border) {
	float left = smoothstep(start, start + border, x);
	float right = smoothstep(end - border, end, x);
	return left - right;
}


void ReadSample(in float2 texCoord, in float3 pixelDirection, out float3 albedo, out float foreground, out float3 normal, out float depth, out float3 position, out float roughness, out float metalness) {
	float4 albedoSample = albedoMap.Sample(clampSampler, texCoord);
	float4 normalSample = normalMap.Sample(clampSampler, texCoord);
	float4 propertiesSample = propertiesMap.Sample(clampSampler, texCoord);

	albedo = albedoSample.xyz;
	foreground = albedoSample.w;
	normal = normalize(normalSample.xyz);
	depth = normalSample.w;
	position = normalize(pixelDirection) * depth + CameraPosition;
	roughness = propertiesSample.x;
	metalness = propertiesSample.y;
}


float DiffusePoint(float3 p, float3 n) {
	float3 l = LightPosition - p;
	float cutoff = 1.0 / LightStrength;
	float falloff = LightStrength / dot(l, l);
	falloff = max((falloff - cutoff) / (1 - cutoff), 0);
	return dot(normalize(l), normalize(n)) * falloff;
}


float DiffuseDirection(float3 n) {
	return dot(-LightDirection, normalize(n)) * LightStrength;
}


float2 ShadowSamplePosition(float3 p) {
	float4 lightPosition = mul(float4(p, 1), LightProjectionView);
	return (lightPosition.xy * float2(1, -1) / lightPosition.w) * 0.5 + 0.5;
}


float2 SampleShadowMap(float2 lightSamplePos) {
	return shadowMap.SampleLevel(clampSampler, lightSamplePos, SHADOW_LEVEL).xy * float2(LightDistance, LightDistance * LightDistance);
	const float2 offset[] = {
		float2(0.001, 0.001),
		float2(-0.001, 0.001),
		float2(-0.001, -0.001),
		float2(0.001, -0.001),
	};

	float2 avg = 0;
	for (int i = 0; i < 4; ++i) {
		avg += shadowMap.SampleLevel(clampSampler, lightSamplePos + offset[i], SHADOW_LEVEL).xy * 0.25;
	}
	return avg * float2(LightDistance, LightDistance * LightDistance);
}


float GetLightingDepth(float3 p) {
	return dot(p - LightPosition, LightDirection);
}


float VarianceShadowMapping(float2 moments, float2 samplePosition, float3 p) {
	float variance = max(moments.y - moments.x * moments.x, 0.5);
	float depth = max(GetLightingDepth(p) - moments.x, 0);
	float light = smoothstep(0.5, 1.0, variance / (variance + pow(depth, 2)));
	float fade = SmoothRegion(samplePosition.x, 0, 1, 0.1) * SmoothRegion(samplePosition.y, 0, 1, 0.1);

	return lerp(1.0, light, fade);
}


float Fresnel(float R0, float nDotV) {
	return R0 + (1 - R0) * pow(1 - nDotV, 5);
}