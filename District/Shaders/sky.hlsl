

cbuffer PerFrame : register(b2) {
	float TimeOfDay;
}

struct PixelInput {
	float4 ScreenPosition : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Direction : TEXCOORD1;
};

Texture2D material : register(t0);
SamplerState clampSampler : register(s0);
SamplerState wrapSampler : register(s1);

float4 main(PixelInput input) : SV_TARGET {
	float3 dir = normalize(input.Direction);
	float2 coord = float2(
		TimeOfDay, // time of day (0 - dawn, 0.25 - noon, 0.5 - dusk, 0.75 - midnight)
		clamp(1.0f - dir.y, 0.1f, 0.9f) // angle to azimuth
	);
	return material.Sample(wrapSampler, coord);
}