#include "lighting.hlsli"
#include "gamma.hlsli"


float4 main(PixelInput input) : SV_TARGET {
	float3 albedo, normal, position, properties;
	float foreground, depth, roughness, metalness;

	ReadSample(input.TexCoord, input.Direction, albedo, foreground, normal, depth, position, roughness, metalness);

	float2 lightSamplePosition = ShadowSamplePosition(position);
	float2 shadowMoments = SampleShadowMap(lightSamplePosition);

	float2 moments = SampleShadowMap(lightSamplePosition);
	float lightDepth = GetLightingDepth(position);
	float shadows = VarianceShadowMapping(shadowMoments, lightSamplePosition, position);

	
	float3 light = normalize(-LightDirection);
	float3 view = normalize(CameraPosition - position);
	float3 m = normalize(light + view);

	float alpha = roughness * roughness;
	float alpha2 = alpha * alpha;
	float k = alpha / 2;

	float nDotL = dot(normal, light);
	float nDotV = dot(normal, view);

	float lambert = nDotL / PI;
	
	float F = Fresnel(metalness, nDotV);
	float D = alpha2 / (PI * pow(pow(dot(normal, m), 2) * (alpha2 - 1) + 1, 2));
	float G = nDotV / (nDotV * (1 - k) + k);

	float f = D * F * G / (4 * nDotV);

	return float4(CorrectSceneGamma((lambert * albedo * (1 - F) + f) * LightColor * shadows * LightStrength), foreground);
}