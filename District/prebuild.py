
import sys, os, glob, subprocess, re
sys.path.insert(0, "../Tools")
from GetBuildNumber import *

def ReadVersionHeader():
	try:
		with open("Version.hpp", "r") as header:
			define = header.read()
		
		match = re.search(".*DISTRICT_BUILD\s(\d+)", define)
		if match:
			return int(match.group(1))
	except:
		pass
	return None

def WriteVersionHeader():
	build = GetBuildNumber()
	current = ReadVersionHeader()
	
	if current == None or build != current:
		print("Updating version number (%s -> %s)" % (current, build))
		version = open("Version.hpp", "w")
		version.write("#pragma once\n#define DISTRICT_BUILD %i" % GetBuildNumber())
		version.close()
	else:
		print("Version number up to date")

def ConvertModels():
	pwd = os.getcwd()
	os.chdir("../bin")
	
	modelConverter = "modelconverter"
	modelDir = "Models"
	argList = [modelConverter, modelDir]
	for file in glob.glob("%s/*.dmf" % modelDir):
		argList.append(file)
	
	subprocess.call(argList)
	
	os.chdir(pwd)

if __name__ == "__main__":
	
	WriteVersionHeader()
	
	ConvertModels()
