#pragma once
#include "../Includes.hpp"
#include <Base/Application/DataFormat.hpp>


enum PhysicsBodyType {
	Unknown,
	Box,
	Sphere
};


inline PhysicsBodyType GetPhysicsType(const std::string& name) {
	if (name == "box") {
		return PhysicsBodyType::Box;
	} else if (name == "sphere") {
		return PhysicsBodyType::Sphere;
	} else {
		return PhysicsBodyType::Unknown;
	}
}


inline std::string GetPhysicsType(PhysicsBodyType type) {
	switch (type) {
	case PhysicsBodyType::Box:
		return "box";
	case PhysicsBodyType::Sphere:
		return "sphere";
	default:
		return "unknown";
	}
}


struct PhysicsBody {
	PhysicsBodyType Type;
	float Mass;
	union {
		struct { struct { glm::vec3 HalfSize; } Box; };
		struct { float Radius; } Sphere;
	};
};


struct WeaponData {
	std::string Name;
	std::string AmmoType;
	std::string Description;
};


struct PropData {
	std::string Name;
	std::string Mesh;
	std::string Texture;
	std::string Properties;
	PhysicsBody Physics;
};


struct BulletData {
	std::string Name;
	float Velocity;
	float Caliber;
	float Mass;
	float DragCoefficient;
};


template<>
struct ConvertDDF<WeaponData> {
	WeaponData operator()(Object& obj) {
		WeaponData data;
		data.Name = obj["name"].As<std::string>();
		data.AmmoType = obj["ammo"].As<std::string>();
		data.Description = obj["description"].As<std::string>();
		return data;
	}
};


template<>
struct ConvertDDF<PhysicsBody> {
	PhysicsBody operator()(Object& obj) {
		PhysicsBody body;
		body.Type = GetPhysicsType(obj["type"].As<std::string>());
		body.Mass = obj["mass"].As<float>();
		switch (body.Type) {
		case PhysicsBodyType::Box:
			body.Box.HalfSize = obj["halfsize"].As<glm::vec3>();
			break;
		case PhysicsBodyType::Sphere:
			body.Sphere.Radius = obj["radius"].As<float>();
			break;
		default:
			throw std::runtime_error("Unknown physics type " + obj["type"].As<std::string>());
		}
		return body;
	}
};


template<>
struct ConvertDDF<PropData> {
	PropData operator()(Object& obj) {
		PropData data;
		data.Name = obj["name"].As<std::string>();
		data.Mesh = obj["mesh"].As<std::string>();
		data.Texture = obj["texture"].As<std::string>();
		data.Properties = obj["properties"].As<std::string>();
		data.Physics = obj["physics"].As<PhysicsBody>();
		return data;
	}
};


template<>
struct ConvertDDF<BulletData> {
	BulletData operator()(Object& obj) {
		BulletData data;
		data.Name = obj["name"].As<std::string>();
		data.Velocity = obj["velocity"].As<float>();
		data.Caliber = obj["caliber"].As<float>();
		data.Mass = obj["mass"].As<float>();
		data.DragCoefficient = obj["drag"].As<float>();
		return data;
	}
};