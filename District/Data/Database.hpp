#pragma once
#include "../Includes.hpp"
#include <Base/Application/DataFormat.hpp>
#include "DataTypes.hpp"

template <typename Data>
class Database {
	std::unordered_map<std::string, Data> table;

public:

	static Database LoadFromDDF(const std::string& filename) {
		Database result;

		Object obj = Object::FromFile(filename);

		for (auto& it : obj) {
			std::string key = it.first;
			Data value = it.second.As<Data>();
			result.table[key] = value;
		}

		return result;
	}

	void SaveToYAML(const std::string& filename) {
		std::ofstream file(filename);
		YAML::Node data;
		for (const auto& it : table) {
			data[it.first] = it.second;
		}
		file << data;
		file.close();
	}

	const Data& Get(const std::string& key) {
		if (table.count(key) == 0) {
			Log::Error("Database error: key not found - %%%", key);
		}
		return table[key];
	}

	const Data& operator[](const std::string& key) {
		return Get(key);
	}

};