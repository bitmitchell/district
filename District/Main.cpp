#include "Version.hpp"
#include "Logic/Game.hpp"
#include <Base/Application/Log.hpp>
#include <Base/Application/Performance.hpp>

#if defined(DEBUG)
int main(int, char**) {
#else
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
#endif

	Log::Init();
	Log::Info("District - build %%%", DISTRICT_BUILD);

	Window::Register(GetModuleHandle(nullptr));

	Game game;

	try {
		game.Initialize();

		game.Run();

		game.Shutdown();
	} catch (std::runtime_error error) {
		MessageBox(nullptr, error.what(), "Critical Error", MB_OK);
	}

	Window::Unregister();

	Log::Shutdown();

	PerformanceTimer::ExportData("perf.txt");

#if defined(X64)
	printf("Press any key to continue...");
	getc(stdin);
#endif
	return 0;
}