#include "Generator.hpp"
#include <Base/Graphics/Buffer.hpp>
#include <Base/Graphics/Vertex.hpp>
#include <Base/Application/Math.hpp>

namespace {
	const float DoorWidth = 1.2f;
	const float DoorHeight = 1.9f;
	const float RoomHeight = 2.4f;
	const float WallThickness = 0.1f;
	const float StairSize = 0.5f;
}

Generator::Generator(int seed) {
	random.seed(seed);
}


void Generator::AddTriangle(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 normal, std::vector<Vertex>& verts, std::vector<Triangle>& tris) {
	int i = verts.size();
	tris.emplace_back(i, i + 1, i + 2);
	verts.emplace_back(a, normal, glm::vec2(a.x, a.z));
	verts.emplace_back(b, normal, glm::vec2(b.x, b.z));
	verts.emplace_back(c, normal, glm::vec2(c.x, c.z));
}


void Generator::Box(glm::vec3 a, glm::vec3 b, std::vector<Vertex>& verts, std::vector<Triangle>& tris) {
	glm::vec3 min(std::min(a.x, b.x), std::max(a.y, b.y), std::min(a.z, b.z));
	glm::vec3 max(std::max(a.x, b.x), std::min(a.y, b.y), std::max(a.z, b.z));

	glm::vec3 corner[] = {
		min,
		{ max.x, min.y, min.z },
		{ min.x, min.y, max.z },
		{ max.x, min.y, max.z },

		{ min.x, max.y, min.z },
		{ max.x, max.y, min.z },
		{ min.x, max.y, max.z },
		max
	};

	glm::vec3 right = glm::normalize(glm::cross(corner[4] - corner[0], corner[1] - corner[0]));
	glm::vec3 up = glm::normalize(glm::cross(corner[3] - corner[0], corner[2] - corner[0]));
	glm::vec3 forward = glm::normalize(glm::cross(corner[2] - corner[0], corner[4] - corner[0]));

	// left and right
	AddTriangle(corner[1], corner[0], corner[4], -right, verts, tris);
	AddTriangle(corner[5], corner[1], corner[4], -right, verts, tris);

	AddTriangle(corner[2], corner[3], corner[7], right, verts, tris);
	AddTriangle(corner[6], corner[2], corner[7], right, verts, tris);

	// top and bottom
	AddTriangle(corner[2], corner[0], corner[3], -up, verts, tris);
	AddTriangle(corner[3], corner[0], corner[1], -up, verts, tris);

	AddTriangle(corner[5], corner[4], corner[6], up, verts, tris);
	AddTriangle(corner[7], corner[5], corner[6], up, verts, tris);

	// front and back
	AddTriangle(corner[4], corner[0], corner[2], -forward, verts, tris);
	AddTriangle(corner[4], corner[2], corner[6], -forward, verts, tris);

	AddTriangle(corner[3], corner[1], corner[5], forward, verts, tris);
	AddTriangle(corner[3], corner[5], corner[7], forward, verts, tris);
}


void Generator::Wall(glm::vec3 a, glm::vec3 b, float height, std::vector<Vertex>& verts, std::vector<Triangle>& tris, float door) {
	if (door < 0.0f || door > 1.0f) {
		glm::vec3 up = glm::vec3(0, 1, 0);
		glm::vec3 dir = glm::normalize(b - a);
		glm::vec3 normal = glm::normalize(glm::cross(up, b - a));
		float offset = WallThickness * 0.5f;

		Box(a + up * height - dir * offset + normal * offset,
			b + dir * offset - normal * offset, verts, tris);
	} else {
		float wallLength = glm::length(a - b);
		float doorStart = door * (wallLength - DoorWidth) / wallLength;
		float doorEnd = doorStart + DoorWidth / wallLength;

		glm::vec3 doorA = Lerp(a, b, doorStart);
		glm::vec3 doorB = Lerp(a, b, doorEnd);
		glm::vec3 up = glm::vec3(0, DoorHeight, 0);

		if (glm::length(a - doorA) > 0.01f) {
			Wall(a, doorA, height, verts, tris);
		}
		Wall(doorA + up, doorB + up, height - DoorHeight, verts, tris);
		if (glm::length(b - doorB) > 0.01f) {
			Wall(doorB, b, height, verts, tris);
		}
	}
}


void Generator::Stairs(glm::vec3 a, glm::vec3 b, float width, float height, std::vector<Vertex>& verts, std::vector<Triangle>& tris) {
	float length = glm::length(a - b);
	int stepCount = int(length / StairSize);

	glm::vec3 abDir = glm::normalize(b - a);

	for (int i = 0; i < stepCount; ++i) {
		float stepStart = float(i) / (float)(stepCount);
		float stepEnd = float(i + 1) / (float)(stepCount);

		glm::vec3 start = Lerp(a, b, stepStart);
		glm::vec3 end = Lerp(a, b, stepEnd);

		glm::vec3 up = glm::vec3(0, 1, 0);
		glm::vec3 normal = glm::normalize(glm::cross(up, b - a));
		float stepWidth = width * 0.5f;
		float stepHeight = Lerp(0.0f, height, stepEnd);

		Box(start + up * stepHeight + normal * stepWidth,
			end - normal * stepWidth, verts, tris);
	}
}


Geometry* Generator::Floor(Graphics* graphics, World* physics) {

	std::vector<Vertex> verts = {
		{ glm::vec3(-100, 0, -100), glm::vec3(0, 1, 0), glm::vec2(-50, 50) },
		{ glm::vec3(100, 0, -100), glm::vec3(0, 1, 0), glm::vec2(50, 50) },
		{ glm::vec3(100, 0, 100), glm::vec3(0, 1, 0), glm::vec2(50, -50) },
		{ glm::vec3(-100, 0, 100), glm::vec3(0, 1, 0), glm::vec2(-50, -50) }
	};

	std::vector<Triangle> tris = {
		{ 0, 1, 2 },
		{ 0, 2, 3 }
	};

	physics->Add(CreatePhysicsBody(verts, tris));

	Buffer* vertexBuffer = Buffer::Create(graphics, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, sizeof(Vertex) * verts.size(), &verts[0]);
	Buffer* indexBuffer = Buffer::Create(graphics, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0, sizeof(Triangle) * tris.size(), &tris[0]);

	return Geometry::Create(vertexBuffer, 4, indexBuffer, 6);
}


Geometry* Generator::Building(Graphics* graphics, World* physics) {
	std::vector<Vertex> verts;
	std::vector<Triangle> tris;

	// stair case room
	Wall(glm::vec3(-5, 0, 0), glm::vec3(0, 0, 0), RoomHeight * 2, verts, tris);
	Wall(glm::vec3(-5, 0, 0), glm::vec3(-5, 0, -7), RoomHeight * 2, verts, tris, 6.0f / 7.0f);
	Wall(glm::vec3(-5, 0, -7), glm::vec3(0, 0, -7), RoomHeight * 2, verts, tris);
	Wall(glm::vec3(0, 0, -7), glm::vec3(0, 0, 0), RoomHeight, verts, tris, 0.5f);

	// staircase
	float halfHeight = RoomHeight * 0.5f;
	Stairs(glm::vec3(-4, 0, -5), glm::vec3(-4, 0, -2), 2, halfHeight, verts, tris);
	Stairs(glm::vec3(-3, halfHeight, -1), glm::vec3(0, halfHeight, -1), 2, halfHeight, verts, tris);
	Box(glm::vec3(-5, halfHeight, -2), glm::vec3(), verts, tris);

	// lower level
	Wall(glm::vec3(0, 0, 7), glm::vec3(5, 0, 7), RoomHeight + 1.0f, verts, tris, 0.5f);
	Wall(glm::vec3(0, 0, 0), glm::vec3(0, 0, 7), RoomHeight + 1.0f, verts, tris);
	Wall(glm::vec3(5, 0, 7), glm::vec3(5, 0, 0), RoomHeight + 1.0f, verts, tris);
	Wall(glm::vec3(5, 0, -7), glm::vec3(0, 0, -7), RoomHeight * 2, verts, tris);

	// upper level
	Box(glm::vec3(0, RoomHeight, 7), glm::vec3(5, RoomHeight - WallThickness, -7), verts, tris);
	Wall(glm::vec3(0, RoomHeight, 0), glm::vec3(5, RoomHeight, 0), RoomHeight, verts, tris, 0.5f);
	Wall(glm::vec3(5, 0, 0), glm::vec3(5, 0, -7), RoomHeight * 2, verts, tris);


	physics->Add(CreatePhysicsBody(verts, tris));

	Buffer* vertexBuffer = Buffer::Create(graphics, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, sizeof(Vertex) * verts.size(), &verts[0]);
	Buffer* indexBuffer = Buffer::Create(graphics, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0, sizeof(Triangle) * tris.size(), &tris[0]);

	return Geometry::Create(vertexBuffer, verts.size(), indexBuffer, tris.size() * 3);
}


Body* Generator::CreatePhysicsBody(std::vector<Vertex>& verts, std::vector<Triangle>& tris) {
	std::vector<glm::vec3> positions;
	positions.reserve(tris.size() * 3);
	for (const Triangle& tri : tris) {
		positions.push_back(verts[tri.A].Position);
		positions.push_back(verts[tri.B].Position);
		positions.push_back(verts[tri.C].Position);
	}
	return Body::StaticMesh(positions, glm::vec3());
}