#pragma once
#include "../Includes.hpp"
#include <Base/Graphics/Geometry.hpp>
#include <Base/Graphics/Vertex.hpp>
#include <Base/Physics/World.hpp>
#include <Base/Physics/Body.hpp>
#include <random>

typedef std::mt19937 RandomSource;
typedef std::uniform_real_distribution<float> RandomUniform;


class Generator {
	RandomSource random;

	void AddTriangle(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 normal, std::vector<Vertex>& verts, std::vector<Triangle>& tris);
	void Box(glm::vec3 a, glm::vec3 b, std::vector<Vertex>& verts, std::vector<Triangle>& tris);
	void Wall(glm::vec3 a, glm::vec3 b, float height, std::vector<Vertex>& verts, std::vector<Triangle>& tris, float door = -1);
	void Stairs(glm::vec3 a, glm::vec3 b, float width, float height, std::vector<Vertex>& verts, std::vector<Triangle>& tris);

	Body* CreatePhysicsBody(std::vector<Vertex>& verts, std::vector<Triangle>& tris);

public:

	Generator(int seed = 0);

	Geometry* Floor(Graphics* graphics, World* physics);
	Geometry* Building(Graphics* graphics, World* physics);

};