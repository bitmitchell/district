#include "Console.hpp"
#include <Base/Graphics/ScreenSpace.hpp>
#include <Base/Graphics/RenderState.hpp>
#include <Base/Application/Math.hpp>
#include <Base/Application/Performance.hpp>

namespace {
	// Values taken from ASCII table
	const int CharacterInputMin = 32;
	const int CharacterInputMax = 126;

	glm::vec4 white = glm::vec4(1, 1, 1, 1);
	glm::vec4 red = glm::vec4(1, 0, 0, 1);
}


void Console::InputContext::OnKeyDown(Keys::Key key) {
	switch (key) {
	case Keys::Enter:
		Submit();
		break;
	case Keys::Backspace:
	case Keys::Delete:
		Backspace();
		break;
	case Keys::Tilde:
		Hide();
		break;
	}
}


void Console::InputContext::OnChar(char character) {
	Character(character);
}


void Console::Initialize(Font* font, Game* game) {
	consoleFont = font;
	enabled = false;

	game->ShowConsole += [this, game]() {
		enabled = true;
		skippedExtraTilde = false;
		inputString = std::string();
		game->Window->PushInputContext(&input);
	};

	input.Hide += [this, game]() {
		enabled = false;
		game->Window->PopInputContext(&input);
	};

	input.Character += [this](char input) {
		if (input >= CharacterInputMin && input <= CharacterInputMax) {
			if (input == '`' && skippedExtraTilde == false) {
				skippedExtraTilde = true;
				return;
			}
			inputString += input;
		}
	};

	input.Backspace += [this]() {
		if (inputString.size() > 0) {
			inputString.erase(inputString.end() - 1);
		}
	};

	input.Submit += [this]() {
		if (inputString.size() > 0) {
			Log::Info("%%%", inputString);
			std::vector<std::string> command = Utilites::Split(inputString);
			if (commands.count(command[0])) {
				commands[command[0]](std::vector<std::string>(command.begin() + 1, command.end()));
			}
			inputString.clear();
		}
	};
}


void Console::AddCommand(std::string name, Delegate<std::vector<std::string>> delegate) {
	commands.emplace(name, delegate);
}

void Console::DrawConsole(Graphics* graphics) {
	if (enabled == true) {
		PerfFunction;
		RenderState::SetDepth(graphics, RenderState::DepthMode::None);
		ScreenSpace::Rectangle(graphics, glm::vec2(0, 0), glm::vec2(graphics->Settings.Width, 225.0f), glm::vec4(0.2f, 0.2f, 0.2f, 0.5f));
		ScreenSpace::Rectangle(graphics, glm::vec2(0, 202.0f), glm::vec2(graphics->Settings.Width, 1.0f), glm::vec4(0.0f, 0.0f, 0.0f, 0.7f));

		float y = 5.0f;
		for (const std::string& msg : Log::GetConsoleMessages()) {
			glm::vec4 color = white;
			if (Utilites::CompareStart(msg, "WARNING")) {
				color = red;
			};
			consoleFont->Draw(graphics, msg, 5.0f, y, color);
			y += 20.0f;
		}

		if (Wrap(blinkTimer.Time(), 0.0, 1.0) > 0.5) {
			consoleFont->Draw(graphics, inputString, 5.0f, 205.0f, white);
		} else {
			consoleFont->Draw(graphics, inputString + "|", 5.0f, 205.0f, white);
		}
	}
}