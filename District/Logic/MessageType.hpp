#pragma once
#include "../Includes.hpp"

enum MessageType {
	Reserved,

	Ping,
	ScoreboardUpdate,

	ClientPlayerInfo,

	ObjectUpdate,
	ClientInput,
	HostCreatesActor,
	HostDeletesObject,

	ClientShootsBullet,
	HostShootsBullet,

	SetGameState
};


typedef uint16_t MessageTypeSize;