#pragma once

#include "../Includes.hpp"
#include "GameObject.hpp"
#include <Base/Network/Message.hpp>
#include <Base/Physics/Character.hpp>
#include "Extrapolator.hpp"

class Actor : public GameObject {
	Extrapolator<glm::vec3> positionExtrapolator;

	float health;

	Actor(ObjectID id) : GameObject(id) {}
public:
	static Actor* Create(ObjectID id, glm::vec3 position);

	Character* Body;
	Event<Actor*> Death;

	void Jump();
	void SetVelocity(glm::vec3 velocity);
	bool IsGrounded();

	glm::vec3 Position();

	void TakeDamage(float damage);
	float GetHealth();

	Message* GetUpdateMessage(Game* game);
	void HandleUpdateMessage(Message* msg, Game* game);
	void UpdateClientPosition(Game* game);

};