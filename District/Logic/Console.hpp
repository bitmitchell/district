#pragma once
#include "../Includes.hpp"
#include <Base/Graphics/Graphics.hpp>
#include <Base/Graphics/Font/FontFactory.hpp>
#include <Base/Application/Log.hpp>
#include "Game.hpp"
#include <Base/Application/Utilities.hpp>
#include <Base/Application/Timer.hpp>

class Console {

	class InputContext : public IInputContext {
	public:
		Event<char> Character;
		Event<> Submit;
		Event<> Backspace;
		Event<> Hide;

		void OnKeyDown(Keys::Key key);
		void OnChar(char character);
	} input;

	Font* consoleFont;
	bool enabled, skippedExtraTilde;
	Timer blinkTimer;

	std::string inputString;
	std::unordered_map<std::string, Delegate<std::vector<std::string>>> commands;

public:
	void Initialize(Font* font, Game* game);
	void DrawConsole(Graphics* graphics);
	void AddCommand(std::string, Delegate<std::vector<std::string>>);
};


