#pragma once

template<typename Type>
class Extrapolator {
public:
	Extrapolator() {
		Reset(0, 0, Type());
	};

	bool AddSample(float packetTime, float curTime, Type pos) {
		Type vel;
		if (fabs(packetTime - lastPacketTime) > 1e-4) {
			float dt = 1.0f / (packetTime - lastPacketTime);
			vel = (pos - lastPacketPos) * dt;
		}
		return AddSample(packetTime, curTime, pos, vel);
	}

	bool AddSample(float packetTime, float curTime, Type pos, Type vel) {
		if (!Estimates(packetTime, curTime)) {
			return false;
		}
		lastPacketPos = pos;
		lastPacketTime = packetTime;
		ReadPosition(curTime, snapPos);
		aimTime = curTime + updateTime;
		float dt = aimTime - packetTime;
		snapTime = curTime;
		aimPos = pos + vel * dt;

		if (fabs(aimTime - snapTime) < 1e-4) {
			snapVel = vel;
		} else {
			float dt = 1.0f / (aimTime - snapTime);
			snapVel = (aimPos - snapPos) * dt;
		}
		return true;
	}

	void Reset(float packetTime, float curTime, Type const pos) {
		Reset(packetTime, curTime, pos, Type());
	}

	void Reset(float packetTime, float curTime, Type const pos, Type const vel) {
		lastPacketTime = packetTime;
		lastPacketPos = pos;
		snapTime = curTime;
		snapPos = pos;
		updateTime = curTime - packetTime;
		latency = updateTime;
		aimTime = curTime + updateTime;
		snapVel = vel;
		aimPos = snapPos + snapVel * updateTime;
	}

	bool ReadPosition(float forTime, Type& oPos) const {
		Type vel;
		return ReadPosition(forTime, oPos, vel);
	}

	bool ReadPosition(float forTime, Type& oPos, Type& oVel) const {
		bool ok = true;

		//  asking for something before the allowable time?
		if (forTime < snapTime) {
			forTime = snapTime;
			ok = false;
		}

		//  asking for something very far in the future?
		float maxRange = aimTime + updateTime;
		if (forTime > maxRange) {
			forTime = maxRange;
			ok = false;
		}

		//  calculate the interpolated position
		oVel = snapVel;
		oPos = snapPos + oVel * (forTime - snapTime);

		if (!ok) {
			oVel = Type();
		}

		return ok;
	}

	float EstimateLatency() const {
		return latency;
	}

	float EstimateUpdateTime() const {
		return updateTime;
	}
private:
	Type snapPos;
	Type snapVel;
	Type aimPos;
	Type lastPacketPos;
	float snapTime;
	float aimTime;
	float lastPacketTime;
	float latency;
	float updateTime;

	bool Estimates(float packet, float cur) {
		if (packet <= lastPacketTime) {
			return false;
		}

		//  The theory is that, if latency increases, quickly 
		//  compensate for it, but if latency decreases, be a 
		//  little more resilient; this is intended to compensate 
		//  for jittery delivery.
		float lat = cur - packet;
		if (lat < 0) lat = 0;
		if (lat > latency) {
			latency = (latency + lat) * 0.5f;
		} else {
			latency = (latency * 7 + lat) * 0.125f;
		}

		//  Do the same running average for update time.
		//  Again, the theory is that a lossy connection wants 
		//  an average of a higher update time.
		float tick = packet - lastPacketTime;
		if (tick > updateTime) {
			updateTime = (updateTime + tick) * 0.5f;
		} else {
			updateTime = (updateTime * 7 + tick) * 0.125f;
		}

		return true;
	}
};