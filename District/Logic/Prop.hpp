#pragma once
#include "../Includes.hpp"
#include <Base/Graphics/Geometry.hpp>
#include <Base/Graphics/Texture2D.hpp>
#include <Base/Physics/World.hpp>
#include <Base/Physics/Body.hpp>
#include <Base/Graphics/Material.hpp>
#include "../Data/Database.hpp"
#include "GameObject.hpp"
#include "Extrapolator.hpp"

class Prop : public GameObject {
	Extrapolator<glm::vec3> positionExtrapolator;
	Extrapolator<glm::vec4> rotationExtrapolator;

	glm::vec3 position, velocity;
	glm::quat rotation;
	const PropData* data;
	std::string type;

	Prop(ObjectID id) : GameObject(id), Mesh(nullptr), Physics(nullptr) {}
public:
	Geometry* Mesh;
	Material Material;
	Body* Physics;

	static Prop* Create(ObjectID id, glm::vec3 position, std::string propType, const PropData& data);

	void Initialize(Game* game);

	glm::vec3 Position() const;
	glm::quat Rotation() const;
	glm::vec3 Velocity() const;

	Message* GetUpdateMessage(Game* game);
	void HandleUpdateMessage(Message* msg, Game* game);
	void UpdateClientPosition(Game* game);

};