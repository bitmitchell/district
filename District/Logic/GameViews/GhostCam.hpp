#pragma once
#include "IGameView.hpp"
#include "../Game.hpp"
#include <Base/Graphics/Camera.hpp>

class GhostCam : public IGameView {
	class InputContext : public IInputContext {
		int centerX, centerY;
	public:
		bool Forward, Backward, Left, Right, Sprint, Up, Down;
		float Pitch, Yaw;

		Event<> Quit;
		Event<> Console;

		InputContext() : Forward(false), Backward(false), Left(false), Right(false), Sprint(false),
			Pitch(0.5f), Yaw(0.0f) {
		}

		void SetMouseCenter(int x, int y) {
			centerX = x;
			centerY = y;
		}

		void OnKeyDown(Keys::Key key);
		void OnKeyUp(Keys::Key key);
		void OnMouseMove(int x, int y);
	} input;

public:
	Camera* Camera;

	virtual void Initialize(Game*);
	virtual void Update(Game*);
	virtual void Shutdown(Game*);
	void MoveCamera(Game*);
};