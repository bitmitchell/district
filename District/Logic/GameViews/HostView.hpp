#pragma once
#include "../../Includes.hpp"
#include "IGameView.hpp"
#include <Base/Network/Network.hpp>
#include <Base/Application/Timer.hpp>
#include "../MessageType.hpp"
#include "../Actor.hpp"


class HostView : public IGameView {
	Timer updateTime, pingTime, scoreTime;

	void UpdateActorWithInput(uint8_t input, float yaw, Actor* actor);

	void HandlePing(Message& msg, Game* game);
	void HandleClientInput(Message& msg, Game* game);
	void HandleClientPlayerInfo(Message& msg, Game* game);
	void HandleClientShootsBullet(Message& msg, Game* game);

public:
	void Initialize(Game* game);
	void Update(Game* game);
	void Shutdown(Game* game);
};