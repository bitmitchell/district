#pragma once
class Game;

class IGameView {
public:
	virtual void Initialize(Game*) = 0;
	virtual void Update(Game*) = 0;
	virtual void Shutdown(Game*) = 0;
};