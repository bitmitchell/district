#pragma once
#include "GhostCam.hpp"
#include <Base/Application/Log.hpp>
#include <Base/Graphics/Camera.hpp>
#include "../Game.hpp"
#include <Base/Application/Math.hpp>

namespace {
	float MoveSpeed = 0.0025f;
	const float RotateSpeed = 0.001f;
}


void GhostCam::InputContext::OnKeyDown(Keys::Key key) {
	switch (key) {
	case Keys::W:
		Forward = true;
		break;
	case Keys::S:
		Backward = true;
		break;
	case Keys::A:
		Left = true;
		break;
	case Keys::D:
		Right = true;
		break;
	case Keys::Shift:
		Sprint = true;
		break;
	case Keys::Space:
		Up = true;
		break;
	case Keys::Control:
		Down = true;
		break;
	case Keys::Escape:
		Quit();
		break;
	case Keys::Tilde:
		Console();
		break;
	}
}


void GhostCam::InputContext::OnKeyUp(Keys::Key key) {
	switch (key) {
	case Keys::W:
		Forward = false;
		break;
	case Keys::S:
		Backward = false;
		break;
	case Keys::A:
		Left = false;
		break;
	case Keys::D:
		Right = false;
		break;
	case Keys::Shift:
		Sprint = false;
		break;
	case Keys::Control:
		Down = false;
		break;
	case Keys::Space:
		Up = false;
		break;
	}
}


void GhostCam::InputContext::OnMouseMove(int x, int y) {
	Pitch = Clamp(Pitch + float(y - centerY) * 0.002f, -float(M_PI_2 * 0.9f), float(M_PI_2 * 0.9f));
	Yaw = Wrap(Yaw + float(centerX - x) * 0.002f, -float(M_PI), float(M_PI));
}


void GhostCam::Initialize(Game* game) {
	int centerX = game->Graphics->Settings.Width / 2;
	int centerY = game->Graphics->Settings.Height / 2;
	input.SetMouseCenter(centerX, centerY);

	input.Console += [game]() { game->ShowConsole(); };
	input.Quit += [game]() { game->Close(); };

	game->Window->SetCursorPos(centerX, centerY);
	game->Window->PushInputContext(&input);

	Camera = new ::Camera(glm::vec3(0, 2, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	Camera->SetResolution(game->Graphics->Settings.Width, game->Graphics->Settings.Height);
}


void GhostCam::Update(Game* game) {
	MoveCamera(game);
}


void GhostCam::Shutdown(Game* game) {
	game->Window->PopInputContext(&input);
}


void GhostCam::MoveCamera(Game* game) {
	glm::vec3 pos = Camera->GetPosition();
	glm::vec3 front = glm::normalize(Camera->GetDirection());
	glm::vec3 left = glm::normalize(glm::cross(front, Camera->GetUp()));
	glm::vec3 up = glm::normalize(glm::cross(left, front));

	float speed = input.Sprint ? MoveSpeed * 3 : MoveSpeed;

	if (input.Forward) {
		pos += front * speed;
	}
	if (input.Backward) {
		pos -= front * speed;
	}
	if (input.Left) {
		pos -= left * speed;
	}
	if (input.Right) {
		pos += left * speed;
	}
	if (input.Down) {
		pos -= up * speed;
	}
	if (input.Up) {
		pos += up * speed;
	}
	Camera->SetPosition(pos);

	glm::vec3 dirPitch = glm::rotate(glm::vec3(0, 0, -1), float(input.Pitch) * RotateSpeed, glm::vec3(1, 0, 0));
	glm::vec3 dirYaw = glm::rotate(dirPitch, float(input.Yaw) * RotateSpeed, glm::vec3(0, 1, 0));

	glm::vec3 upPitch = glm::rotate(glm::vec3(0, 1, 0), float(input.Pitch) * RotateSpeed, glm::vec3(1, 0, 0));
	glm::vec3 upYaw = glm::rotate(upPitch, float(input.Yaw) * RotateSpeed, glm::vec3(0, 1, 0));

	Camera->SetDirection(dirYaw);
	Camera->SetUp(upYaw);
	game->Window->SetCursorPos(game->Graphics->Settings.Width / 2, game->Graphics->Settings.Height / 2);
}