#include "PlayerView.hpp"
#include "../Actor.hpp"
#include "../Game.hpp"
#include <Base/Application/Math.hpp>
#include <Base/Application/Log.hpp>

#include "../MessageType.hpp"

namespace {
	const float Sensitivity = 0.003f;
	const float WalkSpeed = 3.0f;
	const float RunSpeed = 7.0f;
}


void PlayerView::InputContext::OnKeyDown(Keys::Key key) {
	switch (key) {
	case Keys::W:
		Forward = true;
		break;
	case Keys::S:
		Backward = true;
		break;
	case Keys::A:
		Left = true;
		break;
	case Keys::D:
		Right = true;
		break;
	case Keys::Shift:
		Sprint = true;
		break;
	case Keys::Space:
		Jump();
		break;
	case Keys::Escape:
		Quit();
		break;
	case Keys::Tilde:
		Console();
		Scoreboard = false;
		break;
	case Keys::Tab:
		Scoreboard = true;
		break;
	}
}


void PlayerView::InputContext::OnKeyUp(Keys::Key key) {
	switch (key) {
	case Keys::W:
		Forward = false;
		break;
	case Keys::S:
		Backward = false;
		break;
	case Keys::A:
		Left = false;
		break;
	case Keys::D:
		Right = false;
		break;
	case Keys::Shift:
		Sprint = false;
		break;
	case Keys::Tab:
		Scoreboard = false;
		break;
	}
}


void PlayerView::InputContext::OnMouseDown(Mouse::Button button, int, int) {
	if (button == Mouse::LeftButton) {
		Fire();
	};
}


void PlayerView::InputContext::OnMouseMove(int x, int y) {
	if (y != centerY || x != centerX) {
		needsReset = true;
	}
	Pitch = Clamp(Pitch + (y - centerY), -int(M_PI_2 * 0.9 / Sensitivity), int(M_PI_2 * 0.9 / Sensitivity));
	Yaw = Wrap(Yaw + (centerX - x), -int(M_PI / Sensitivity), int(M_PI / Sensitivity));
}


void PlayerView::InputContext::ResetMouse(Game* game) {
	if (needsReset) {
		game->Window->SetCursorPos(centerX, centerY);
		needsReset = false;
	}
}


PlayerView::PlayerView(::Actor* actor) :
	Actor(actor) {
}


void PlayerView::Initialize(Game* game) {
	int centerX = game->Graphics->Settings.Width / 2;
	int centerY = game->Graphics->Settings.Height / 2;
	input.SetMouseCenter(centerX, centerY);
	input.Jump += [this]() {
		this->Actor->Jump();
	};
	input.Quit += [this]() {
		Quit();
	};
	input.Console += [game]() {
		game->ShowConsole();
	};
	input.Fire += [this, game]() {
		glm::vec3 dir = Direction();
		glm::vec3 start = Actor->Body->GetEyePosition() + dir;
		game->ShootBullet(start, dir);
	};

	game->Window->SetCursorPos(centerX, centerY);
	game->Window->PushInputContext(&input);

	Camera = new ::Camera(glm::vec3(), glm::vec3(), glm::vec3(0, 1, 0));
	Camera->SetResolution(game->Graphics->Settings.Width, game->Graphics->Settings.Height);

	UpdateCameraPosition();
}


void PlayerView::Update(Game* game) {
	velocity = glm::vec3(
		(input.Left ? 1.0f : 0.0f) + (input.Right ? -1.0f : 0.0f), 0.0f,
		(input.Backward ? -1.0f : 0.0f) + (input.Forward ? 1.0f : 0.0f));

	if (glm::length2(velocity) > 0) {
		velocity = glm::normalize(velocity) * (input.Sprint ? RunSpeed : WalkSpeed);
	}
	Actor->SetVelocity(glm::rotateY(velocity, float(input.Yaw) * Sensitivity));

	UpdateCameraPosition();
	input.ResetMouse(game);
}


void PlayerView::Shutdown(Game* game) {
	game->Window->PopInputContext(&input);
}


glm::vec3 PlayerView::Direction() {
	glm::vec3 pitched = glm::rotateX(glm::vec3(0, 0, 1), float(input.Pitch) * Sensitivity);
	return glm::rotateY(pitched, float(input.Yaw) * Sensitivity);
}


void PlayerView::UpdateCameraPosition() {
	glm::vec3 eyeDir = Direction();
	glm::vec3 eyePos = Actor->Body->GetEyePosition();
	Camera->SetPosition(eyePos);
	Camera->SetDirection(eyeDir);
}


bool PlayerView::ShowScoreboard() const {
	return input.Scoreboard;
}