#pragma once
#include "../../Includes.hpp"

#include "IGameView.hpp"
#include <Base/Graphics/Camera.hpp>
#include <Base/Application/Inputs.hpp>
#include <Base/Application/Math.hpp>
#include <Base/Application/Event.hpp>
#include "../../Data/DataTypes.hpp"

class Actor;

class PlayerView : public IGameView {

	class InputContext : public IInputContext {
		int centerX, centerY;
		bool needsReset;
	public:
		bool Forward, Backward, Left, Right, Sprint, Scoreboard;
		int Pitch, Yaw;

		Event<> Jump;
		Event<> Quit;
		Event<> Console;
		Event<> Fire;

		InputContext() : Forward(false), Backward(false), Left(false), Right(false), Sprint(false),
			Pitch(0), Yaw(0), needsReset(true) {
		}

		void SetMouseCenter(int x, int y) {
			centerX = x;
			centerY = y;
		}

		void OnKeyDown(Keys::Key key);
		void OnKeyUp(Keys::Key key);
		void OnMouseMove(int x, int y);
		void OnMouseDown(Mouse::Button button, int x, int y);

		void ResetMouse(Game* game);
	} input;

	glm::vec3 velocity;

	void UpdateCameraPosition();

public:

	Actor* Actor;
	Camera* Camera;

	Event<> Quit;

	PlayerView(::Actor* actor);

	glm::vec3 Direction();

	virtual void Initialize(Game* game);
	virtual void Update(Game* game);
	virtual void Shutdown(Game* game);

	bool ShowScoreboard() const;

};