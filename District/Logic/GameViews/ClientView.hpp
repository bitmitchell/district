#pragma once
#include "../../Includes.hpp"
#include "IGameView.hpp"
#include <Base/Network/Network.hpp>
#include <Base/Application/Timer.hpp>
#include "../../Data/Database.hpp"
#include "../GameObject.hpp"

class ClientView : public IGameView {

	Database<PropData> propTypes;

	void HandleObjectUpdate(Message& msg, Game* game);
	void HandlePing(Message& msg, Game* game);
	void HandleScoreboardUpdate(Message& msg, Game* game);
	void HandleHostShootsBullet(Message& msg, Game* game);
	void HandleHostDeletesObject(Message& msg, Game* game);

	void CreateProp(Message& msg, Game* game, ObjectID id, std::string propType);
	void CreateActor(Message& msg, Game* game, ObjectID id);

public:

	void Initialize(Game* game);
	void Update(Game* game);
	void Shutdown(Game* game);
};