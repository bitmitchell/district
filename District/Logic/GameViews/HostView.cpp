#include "HostView.hpp"
#include "../Game.hpp"
#include <Base\Application\Log.hpp>

namespace {
	const float TickRate = 30.0f;
	const float UpdatePeriod = 1.0f / TickRate;
	const float PingPeriod = 0.5f;
	const float ScorePeriod = 1.0f;
	const float PingExponentialWeight = 0.25f;

	const float WalkSpeed = 3.0f;
	const float RunSpeed = 7.0f;
}

void HostView::UpdateActorWithInput(uint8_t input, float yaw, Actor* actor) {
	if (actor == nullptr) {
		Log::Warning("Tried to update a null actor with client input, maybe object ID mismatch?");
		return;
	}

	glm::vec3 velocity = glm::vec3(
		(BIT_ISSET(input, 3) ? 1.0f : 0.0f) + (BIT_ISSET(input, 4) ? -1.0f : 0.0f), 0.0f,
		(BIT_ISSET(input, 2) ? -1.0f : 0.0f) + (BIT_ISSET(input, 1) ? 1.0f : 0.0f));

	if (glm::length2(velocity) > 0) {
		velocity = glm::normalize(velocity) * (BIT_ISSET(input, 6) ? RunSpeed : WalkSpeed);
	}

	if (BIT_ISSET(input, 5)) {
		actor->Jump();
	}

	actor->SetVelocity(glm::rotateY(velocity, yaw));
}

void HostView::Initialize(Game* game) {
	game->Network->MessageReceived += [this, game](Message msg) {
		MessageTypeSize messageType;
		msg >> messageType;
		
		switch (messageType) {
		case ClientInput:
			HandleClientInput(msg, game);
			break;
		case Ping:
			HandlePing(msg, game);
			break;
		case ClientPlayerInfo:
			HandleClientPlayerInfo(msg, game);
			break;
		case ClientShootsBullet:
			HandleClientShootsBullet(msg, game);
			break;
		}
	};


	game->Network->ClientConnected += [game](PlayerID id) {
		game->Players.emplace(id, PlayerID(id));
	};

	game->Network->ClientDisconnected += [game](PlayerID id) {
		ObjectID actorID = game->Players[id].ActorID;
		if (actorID != InvalidObject) {
			Actor* actor = reinterpret_cast<Actor*>(game->GetObject(actorID));
			game->Physics->Remove(actor->Body);
			game->RemoveObject(actorID);
		}
		game->Players.erase(id);
	};
}


void HostView::HandleClientShootsBullet(Message& msg, Game* game) {
	glm::vec3 pos, dir;
	msg >> pos >> dir;
	game->ShootBullet(pos, dir);
}


void HostView::HandleClientPlayerInfo(Message& msg, Game* game) {
	PlayerID id;
	std::string name;
	msg >> id >> name;
	game->Players[id].Name = name;
	Log::Info("Received id %%% name %%%", id, name);
}


void HostView::HandleClientInput(Message& msg, Game* game) {
	PlayerID id;
	msg >> id;

	if (game->Players[id].ActorID != InvalidObject) {
		uint8_t movedata;
		float yaw;
		msg >> movedata >> yaw;
		Actor* actor = dynamic_cast<Actor*>(game->GetObject(game->Players[id].ActorID));
		UpdateActorWithInput(movedata, yaw, actor);
	}
}


void HostView::HandlePing(Message& msg, Game* game) {
	PlayerID id;
	float oldTime;
	msg >> id >> oldTime;

	PlayerInfo& info = game->Players[id];

	float ping = (float(game->Time.Time()) - oldTime);
	float diff = ping - info.Ping;
	float incr = PingExponentialWeight * diff;
	info.Ping += incr;
	info.Jitter = (1 - PingExponentialWeight) * info.Jitter + PingExponentialWeight * diff * diff;
}


void HostView::Update(Game* game) {
	if (updateTime.Time() > UpdatePeriod) {
		updateTime.Reset();

		std::unordered_map<ObjectID, GameObject*>& objects = game->GetObjects();
		for (auto& idObject : objects) {
			Message* msg = idObject.second->GetUpdateMessage(game);
			if (msg) {
				game->Network->Send(*msg, Network::Updates);
				delete msg;
			}
		}
	}

	if (pingTime.Time() > PingPeriod) {
		pingTime.Reset();

		Message msg(6);
		msg << MessageTypeSize(Ping);
		msg << float(game->Time.Time());
		game->Network->Send(msg, Network::Updates);

		for (auto& idPlayer : game->Players) {
			const Network::Connection* connection = game->Network->GetConnection(idPlayer.first);
			if (connection) {
				idPlayer.second.PacketLoss = float(connection->Peer->packetLoss) / float(ENET_PEER_PACKET_LOSS_SCALE);
			}
		}
	}

	if (scoreTime.Time() > ScorePeriod) {
		scoreTime.Reset();

		const std::unordered_map<PlayerID, PlayerInfo>& players = game->Players;
		int sizeOfStrings = 0;
		for (auto& idPlayer : players) {
			sizeOfStrings += idPlayer.second.Name.size() + 2;
		}

		Message msg(4 + players.size() * 21 + sizeOfStrings);
		msg << MessageTypeSize(ScoreboardUpdate);
		msg << uint16_t(players.size());
		
		for (auto& idPlayer : players) {
			msg << idPlayer.second.NetworkID;
			msg << idPlayer.second.ActorID;
			msg << idPlayer.second.Ping;
			msg << idPlayer.second.PacketLoss;
			msg << idPlayer.second.Jitter;
			msg << idPlayer.second.Name;
			msg << uint8_t(idPlayer.second.Team);
		}

		game->Network->Send(msg, Network::Updates);
	}
}


void HostView::Shutdown(Game* game) {
}
