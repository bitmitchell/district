#pragma once
#include "../../Includes.hpp"

#include "IGameView.hpp"
#include <Base/Graphics/Camera.hpp>
#include <Base/Application/Inputs.hpp>
#include <Base/Application/Math.hpp>
#include <Base/Application/Event.hpp>
#include <Base/Application/Timer.hpp>
#include "../../Data/DataTypes.hpp"
#include "../GameObject.hpp"

class Actor;

class ClientPlayerView : public IGameView {

	class ClientInputContext : public IInputContext {
		int centerX, centerY;
		bool mouseNeedsReset;
	public:
		bool Forward, Backward, Left, Right, Sprint, Jumping, Scoreboard;
		double Pitch, Yaw;

		Event<> Quit;
		Event<> Console;
		Event<> Fire;

		ClientInputContext() : Forward(false), Backward(false), Left(false), Right(false), Sprint(false),
			Pitch(0.5), Yaw(0.0) {
		}

		void SetMouseCenter(int x, int y) {
			centerX = x;
			centerY = y;
		}

		void OnKeyDown(Keys::Key key);
		void OnKeyUp(Keys::Key key);
		void OnMouseMove(int x, int y);
		void OnMouseDown(Mouse::Button button, int x, int y);
		void ResetMouse(Game* game);
	} input;

	BulletData bulletType;

	glm::vec3 velocity;
	Timer updateTime;

	void UpdateCameraPosition();
	glm::vec3 Direction();
public:
	ObjectID ActorID;
	Actor* Actor;
	Camera* Camera;

	Event<> Quit;

	ClientPlayerView(ObjectID actorID) : ActorID(actorID) {}
	
	virtual void Initialize(Game* game);
	virtual void Update(Game* game);
	virtual void Shutdown(Game* game);

	bool ShowScoreboard() const;

};