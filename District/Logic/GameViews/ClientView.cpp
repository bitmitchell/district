#include "ClientView.hpp"
#include "../Game.hpp"
#include "../MessageType.hpp"
#include <Base/Application/Log.hpp>
#include "../Actor.hpp"
#include "../Prop.hpp"
#include "../GameStates/MainMenu.hpp"

namespace {
	const float GameTimeMaxError = 0.15f;
}

void ClientView::Initialize(Game* game) {
	propTypes = Database<PropData>::LoadFromDDF("Data/props.ddf");

	game->Network->MessageReceived += [game, this](Message msg) {
		MessageTypeSize messageType;
		msg >> messageType;

		switch (messageType) {
		case ObjectUpdate:
			HandleObjectUpdate(msg, game);
			break;
		case Ping:
			HandlePing(msg, game);
			break;
		case ScoreboardUpdate:
			HandleScoreboardUpdate(msg, game);
			break;
		case HostShootsBullet:
			HandleHostShootsBullet(msg, game);
			break;
		case HostDeletesObject:
			HandleHostDeletesObject(msg, game);
			break;
		}
	};

	game->Network->Disconnected += [game]() {
		game->SetNewState(new MainMenu);
	};
}


void ClientView::HandleHostDeletesObject(Message& msg, Game* game) {
	ObjectID id;
	msg >> id;
	Actor* actor = dynamic_cast<Actor*>(game->GetObject(id));
	if (actor) {
		game->Physics->Remove(actor->Body);
	}
	game->RemoveObject(id);
}


void ClientView::HandleHostShootsBullet(Message& msg, Game* game) {
	glm::vec3 pos, dir;
	msg >> pos >> dir;
	game->ShootBullet(pos, dir, true);
}


void ClientView::HandleScoreboardUpdate(Message& msg, Game* game) {
	std::unordered_map<PlayerID, PlayerInfo> players;

	uint16_t count;
	PlayerID networkId;
	ObjectID actorId;
	float ping, packetLoss, jitter;
	std::string name;
	uint8_t team;

	msg >> count;

	for (int i = 0; i < count; ++i) {
		msg >> networkId >> actorId >> ping >> packetLoss >> jitter >> name >> team;
		players[networkId] = PlayerInfo(networkId, actorId, ping, packetLoss, jitter, name, (Team)team);
	}

	game->Players = players;
}


void ClientView::HandleObjectUpdate(Message& msg, Game* game) {
	ObjectTypeSize objType;
	ObjectID id;
	std::string propType;
	msg >> objType >> id;
	if (objType == ObjectProp) {
		msg >> propType;
	}

	GameObject* object = game->GetObject(id);
	if (object == nullptr) {
		switch (objType) {
		case ObjectActor:
			CreateActor(msg, game, id);
			return;
		case ObjectProp:
			CreateProp(msg, game, id, propType);
			return;
		}
	}

	if (object) {
		object->HandleUpdateMessage(&msg, game);
	}
}


void ClientView::CreateActor(Message& msg, Game* game, ObjectID id) {
	glm::vec3 position, velocity;
	float time;
	msg >> time;
	msg >> position;
	msg >> velocity;

	game->SpawnActor(InvalidPlayer, position);
}


void ClientView::CreateProp(Message& msg, Game* game, ObjectID id, std::string propType) {
	glm::vec3 pos, vel;
	glm::quat rot;
	float time;
	msg >> time;
	msg >> pos;
	msg >> vel;
	msg >> rot;

	Prop* prop = Prop::Create(id, pos, propType, propTypes[propType]);
	prop->Initialize(game);
	game->AddObject(prop);
}


void ClientView::HandlePing(Message& msg, Game* game) {
	float time;
	msg >> time;
	if (std::abs(game->Time.Time() - time) > GameTimeMaxError * 0.5f) {
		game->Time.SetTo(time);
	}

	Message response(10);
	response << MessageTypeSize(Ping);
	response << game->Network->ID();
	response << time;

	game->Network->Send(response, Network::Updates);
}


void ClientView::Update(Game* game) {
	std::unordered_map<ObjectID, GameObject*> objects = game->GetObjects();
	for (auto& idObject : objects) {
		idObject.second->UpdateClientPosition(game);
	}
}


void ClientView::Shutdown(Game* game) {
}