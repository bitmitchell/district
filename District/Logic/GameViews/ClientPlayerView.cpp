#include "ClientPlayerView.hpp"
#include "../Actor.hpp"
#include "../Game.hpp"
#include <Base/Application/Math.hpp>
#include <Base/Application/Log.hpp>

#include "../MessageType.hpp"

namespace {
	const double Sensitivity = 0.003;
	const float TickRate = 30.0f;
	const float UpdateRate = 1.0f / TickRate;
}


void ClientPlayerView::ClientInputContext::OnKeyDown(Keys::Key key) {
	switch (key) {
	case Keys::W:
		Forward = true;
		break;
	case Keys::S:
		Backward = true;
		break;
	case Keys::A:
		Left = true;
		break;
	case Keys::D:
		Right = true;
		break;
	case Keys::Shift:
		Sprint = true;
		break;
	case Keys::Space:
		Jumping = true;
		break;
	case Keys::Escape:
		Quit();
		break;
	case Keys::Tilde:
		Console();
		Scoreboard = false;
		break;
	case Keys::Tab:
		Scoreboard = true;
		break;
	}
}


void ClientPlayerView::ClientInputContext::OnKeyUp(Keys::Key key) {
	switch (key) {
	case Keys::W:
		Forward = false;
		break;
	case Keys::S:
		Backward = false;
		break;
	case Keys::A:
		Left = false;
		break;
	case Keys::D:
		Right = false;
		break;
	case Keys::Shift:
		Sprint = false;
		break;
	case Keys::Space:
		Jumping = false;
		break;
	case Keys::Tab:
		Scoreboard = false;
		break;
	}
}


void ClientPlayerView::ClientInputContext::OnMouseDown(Mouse::Button button, int, int) {
	if (button == Mouse::LeftButton) {
		Fire();
	};
}


void ClientPlayerView::ClientInputContext::OnMouseMove(int x, int y) {
	if (x != centerX || y != centerY) {
		mouseNeedsReset = true;
		Pitch = Clamp(Pitch + double(y - centerY) * Sensitivity, -M_PI_2 * 0.9, M_PI_2 * 0.9);
		Yaw = Wrap(Yaw + double(centerX - x) * Sensitivity, -M_PI, M_PI);
	}
}


void ClientPlayerView::ClientInputContext::ResetMouse(Game* game) {
	if (mouseNeedsReset) {
		mouseNeedsReset = false;
		game->Window->SetCursorPos(centerX, centerY);
	}
}


void ClientPlayerView::Initialize(Game* game) {
	int centerX = game->Graphics->Settings.Width / 2;
	int centerY = game->Graphics->Settings.Height / 2;
	input.SetMouseCenter(centerX, centerY);

	input.Quit += [this]() {
		Quit();
	};

	input.Console += [game]() {
		game->ShowConsole();
	};

	input.Fire += [this, game]() {
		glm::vec3 dir = Direction();
		glm::vec3 start = Actor->Body->GetEyePosition() + dir;
		game->ShootBullet(start, dir);
	};

	game->Window->SetCursorPos(centerX, centerY);
	game->Window->PushInputContext(&input);

	Camera = new ::Camera(glm::vec3(), glm::vec3(), glm::vec3(0, 1, 0));
	Camera->SetResolution(game->Graphics->Settings.Width, game->Graphics->Settings.Height);

	UpdateCameraPosition();
}


void ClientPlayerView::Update(Game* game) {
	if (!game->Network->IsHost() && updateTime.Time() > UpdateRate) {
		updateTime.Reset();

		uint8_t move = 0;
		BIT_SET(move, 1, input.Forward);
		BIT_SET(move, 2, input.Backward);
		BIT_SET(move, 3, input.Left);
		BIT_SET(move, 4, input.Right);
		BIT_SET(move, 5, input.Jumping);
		BIT_SET(move, 6, input.Sprint);

		Message msg = Message(11);
		msg << MessageTypeSize(ClientInput);
		msg << game->Network->ID();
		msg << move;
		msg << float(input.Yaw);
		game->Network->Send(msg, Network::Updates);
	}

	if (ActorID != InvalidObject) {
		Actor = dynamic_cast<::Actor*>(game->GetObject(ActorID));
	}

	UpdateCameraPosition();
	input.ResetMouse(game);
}


void ClientPlayerView::Shutdown(Game* game) {
	game->Window->PopInputContext(&input);
}


glm::vec3 ClientPlayerView::Direction() {
	glm::vec3 pitched = glm::rotateX(glm::vec3(0, 0, 1), float(input.Pitch));
	return glm::rotateY(pitched, float(input.Yaw));
}


void ClientPlayerView::UpdateCameraPosition() {
	glm::vec3 eyeDir = Direction();
	Camera->SetDirection(eyeDir);
	if (Actor) {
		glm::vec3 eyePos = Actor->Body->GetEyePosition();
		Camera->SetPosition(eyePos);;
	}
}

bool ClientPlayerView::ShowScoreboard() const {
	return input.Scoreboard;
}