#pragma once
#include "../Includes.hpp"
#include <Base/Application/Window.hpp>
#include <Base/Graphics/Graphics.hpp>
#include <Base/Network/Network.hpp>
#include <Base/Physics/World.hpp>
#include <Base/Application/Timer.hpp>
#include <Base/Audio/Audio.hpp>
#include "State.hpp"
#include "GameObject.hpp"
#include "GameViews\IGameView.hpp"
#include "PlayerInfo.hpp"
#include "Bullet.hpp"
#include "../Data/Database.hpp"

class Actor;

class Game {
	IState* currentState, *nextState;

	std::unordered_map<ObjectID, GameObject*> objects;
	std::unordered_set<IGameView*> views;
	std::unordered_set<Bullet*> bullets;

	Database<BulletData> bulletTypes;

	Timer deltaTime;
	float updateTime;

	bool running;

	void Update();

	void MoveToNewGameState();

public:

	struct {
		std::string PlayerName;
		int ResolutionX, ResolutionY;
	} Settings;

	Event<> ShowConsole;

	Graphics* Graphics;
	Window* Window;
	World* Physics;
	Network* Network;
	Audio* Audio;

	Timer Time;
	std::unordered_map<PlayerID, PlayerInfo> Players;

	Game();
	~Game();

	void Close();

	void Initialize();
	void Run();
	void Shutdown();

	void SetNewState(IState*state);

	void Reset();
	void AddObject(GameObject* object);
	void AddGameView(IGameView* view);
	void RemoveGameView(IGameView* view);

	GameObject* GetObject(ObjectID id);
	std::unordered_map<ObjectID, GameObject*>& GetObjects();
	void RemoveObject(ObjectID id);

	void ShootBullet(glm::vec3 position, glm::vec3 dir, bool clientOverride = false);
	std::unordered_set<Bullet*>& GetBullets();
	void ClearBulletts();

	float GetUpdateTime();

	std::vector<PlayerID> GetTeam(Team team);

	Actor* SpawnActor(PlayerID id, glm::vec3 position);
};