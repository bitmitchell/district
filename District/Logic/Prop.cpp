#include <Base/Network/Network.hpp>
#include "Prop.hpp"
#include "MessageType.hpp"
#include "Game.hpp"
#include <Base/Application/Log.hpp>

Prop* Prop::Create(ObjectID id, glm::vec3 position, std::string type, const PropData& data) {
	Prop* p = new Prop(id);
	p->position = position;
	p->type = type;
	p->data = &data;
	p->positionExtrapolator.Reset(0, 0, position);
	p->rotationExtrapolator.Reset(0, 0, glm::vec4());
	return p;
}

void Prop::Initialize(Game* game) {
	Mesh = Geometry::CreateFromBDM(game->Graphics, data->Mesh);
	Material.Color = Texture2D::FromFile(game->Graphics, data->Texture);
	Material.Properties = Texture2D::FromFile(game->Graphics, data->Properties);

	if (game->Physics) {
		switch (data->Physics.Type) {
		case PhysicsBodyType::Box:
			Physics = Body::Box(data->Physics.Box.HalfSize, position, data->Physics.Mass);
			break;
		case PhysicsBodyType::Sphere:
			Physics = Body::Sphere(data->Physics.Sphere.Radius, position, data->Physics.Mass);
			break;
		}

		if (Physics) {
			game->Physics->Add(Physics);
		}
	}
}

glm::vec3 Prop::Position() const {
	if (Physics) {
		return Physics->Position();
	} else {
		return position;
	}
}


glm::quat Prop::Rotation() const {
	if (Physics) {
		return Physics->Rotation();
	} else {
		return rotation;
	}
}


glm::vec3 Prop::Velocity() const {
	if (Physics) {
		return Physics->Velocity();
	} else {
		return velocity;
	}
}


Message* Prop::GetUpdateMessage(Game* game) {
	Message* msg = new Message(53 + type.size());
	*msg << MessageTypeSize(ObjectUpdate);
	*msg << ObjectTypeSize(ObjectProp);
	*msg << ID;
	*msg << type;
	*msg << float(game->Time.Time());
	*msg << Position();
	*msg << Physics->Velocity();
	*msg << Rotation();
	return msg;
}


void Prop::HandleUpdateMessage(Message* msg, Game* game) {
	glm::vec3 pos, vel;
	glm::quat rot;
	float time;
	*msg >> time;
	*msg >> pos;
	*msg >> vel;
	*msg >> rot;
	positionExtrapolator.AddSample(time, float(game->Time.Time()), pos, vel);
	rotationExtrapolator.AddSample(time, float(game->Time.Time()), glm::vec4(rot.x, rot.y, rot.z, rot.w));
}


void Prop::UpdateClientPosition(Game* game) {
	glm::vec4 rot;
	positionExtrapolator.ReadPosition(float(game->Time.Time()), position, velocity);
	rotationExtrapolator.ReadPosition(float(game->Time.Time()), rot);
	rotation = glm::normalize(glm::quat(rot.w, rot.x, rot.y, rot.z));

	if (Physics) {
		Physics->SetPosition(position);
		Physics->SetVelocity(glm::vec3());
		Physics->SetRotation(rotation);
	}
}