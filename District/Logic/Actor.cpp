#include "Actor.hpp"
#include "MessageType.hpp"
#include "Game.hpp"
#include <Base/Application/Log.hpp>


Actor* Actor::Create(ObjectID id, glm::vec3 position) {
	Actor* actor = new Actor(id);
	actor->Body = Character::Create(position);
	actor->positionExtrapolator.Reset(0, 0, position);
	actor->health = 100;
	return actor;
}


void Actor::TakeDamage(float damage) {
	health -= damage;
	if (health <= 0) {
		Death(this);
	}
}


float Actor::GetHealth() {
	return health;
}


glm::vec3 Actor::Position() {
	return Body->Position();
}


void Actor::Jump() {
	Body->Jump();
}


void Actor::SetVelocity(glm::vec3 vel) {
	Body->SetTargetVelocity(glm::vec2(vel.x, vel.z));
}


bool Actor::IsGrounded() {
	return Body->IsGrounded();
}


Message* Actor::GetUpdateMessage(Game* game) {
	Message* msg = new Message(39);
	*msg << MessageTypeSize(ObjectUpdate);
	*msg << ObjectTypeSize(ObjectActor);
	*msg << ID;
	*msg << float(game->Time.Time());
	*msg << Position();
	*msg << Body->Velocity();
	*msg << health;
	return msg;
}


void Actor::HandleUpdateMessage(Message* msg, Game* game) {
	glm::vec3 position, velocity;
	float time;
	*msg >> time;
	*msg >> position;
	*msg >> velocity;
	*msg >> health;
	positionExtrapolator.AddSample(time, float(game->Time.Time()), position, velocity);
}


void Actor::UpdateClientPosition(Game* game) {
	glm::vec3 position, velocity;
	positionExtrapolator.ReadPosition(float(game->Time.Time()), position, velocity);
	Body->SetPosition(position);
	Body->SetVelocity(velocity);
}