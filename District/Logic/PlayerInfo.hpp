#pragma once

#include "GameObject.hpp"
#include <Base/Network/Network.hpp>

enum Team {
	None,
	Red, // Defenders
	Blue // Attackers
};

struct PlayerInfo {
	ObjectID ActorID;
	PlayerID NetworkID;

	float Ping;
	float PacketLoss;
	float Jitter;

	std::string Name;
	Team Team;

	PlayerInfo() : NetworkID(InvalidPlayer), ActorID(InvalidObject), Ping(0.0f), PacketLoss(0.0f), Jitter(0.0f), Team(None) {}
	PlayerInfo(PlayerID id) : NetworkID(id), ActorID(InvalidObject), Ping(0.0f), PacketLoss(0.0f), Jitter(0.0f), Team(None) {}
	PlayerInfo(PlayerID id, ObjectID actor, float ping, float packetLoss, float jitter, std::string name, ::Team team) :
		NetworkID(id), ActorID(actor), Ping(ping), PacketLoss(packetLoss), Jitter(jitter), Name(name), Team(team) {
	}
};