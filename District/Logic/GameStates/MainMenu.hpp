#pragma once
#include "../State.hpp"
#include <Base/Graphics/PixelShader.hpp>
#include <Base/Graphics/VertexShader.hpp>

#include <Base/Graphics/Font/Font.hpp>
#include <Base/UI/Button.hpp>
#include <Base/UI/TextBox.hpp>
#include <Base/UI/IComponent.hpp>
#include <Base/UI/TickBox.hpp>
#include <Base/UI/VerticalStack.hpp>

#include <Base/Graphics/ScreenSpace.hpp>
#include <Base/Application/Window.hpp>


class MainMenu : public IState {
	class InputContext : public IInputContext {
	public:
		Event<int, int> MouseDown, MouseUp, MouseMove;
		Event<> Quit;
		void OnKeyDown(Keys::Key key);
		void OnMouseMove(int x, int y);
		void OnMouseDown(Mouse::Button button, int x, int y);
		void OnMouseUp(Mouse::Button button, int x, int y);
	} input;

	Style buttonStyle, inputStyle, tickStyle;

	VertexShader* vertex;
	PixelShader* shape;
	Font* font;
	VerticalStack* uiStack;

	void SetupInput(Game* game);
	void BuildMenu(Game* game);

public:
	virtual void Initialize(Game*);
	virtual void Update(Game*);
	virtual void Shutdown(Game*);

};