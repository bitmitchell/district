#pragma once
#include "../../Includes.hpp"
#include "../State.hpp"
#include "../GameViews/GhostCam.hpp"

#include <Base/Audio/Audio.hpp>
#include <Base/Physics/World.hpp>
#include <Base/Physics/Body.hpp>
#include <Base/Physics/Character.hpp>
#include <Base/Physics/Projectile.hpp>

#include <Base/Graphics/Buffer.hpp>
#include <Base/Graphics/PixelShader.hpp>
#include <Base/Graphics/VertexShader.hpp>
#include <Base/Graphics/Vertex.hpp>
#include <Base/Graphics/Geometry.hpp>
#include <Base/Graphics/Font/Font.hpp>
#include <Base/Graphics/Texture2D.hpp>
#include <Base/Application/Timer.hpp>

#include "../GameViews/PlayerView.hpp"
#include "../GameViews/ClientPlayerView.hpp"

#include "../Console.hpp"
#include "../../Rendering/RenderQueue.hpp"
#include "../../Rendering/Skybox.hpp"
#include "../Prop.hpp"
#include "../../Data/Database.hpp"
#include "../../Rendering/DeferredRenderer.hpp"

enum GameState {
	WaitingForPlayers,
	WaitingToStart,
	PlayingTheGame
};

class Playground : public IState {

	class InputContext : public IInputContext {
	public:
		bool Scoreboard;
		Event<> Quit;

		void OnKeyDown(Keys::Key key);
		void OnKeyUp(Keys::Key key);
	} spectateInput;

	Font *font;

	glm::vec3 flagPosition;

	DeferredRenderer renderer;
	Console console;

	bool showFrameTimes;

	Database<PropData> propTypes;

	PlayerView* player;
	ClientPlayerView* clientPlayer;
	Camera* spectateView;

	std::string localPlayerName;
	int requiredPlayers;
	GameState state;
	Timer gameStartTimer, roundOverTimer;
	Team previousWinner;

	void DrawScoreboard(Game* game);
	void GotoGameState(GameState state, Game* game);
	void UpdateGameState(Game* game);
	void SpawnPlayers(Game* game);
	void RemovePlayers(Game* game);

	void LoadResources(Game* game);
	void SetupSpectateInput(Game* game);

public:
	Playground(std::string playerName, int requiredPlayers) : player(nullptr), clientPlayer(nullptr), localPlayerName(playerName), requiredPlayers(requiredPlayers) {}

	virtual void Initialize(Game*);
	virtual void Update(Game*);
	virtual void Shutdown(Game*);

};