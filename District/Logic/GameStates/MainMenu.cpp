#include "MainMenu.hpp"
#include "../Game.hpp"
#include <Base/Application/Log.hpp>
#include <Base/Graphics/RenderTarget2D.hpp>
#include <Base/Network/Network.hpp>
#include "Playground.hpp"

#include "../GameViews/ClientView.hpp"
#include "../GameViews/HostView.hpp"


void MainMenu::InputContext::OnKeyDown(Keys::Key key) {
	if (key == Keys::Escape) {
		Quit();
	}
}


void MainMenu::InputContext::OnMouseMove(int x, int y) {
	MouseMove(x, y);
}


void MainMenu::InputContext::OnMouseDown(Mouse::Button button, int x, int y) {
	if (button == Mouse::LeftButton) {
		MouseDown(x, y);
	}
}


void MainMenu::InputContext::OnMouseUp(Mouse::Button button, int x, int y) {
	if (button == Mouse::LeftButton) {
		MouseUp(x, y);
	}
}


void MainMenu::Initialize(Game* game) {
	vertex = VertexShader::Create(game->Graphics, "Shaders/vertex.cso");
	shape = PixelShader::Create(game->Graphics, "Shaders/shape.cso");
	font = FontFactory::LoadFont(game->Graphics, "Fonts/Jura-Regular.ttf", 20);
	ScreenSpace::SetResolution(glm::vec2(game->Graphics->Settings.Width, game->Graphics->Settings.Height));
	ScreenSpace::SetShaders(vertex, shape);

	SetupInput(game);
	BuildMenu(game);
}


void MainMenu::BuildMenu(Game* game) {
	buttonStyle = Style(glm::vec4(0.0f), glm::vec4(0.5f), glm::vec4(0.2f),
						glm::vec4(1.0f), glm::vec4(0.7f), glm::vec4(0.7f));

	inputStyle = Style(glm::vec4(0.3f), glm::vec4(0.5f), glm::vec4(0.4f),
						glm::vec4(0.7f), glm::vec4(0.5f), glm::vec4(1.0f));

	tickStyle = Style(glm::vec4(0.3f), glm::vec4(0.5f), glm::vec4(0.4f),
					   glm::vec4(1.0f), glm::vec4(0.7f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));

	uiStack = VerticalStack::Create(glm::vec2(50, game->Graphics->Settings.Height - 350), 30);

	TextBox* playerName = TextBox::Create(font, glm::vec2(), glm::vec2(200, 20), inputStyle, game->Window);
	playerName->Text(game->Settings.PlayerName);
	playerName->SetCharacterLimit(10);

	TextBox* numberOfPlayers = TextBox::Create(font, glm::vec2(), glm::vec2(100, 20), inputStyle, game->Window);
	numberOfPlayers->Text("1");

	Button* hostButton = Button::Create("Host", font, glm::vec2(), glm::vec2(0, 20), buttonStyle);
	hostButton->Clicked += [game, playerName, numberOfPlayers]() {
		if (game->Network->Host(2514)) {
			game->AddGameView(new HostView);
			game->SetNewState(new Playground(playerName->Text(), atoi(numberOfPlayers->Text().c_str())));
		}
	};

	TextBox* input = TextBox::Create(font, glm::vec2(), glm::vec2(200, 20), inputStyle, game->Window);
	input->OnSubmit += [](std::string text) { Log::Info(text); };

	Button* joinButton = Button::Create("Join", font, glm::vec2(), glm::vec2(0, 20), buttonStyle);
	joinButton->Clicked += [game, input]() {
		game->Network->Connect(input->Text(), 2514);
	};

	game->Network->Connected += [game, playerName]() {
		game->AddGameView(new ClientView);
		game->SetNewState(new Playground(playerName->Text(), 0));
	};

	Button* quitButton = Button::Create("Quit", font, glm::vec2(), glm::vec2(0, 20), buttonStyle);
	quitButton->Clicked += [game]() { game->Close(); };

	Button* optionsButton = Button::Create("Options", font, glm::vec2(), glm::vec2(0, 20), buttonStyle);

	TickBox* tick = TickBox::Create(glm::vec2(), glm::vec2(20), tickStyle);

	uiStack->Add(playerName);
	uiStack->Add(numberOfPlayers);
	uiStack->Add(hostButton);
	uiStack->Add(input);
	uiStack->Add(joinButton);
	uiStack->AddSpacing();
	uiStack->Add(optionsButton);
	uiStack->AddSpacing();
	uiStack->Add(quitButton);
	uiStack->Add(tick);
}


void MainMenu::SetupInput(Game* game) {
	input.MouseMove += [this](int x, int y) {
		uiStack->TryMouseOver(x, y);
	};

	input.MouseDown += [this](int x, int y) {
		uiStack->TryClick(x, y);
	};

	input.MouseUp += [this](int x, int y) {
		uiStack->TryRelease(x, y);
	};

	input.Quit += [game]() {
		game->Close();
	};

	game->Window->PushInputContext(&input);
}


void MainMenu::Update(Game* game) {
	RenderTarget2D* target = game->Graphics->RenderTarget;
	target->Clear(game->Graphics, 0.0f, 0.0f, 0.0f, 1.0f);
	target->Apply(game->Graphics);
	
	uiStack->Draw(game->Graphics);
}



void MainMenu::Shutdown(Game* game) {
	game->Window->PopInputContext(&input);
}