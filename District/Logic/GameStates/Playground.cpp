#pragma once
#include "Playground.hpp"

#include <Base/Graphics/RenderTarget2D.hpp>
#include <Base/Graphics/Camera.hpp>
#include <Base/Graphics/Font/FontFactory.hpp>
#include <Base/Audio/Audio.hpp>
#include <Base/Application/Inputs.hpp>
#include <Base/Graphics/RenderState.hpp>
#include <Base/Application/Performance.hpp>
#include <Base/Application/Utilities.hpp>
#include "../../Generator/Generator.hpp"

#include "../GameViews/PlayerView.hpp"
#include "MainMenu.hpp"
#include "../Actor.hpp"
#include <Base/Graphics/ScreenSpace.hpp>
#include <Base/Network/Network.hpp>
#include "../MessageType.hpp"
#include "../../Rendering/Light.hpp"


namespace {
	const float PlayerColumn = 60;
	const float ActorColumn = 180;
	const float TeamColumn = 260;
	const float PingColumn = 335;
	const float JitterColumn = 410;
	const float PacketLossColumn = 500;

	const float GameStartTime = 5;
	const float RoundTime = 300;
}


void Playground::InputContext::OnKeyDown(Keys::Key key) {
	switch (key) {
	case Keys::Escape:
		Quit();
		break;
	case Keys::Tab:
		Scoreboard = true;
		break;
	}
}


void Playground::InputContext::OnKeyUp(Keys::Key key) {
	switch (key) {
	case Keys::Tab:
		Scoreboard = false;
		break;
	}
}


void Playground::Initialize(Game* game) {
	PerfFunction;

	renderer.Initialize(game);
	LoadResources(game);

	clientPlayer = nullptr;
	player = nullptr;
	previousWinner = None;
	flagPosition = glm::vec3(2.5f, 2.4f, 3.5f);

	game->Window->ShowCursor(false);

	spectateView = new Camera(glm::vec3(-50, 20, 50), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	spectateView->SetResolution(game->Graphics->Settings.Width, game->Graphics->Settings.Height);

	console.Initialize(font, game);
	console.AddCommand("quit", [game](std::vector<std::string> args) {
		game->Close();
	});

	console.AddCommand("show_fps", [this](std::vector<std::string> args) {
		if (args.size() != 1 || (args[0] != "1" && args[0] != "0")) {
			Log::Info("show_fps 1|0");
		} else {
			showFrameTimes = atoi(args[0].c_str()) == 1;
		}
	});

	GotoGameState(WaitingForPlayers, game);
	SetupSpectateInput(game);


	if (game->Network->IsHost()) {
		game->Players[0] = PlayerInfo(0, InvalidObject, 0, 0, 0, localPlayerName, None);

		game->Network->ClientConnected += [this, game](PlayerID id) {
			Message msg(12);
			msg << MessageTypeSize(SetGameState) << uint8_t(state);
			msg << (state == WaitingToStart ? gameStartTimer.Time() : roundOverTimer.Time());
			msg << uint8_t(previousWinner);
			game->Network->Send(msg, Network::Events);
		};
	} else {
		game->Network->MessageReceived += [this, game](Message msg) {
			MessageTypeSize type;
			msg >> type;
			if (type == SetGameState) {
				uint8_t state;
				double time;
				uint8_t prevWinner;
				msg >> state >> time >> prevWinner;
				previousWinner = (Team)prevWinner;
				GotoGameState((GameState)state, game);
				if (state == WaitingToStart) {
					gameStartTimer.SetTo(time);
				} else {
					roundOverTimer.SetTo(time);
				}
			} else if (type == HostCreatesActor) {
				PlayerID player;
				ObjectID object;
				msg >> player >> object;
				if (player == game->Network->ID()) {
					clientPlayer = new ClientPlayerView(object);
					game->AddGameView(clientPlayer);
					clientPlayer->Quit += [game]() {
						game->SetNewState(new MainMenu);
					};
				}
			} else if (type == HostDeletesObject) {
				ObjectID object;
				msg >> object;
				if (clientPlayer && object == clientPlayer->ActorID) {
					game->RemoveGameView(clientPlayer);
					delete clientPlayer;
					clientPlayer = nullptr;
				}
			}
		};

		game->Network->IDSet += [this, game]() {
			Message msg(8 + localPlayerName.size());
			msg << MessageTypeSize(ClientPlayerInfo);
			msg << game->Network->ID();
			msg << localPlayerName;
			Log::Info("Sending id %%% name %%%", game->Network->ID(), localPlayerName);
			game->Network->Send(msg, Network::Events);
		};
	}
}


void Playground::SetupSpectateInput(Game* game) {
	spectateInput.Quit += [game]() {
		game->SetNewState(new MainMenu);
	};

	game->Window->PushInputContext(&spectateInput);
}


void Playground::LoadResources(Game* game) {
	propTypes = Database<PropData>::LoadFromDDF("Data/props.ddf");

	font = FontFactory::LoadFont(game->Graphics, "Fonts/Jura-Regular.ttf", 20);
	
	Light* light = Light::CreatePoint(game->Graphics, glm::vec3(10, 25, 10), glm::vec3(), 200, { 1, 0, 0 }, 1024);
	light->SetFOV(float(M_PI / 4));
	renderer.AddLight(light);
	
	light = Light::CreatePoint(game->Graphics, glm::vec3(-10, 25, 10), glm::vec3(), 200, { 0, 0, 1 }, 1024);
	light->SetFOV(float(M_PI / 4));
	renderer.AddLight(light);

	light = Light::CreatePoint(game->Graphics, glm::vec3(0, 20, -20), glm::vec3(), 100, { 0, 1, 0 }, 1024);
	light->SetFOV(float(M_PI / 4));
	renderer.AddLight(light);
	
	light = Light::CreateDirection(game->Graphics, glm::vec3(1, -2, 1), 0.2f, { 1, 1, 1 }, 2048, 30.0f);
	renderer.AddLight(light);
	
}


void Playground::GotoGameState(GameState next, Game* game) {
	state = next;

	switch (state) {
	case WaitingForPlayers:
		RemovePlayers(game);
		if ((int)game->Players.size() >= requiredPlayers) {
			GotoGameState(WaitingToStart, game);
		}
		break;
	case WaitingToStart:
		gameStartTimer.Reset();
		break;
	case PlayingTheGame:
		if (game->Network->IsHost()) {
			for (int x = 7; x < 15; x += 3) {
				Prop* prop = Prop::Create(GameObject::GetFreeID(), glm::vec3(x, 2, 7), "Sphere", propTypes["sphere"]);
				prop->Initialize(game);
				game->AddObject(prop);
			}
		}
		roundOverTimer.Reset();
		SpawnPlayers(game);
		game->ClearBulletts();
		break;
	}

	if (game->Network->IsHost()) {
		Message msg(12);
		msg << MessageTypeSize(SetGameState) << uint8_t(state);
		msg << (state == WaitingToStart ? gameStartTimer.Time() : roundOverTimer.Time());
		msg << uint8_t(previousWinner);
		game->Network->Send(msg, Network::Events);
	}
}


void Playground::SpawnPlayers(Game* game) {
	if (!game->Network->IsHost()) {
		return;
	}

	int blues = 0, reds = 0;

	glm::vec3 redSpawn(2, 4, -5);
	glm::vec3 blueSpawn(-20, 4, 20);

	for (auto& idInfo : game->Players) {
		glm::vec3 pos;
		if (blues > reds) {
			idInfo.second.Team = Red;
			reds++;
			pos = redSpawn;
			redSpawn.z += 2;
		} else {
			idInfo.second.Team = Blue;
			blues++;
			pos = blueSpawn;
			blueSpawn.z += 2;
		}

		Actor* actor = game->SpawnActor(idInfo.first, pos);

		actor->Death += [game, this](Actor* actor) {
			for (auto& idInfo : game->Players) {
				if (idInfo.second.ActorID == actor->ID) {
					idInfo.second.ActorID = InvalidObject;
					idInfo.second.Team = None;
				}
			}

			if (actor == player->Actor) {
				game->RemoveGameView(player);
				delete player;
				player = nullptr;
			}

			game->Physics->Remove(actor->Body);
			game->RemoveObject(actor->ID);
		};

		if (idInfo.first == 0) {
			player = new PlayerView(actor);
			game->AddGameView(player);
			player->Quit += [game]() {
				game->SetNewState(new MainMenu);
			};
		}
	}
}


void Playground::RemovePlayers(Game* game) {
	if (!game->Network->IsHost()) {
		return;
	}

	for (auto& idInfo : game->Players) {
		if (idInfo.second.ActorID != InvalidObject) {
			Actor* actor = dynamic_cast<Actor*>(game->GetObject(idInfo.second.ActorID));
			game->Physics->Remove(actor->Body);
			game->RemoveObject(idInfo.second.ActorID);
			idInfo.second.ActorID = InvalidObject;
			idInfo.second.Team = None;
		}
	}

	if (player) {
		game->RemoveGameView(player);
		delete player;
		player = nullptr;
	}
}


void Playground::UpdateGameState(Game* game) {
	if (game->Network->IsHost()) {
		switch (state) {
		case WaitingForPlayers:
			if ((int)game->Players.size() >= requiredPlayers) {
				GotoGameState(WaitingToStart, game);
			}
			break;
		case WaitingToStart:
			if (gameStartTimer.Time() > GameStartTime) {
				GotoGameState(PlayingTheGame, game);
			}
			break;
		case PlayingTheGame:
			std::vector<PlayerID> blueTeam = game->GetTeam(Blue);

			bool won = false;
			if (blueTeam.size() == 0) {
				won = true;
				previousWinner = Red;
			} else {
				for (PlayerID player : blueTeam) {
					ObjectID actorID = game->Players[player].ActorID;
					Actor* actor = dynamic_cast<Actor*>(game->GetObject(actorID));
					if (actor && glm::length2(actor->Position() - glm::vec3(0, 0.8f, 0) - flagPosition) < 0.3f) {
						won = true;
						previousWinner = Blue;
					}
				}
			}

			if (roundOverTimer.Time() > RoundTime || won) {
				GotoGameState(WaitingForPlayers, game);
			}
			break;
		}
	}
}


void Playground::Update(Game* game) {
	PerfFunction;
	game->Graphics->StartTiming();

	UpdateGameState(game);

	Camera* camera = spectateView;
	if (player) {
		camera = player->Camera;
	} else if (clientPlayer) {
		camera = clientPlayer->Camera;
	}

	glm::vec3 cameraVelocity;
	if (clientPlayer && clientPlayer->Actor) {
		cameraVelocity = clientPlayer->Actor->Body->Velocity();
	} else if (player) {
		cameraVelocity = player->Actor->Body->Velocity();
	}
	game->Audio->SetListener(camera->GetPosition(), cameraVelocity, camera->GetUp(), camera->GetDirection());

	renderer.SetFlagPosition(flagPosition);
	renderer.Render(game, camera);

	RenderState::SetDepth(game->Graphics, RenderState::DepthMode::None);

	console.DrawConsole(game->Graphics);

	if (showFrameTimes) {
		font->Draw(game->Graphics, "Update Time: " + std::to_string(game->GetUpdateTime() * 1000) + "ms", game->Graphics->Settings.Width - 300.0f, 5, { 1, 0, 0, 1 });
		font->Draw(game->Graphics, "Render Time: " + std::to_string(game->Graphics->FrameTime() * 1000) + "ms", game->Graphics->Settings.Width - 300.0f, 25, { 1, 0, 0, 1 });
	}

	if ((clientPlayer && clientPlayer->ShowScoreboard()) || (player && player->ShowScoreboard()) || (!clientPlayer && !player && spectateInput.Scoreboard)) {
		DrawScoreboard(game);
	}

	std::string winnerMessage;
	if (previousWinner == Red) {
		winnerMessage = "Red Team Won!";
	} else if (previousWinner == Blue) {
		winnerMessage = "Blue Team Won!";
	}

	if (state == WaitingForPlayers) {
		std::string message = "Waiting For Players";
		float width = font->MeasureText(message);
		float x = (game->Graphics->Settings.Width - width) * 0.5f;
		font->Draw(game->Graphics, message, x, game->Graphics->Settings.Height / 2.0f, { 1, 0, 0, 1 });
		font->Draw(game->Graphics, winnerMessage, (game->Graphics->Settings.Width - font->MeasureText(winnerMessage)) * 0.5f, game->Graphics->Settings.Height / 2.0f + 30.0f, { 1, 0, 0, 1 });
	} else if (state == WaitingToStart) {
		std::string time = Utilites::Format("%%%", int(GameStartTime - gameStartTimer.Time()) + 1);
		float width = font->MeasureText(time);
		float x = (game->Graphics->Settings.Width - width) * 0.5f;
		font->Draw(game->Graphics, time, x, game->Graphics->Settings.Height / 2.0f, { 1, 0, 0, 1 });
		font->Draw(game->Graphics, winnerMessage, (game->Graphics->Settings.Width - font->MeasureText(winnerMessage)) * 0.5f, game->Graphics->Settings.Height / 2.0f + 30.0f, { 1, 0, 0, 1 });
	}

	Actor* playerActor = nullptr;
	if (clientPlayer) {
		playerActor = clientPlayer->Actor;
	} else if (player) {
		playerActor = player->Actor;
	}
	Team team = game->Players[game->Network->ID()].Team;
	glm::vec4 teamColor = (team == Red ? glm::vec4(1, 0, 0, 1) : glm::vec4(0, 0, 1, 1));
	if (playerActor) {
		font->Draw(game->Graphics, Utilites::Format("Health: %%%", playerActor->GetHealth()), 10, float(game->Graphics->Settings.Height) - 30, teamColor);
	}

	game->Graphics->EndTiming();
}


void Playground::DrawScoreboard(Game* game) {
	ScreenSpace::Rectangle(game->Graphics, glm::vec2(50, 50), glm::vec2(game->Graphics->Settings.Width - 100, game->Graphics->Settings.Height - 100), { 0.0f, 0.0f, 0.0f, 0.5f });
	float y = 100;
	font->Draw(game->Graphics, "Player:", PlayerColumn, 60, { 1, 0, 0, 1 });
	font->Draw(game->Graphics, "Actor:", ActorColumn, 60, { 1, 0, 0, 1 });
	font->Draw(game->Graphics, "Team:", TeamColumn, 60, { 1, 0, 0, 1 });
	font->Draw(game->Graphics, "Ping:", PingColumn, 60, { 1, 0, 0, 1 });
	font->Draw(game->Graphics, "Jitter:", JitterColumn, 60, { 1, 0, 0, 1 });
	font->Draw(game->Graphics, "Packet Loss:", PacketLossColumn, 60, { 1, 0, 0, 1 });

	for (auto& idPlayer : game->Players) {
		glm::vec4 color = { 1, 1, 1, 1 };
		if (idPlayer.first == game->Network->ID()) {
			color = { 0, 1, 0, 1 };
		}

		const PlayerInfo& info = idPlayer.second;
		std::string team;
		if (info.Team == Red) { team = "Red"; }
		if (info.Team == Blue) { team = "Blue"; }

		font->Draw(game->Graphics, Utilites::Format("%%%", info.Name), PlayerColumn, y, color);
		if (info.ActorID != InvalidObject) {
			font->Draw(game->Graphics, Utilites::Format("%%%", info.ActorID), ActorColumn, y, color);
		}
		font->Draw(game->Graphics, team, TeamColumn, y, color);
		font->Draw(game->Graphics, Utilites::Format("%%%ms", int(info.Ping * 1000)), PingColumn, y, color);
		font->Draw(game->Graphics, Utilites::Format("%%%ms", int(std::sqrtf(info.Jitter) * 1000)), JitterColumn, y, color);
		font->Draw(game->Graphics, Utilites::Format("%%%%", int(info.PacketLoss * 100)), PacketLossColumn, y, color);
		y += font->GetSize();
	}
}


void Playground::Shutdown(Game* game) {
	PerfFunction;

	renderer.Shutdown(game);
	delete spectateView;

	game->Network->Disconnect();
	game->Window->ShowCursor(true);
	game->Window->PopInputContext(&spectateInput);
	game->Reset();
}