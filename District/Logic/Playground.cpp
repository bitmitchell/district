#pragma once
#include "Playground.hpp"

#include <Base/Graphics/RenderTarget2D.hpp>
#include <Base/Graphics/Camera.hpp>
#include <Base/Graphics/Font/FontFactory.hpp>
#include <Base/Application/Inputs.hpp>
#include <Base/Graphics/RenderState.hpp>
#include <Base/Application/Performance.hpp>
#include "../Generator/Generator.hpp"
#include "GameViews/PlayerView.hpp"
#include "Actor.hpp"


void Playground::Initialize(Game* game) {
	PerfFunction;
	Generator gen;

	propTypes = Database<PropData>::LoadFromYAML("Data/props.yml");
	bulletTypes = Database<BulletData>::LoadFromYAML("Data/bullets.yml");
	BulletData type = bulletTypes["nato556"];

	audio = AudioEngine::Create();

	game->Window->ShowCursor(false);

	pixel = PixelShader::Create(game->Graphics, "Shaders/pixel.cso");
	shadow = PixelShader::Create(game->Graphics, "Shaders/shadow.cso");
	vertex = VertexShader::Create(game->Graphics, "Shaders/vertex.cso");

	model = Geometry::CreateFromBDM(game->Graphics, "Models/box.bdm");
	concrete = Texture2D::FromFile(game->Graphics, "Materials/concrete.png");

	font = FontFactory::LoadFont(game->Graphics, "Fonts/Jura-Regular.ttf", 20);

	light = Light::Create(game->Graphics, glm::vec3(-10, 10, -5), glm::vec3(), 1024);

	sky.Initialize(game->Graphics);

	floor = gen.Floor(game->Graphics);

	physics.Initialize();

	plane = Body::Plane(glm::vec3(0, 1, 0), 0);
	plane->SetFriction(3.0f);
	physics.Add(plane);

	player = new PlayerView(Actor::Create(glm::vec3(0, 4, 0)));
	player->Initialize(game);
	physics.Add(player->Actor->Body);

	glm::vec3 positions[] = {
		glm::vec3(-2, 5, -2),
		glm::vec3(2, 5, -2),
		glm::vec3(2, 5, 2),
		glm::vec3(2, 15, -2),
		glm::vec3(2, 15, 2),
		glm::vec3(-2, 5, 2)
	};

	for (const auto& pos : positions) {
		props.emplace_back(pos, propTypes["box"]);
		props.back().Initialize(game->Graphics, &physics);
	}

	console.Initialize(font, game);
	console.AddCommand("quit", [game](std::vector<std::string> args) {
		game->Close();
	});

	game->Window->OnMouseDown += [this, type](Mouse button, int x, int y) {
		if (button == Mouse::LeftButton) {
			glm::vec3 dir = player->Direction();
			glm::vec3 start = player->Actor->Position() + dir;
			bullets.push_back(new Bullet(start, dir, &physics, audio, &type, "Audio/tone.ogg"));
		};
	};
}


void Playground::Update(Game* game) {
	PerfFunction;
	game->Graphics->StartTiming();


	player->Update(game);

	Camera* camera = player->Camera;

	double dT = deltaTime.Time();
	deltaTime.Reset();

	physics.Update((float)dT);

	for (const auto& prop : props){
		scene.Queue(prop.Mesh, prop.Texture, glm::translate(prop.Position()) * glm::mat4_cast(prop.Rotation()));
	}

	for (const auto& projectile : physics.GetProjectiles()) {
		scene.Queue(model, concrete, glm::translate(projectile->Position()) * glm::scale(glm::vec3(0.5f, 0.5f, 0.5f)));
	}

	for (auto& bullet = bullets.begin(); bullet != bullets.end();){
		if ((*bullet)->IsAlive()){
			(*bullet)->Update(dT, player->Actor->Position());
			++bullet;
		}
		else{
			bullet = bullets.erase(bullet);
		}
	}

	scene.Queue(model, concrete, glm::translate(player->Actor->Position()) *
		glm::scale(glm::vec3(0.3f, 0.75f, 0.3f)));

	scene.Queue(floor, concrete, glm::mat4());

	game->Graphics->SetSceneConstants((float)time.Time() / 20.0f);


	// Render the light
	light->SetTarget(player->Actor->Position());
	light->Apply(game->Graphics);
	light->RenderTarget->Clear(game->Graphics, FLT_MAX, 1.0f, 1.0f, 1.0f);
	light->RenderTarget->Apply(game->Graphics);

	scene.Draw(game->Graphics, light, vertex, shadow);


	// Render the scene
	RenderTarget2D* target = game->Graphics->RenderTarget;
	target->Clear(game->Graphics, 0.0f, 1.0f, 0.0f, 1.0f);
	target->Apply(game->Graphics);

	game->Graphics->SetView(camera);

	RenderState::SetDepth(game->Graphics, RenderState::DepthMode::None);
	sky.Draw(game->Graphics);
	RenderState::SetDepth(game->Graphics, RenderState::DepthMode::ReadWrite);

	light->RenderTarget->GenerateMips(game->Graphics);
	light->SetAsShadowMap(game->Graphics);
	scene.Draw(game->Graphics, camera, vertex, pixel);

	scene.Clear();

	ID3D11ShaderResourceView* null = nullptr;
	game->Graphics->Context->PSSetShaderResources(1, 1, &null);

	console.DrawConsole(game->Graphics);

	font->Draw(game->Graphics, "Frame Time: " + std::to_string(game->Graphics->FrameTime() * 1000) + "ms", 5, 220, glm::vec4(1, 0, 0, 1));

	game->Graphics->EndTiming();
	audio->Update(camera);
}


void Playground::Shutdown(Game* game) {
	PerfFunction;
	floor->Release();
	light->Release();
}