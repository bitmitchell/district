#include "Game.hpp" 
#include <Base/Application/DataFormat.hpp>
#include <Base/Network/Network.hpp>
#include "GameStates/MainMenu.hpp"
#include "MessageType.hpp"
#include "../Generator/Generator.hpp"
#include "Actor.hpp"

namespace {
	const std::string SettingsFile("settings.ddf");
	const std::string SettingsVideo("video");
	const std::string VideoWidth("width");
	const std::string VideoHeight("height");
	const std::string SettingsPlayer("player");
	const std::string PlayerName("name");

	const std::string DefaultPlayerName("Player");

	const int DefaultWidth = 1280;
	const int DefaultHeight = 720;
}


Game::Game() : nextState(nullptr), currentState(nullptr) {
}


Game::~Game() {
}


void Game::Initialize() {
	Object settings = Object::FromFile(SettingsFile);
	Object video = settings[SettingsVideo];

	Window::Options options;
	
	options.PosX = CW_USEDEFAULT;
	options.PosY = CW_USEDEFAULT;
	options.Width = video[VideoWidth].As<int>();
	options.Height = video[VideoHeight].As<int>();

	Settings.ResolutionX = options.Width;
	Settings.ResolutionY = options.Height;
	Settings.PlayerName = settings[SettingsPlayer][PlayerName].As<std::string>();

	Window = new ::Window();
	Window->Create(options);

	Window->OnClose += [this]() {
		running = false;
	};

	Graphics = Graphics::Create(Window->GetHandle());
	Physics = World::Create();
	Network = Network::Create();
	Audio = Audio::Create();

	bulletTypes = Database<BulletData>::LoadFromDDF("Data/bullets.ddf");

	SetNewState(new MainMenu);

	Window->Show();
	running = true;
	deltaTime.Reset();
}


void Game::Close() {
	Window->Close();
}


void Game::Update() {
	Network->Update();

	updateTime = (float)deltaTime.Time();
	Physics->Update(updateTime);
	deltaTime.Reset();

	Audio->Update();

	for (IGameView* view : views) {
		view->Update(this);
	}
}


void Game::Run() {
	while (running) {
		MoveToNewGameState();

		Window->MessagePump();

		if (currentState) {
			currentState->Update(this);
		}

		Update();

		Graphics->Present();
	}
}


void Game::Shutdown() {
	Reset();
	if (currentState) {
		currentState->Shutdown(this);
		delete currentState;
		currentState = nullptr;
	}

	Graphics->Shutdown();
	Network->Shutdown();
}


void Game::SetNewState(IState* state) {
	if (nextState != nullptr) {
		delete nextState;
	}
	nextState = state;
}


void Game::MoveToNewGameState() {
	if (nextState) {
		if (currentState) {
			currentState->Shutdown(this);
			delete currentState;
			currentState = nullptr;
		}

		std::swap(currentState, nextState);
		currentState->Initialize(this);
	}
}


void Game::Reset() {
	ShowConsole.Clear();
	Network->ClearEvents();
	Window->ClearEvents();

	Window->OnClose += [this]() {
		running = false;
	};

	for (IGameView* view : views) {
		view->Shutdown(this);
		delete view;
	}
	views.clear();

	for (auto& object : objects) {
		delete object.second;
	}
	objects.clear();
	
	for (auto& bullet : bullets) {
		delete bullet;
	}
	bullets.clear();
	
	Players.clear();
	Physics->Reset();
	Audio->StopAllSounds();
}


void Game::AddObject(GameObject* object) {
	objects.insert(std::make_pair(object->ID, object));
}


void Game::AddGameView(IGameView* view) {
	view->Initialize(this);
	views.insert(view);
}


void Game::RemoveGameView(IGameView* view) {
	view->Shutdown(this);
	views.erase(view);
}


GameObject* Game::GetObject(ObjectID id) {
	if (objects.count(id) == 0) {
		return nullptr;
	}
	return objects[id];
}


std::unordered_map<ObjectID, GameObject*>& Game::GetObjects() {
	return objects;
}


void Game::RemoveObject(ObjectID id) {
	if (objects.count(id) == 0) {
		Log::Warning("Tried to remove object %%% that doesn't exist", id);
	}

	objects.erase(id);

	if (Network->IsHost()) {
		Message msg(6);
		msg << MessageTypeSize(HostDeletesObject) << id;
		Network->Send(msg, Network::Events);
	}
}


float Game::GetUpdateTime() {
	return updateTime;
}


void Game::ShootBullet(glm::vec3 position, glm::vec3 dir, bool clientOverride) {
	if (Network->IsHost() || clientOverride) {
		const BulletData& bulletType = bulletTypes["nato556"];
		bullets.insert(new Bullet(Physics->FireProjectile(position, dir, bulletType.Velocity, bulletType.Mass, bulletType.DragCoefficient, bulletType.Caliber)));

		Message msg(26);
		msg << MessageTypeSize(HostShootsBullet);
		msg << position << dir;
		Network->Send(msg, Network::Events);
	} else {
		Message msg(26);
		msg << MessageTypeSize(ClientShootsBullet);
		msg << position << dir;
		Network->Send(msg, Network::Events);
	}
}


void Game::ClearBulletts() {
	for (auto& bullet : bullets) {
		delete bullet;
	}
	bullets.clear();
}


std::unordered_set<Bullet*>& Game::GetBullets() {
	return bullets;
}


std::vector<PlayerID> Game::GetTeam(Team team) {
	std::vector<PlayerID> players;
	for (auto idInfo : Players) {
		if (idInfo.second.Team == team) {
			players.push_back(idInfo.first);
		}
	}
	return players;
}


Actor* Game::SpawnActor(PlayerID id, glm::vec3 position) {
	Actor* actor = Actor::Create(GameObject::GetFreeID(), position);
	AddObject(actor);
	Physics->Add(actor->Body);

	if (Network->IsHost()) {
		Players[id].ActorID = actor->ID;

		actor->Body->Shot += [actor](Projectile*) {
			actor->TakeDamage(35);
		};

		Message accept = Message(10);
		accept << MessageTypeSize(HostCreatesActor);
		accept << id;
		accept << actor->ID;
		Network->Send(accept, Network::Events);
	}

	return actor;
}