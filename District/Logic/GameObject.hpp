#pragma once
#include <cstdint>

class Message;
class Game;

enum ObjectType {
	ObjectActor,
	ObjectProp
};

typedef uint8_t ObjectTypeSize;

typedef unsigned int ObjectID;
#define InvalidObject UINT_MAX

class GameObject {
	static ObjectID nextFreeID;
public:
	ObjectID ID;

	static ObjectID GetFreeID() { return nextFreeID++; }

	GameObject(ObjectID id);

	virtual Message* GetUpdateMessage(Game* game) = 0;
	virtual void HandleUpdateMessage(Message* msg, Game* game) = 0;
	virtual void UpdateClientPosition(Game* game) = 0;
};