#pragma once
#include "../Includes.hpp"
#include <Base/Physics/Projectile.hpp>

class Bullet {
	Projectile* projectile;
public:

	Bullet(Projectile* proj) : projectile(proj) {
	}

	glm::vec3 Position() {
		return projectile->Position();
	}

	glm::vec3 Velocity() {
		return projectile->Velocity();
	}


};