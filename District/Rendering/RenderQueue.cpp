#include "RenderQueue.hpp"


void RenderQueue::Queue(Geometry* geometry, Material* material, const glm::mat4& transform) {
	renderQueue.push_back({
		geometry,
		material,
		transform });
}


void RenderQueue::Draw(Graphics* graphics, Camera* camera, VertexShader* vertex, PixelShader* pixel) {
	for (auto& element : renderQueue) {
		Transform world(camera->GetTransposeProjectionViewMatrix(), element.WorldTransform);
		graphics->Draw(element.Geometry, vertex, pixel, world, element.Material);
	}
}


void RenderQueue::Clear() {
	renderQueue.clear();
}