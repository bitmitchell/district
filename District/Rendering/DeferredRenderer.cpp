#include "DeferredRenderer.hpp"
#include <Base/Graphics/RenderState.hpp>
#include <Base/Graphics/Font/FontFactory.hpp>

#include "../Logic/Actor.hpp"
#include "../Logic/Prop.hpp"
#include "../Generator/Generator.hpp"



void DeferredRenderer::Initialize(Game* game) {
	pixel = PixelShader::Create(game->Graphics, "Shaders/pixel.cso");
	shadow = PixelShader::Create(game->Graphics, "Shaders/shadow.cso");
	vertex = VertexShader::Create(game->Graphics, "Shaders/vertex.cso");
	fullscreen = VertexShader::Create(game->Graphics, "Shaders/fullscreen.cso");
	point = PixelShader::Create(game->Graphics, "Shaders/point.cso");
	direction = PixelShader::Create(game->Graphics, "Shaders/direction.cso");
	buffer = PixelShader::Create(game->Graphics, "Shaders/buffer.cso");
	ambient = PixelShader::Create(game->Graphics, "Shaders/ambient.cso");

	model = Geometry::CreateFromBDM(game->Graphics, "Models/sphere.bdm");
	capsule = Geometry::CreateFromBDM(game->Graphics, "Models/capsule.bdm");
	flag = Geometry::CreateFromBDM(game->Graphics, "Models/flag.bdm");

	Texture2D* nullProp = Texture2D::FromFile(game->Graphics, "Materials/default_rough.png");

	concrete = { Texture2D::FromFile(game->Graphics, "Materials/concrete.png"), Texture2D::FromFile(game->Graphics, "Materials/wood_prop.png") };
	flagTex = { Texture2D::FromFile(game->Graphics, "Materials/flag.png"), nullProp };
	redTex = { Texture2D::FromFile(game->Graphics, "Materials/red.png"), nullProp };
	blueTex = { Texture2D::FromFile(game->Graphics, "Materials/blue.png"), nullProp };

	font = FontFactory::LoadFont(game->Graphics, "Fonts/Jura-Regular.ttf", 20);

	sky.Initialize(game->Graphics);

	Generator gen;
	floor = gen.Floor(game->Graphics, game->Physics);
	building = gen.Building(game->Graphics, game->Physics);

	auto settings = game->Graphics->Settings;
	albedo = RenderTarget2D::Create(game->Graphics, settings.Width, settings.Height, DXGI_FORMAT_R8G8B8A8_UNORM);
	albedo->CreateDepthBuffer(game->Graphics);

	normal = RenderTarget2D::Create(game->Graphics, settings.Width, settings.Height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	properties = RenderTarget2D::Create(game->Graphics, settings.Width, settings.Height, DXGI_FORMAT_R8G8B8A8_UNORM);
}


void DeferredRenderer::Shutdown(Game* game) {
	RELEASE_DELETE(floor);
	RELEASE_DELETE(building);
	RELEASE_DELETE(albedo);
}


void DeferredRenderer::AddLight(Light* light) {
	lights.push_back(light);
}


void DeferredRenderer::RemoveLight(Light* light) {
	auto it = std::find(lights.begin(), lights.end(), light);
	if (it != lights.end()) {
		lights.erase(it);
	}
}


void DeferredRenderer::SetFlagPosition(glm::vec3 pos) {
	flagPosition = pos;
}


void DeferredRenderer::Render(Game* game, Camera* camera) {
	for (const auto& bullet : game->GetBullets()) {
		scene.Queue(model, &concrete, glm::translate(bullet->Position()) * glm::scale(glm::vec3(0.1f, 0.1f, 0.1f)));
	}

	std::unordered_map<ObjectID, GameObject*> objects = game->GetObjects();
	for (auto& idObject : objects) {
		Actor* actor = dynamic_cast<Actor*>(idObject.second);
		Prop* prop = dynamic_cast<Prop*>(idObject.second);
		if (actor) {
			Material* tex = &concrete;
			for (auto& idInfo : game->Players) {
				if (idInfo.second.ActorID == actor->ID) {
					if (idInfo.second.Team == Red) {
						tex = &redTex;
					} else if (idInfo.second.Team == Blue) {
						tex = &blueTex;
					}
					break;
				}
			}

			scene.Queue(capsule, tex, glm::translate(actor->Position()));
		} else if (prop) {
			scene.Queue(prop->Mesh, &prop->Material, glm::translate(prop->Position()) * glm::mat4_cast(prop->Rotation()));
		}
	}

	scene.Queue(floor, &concrete, glm::mat4());
	scene.Queue(building, &concrete, glm::mat4());
	scene.Queue(flag, &flagTex, glm::translate(flagPosition));

	game->Graphics->SetSceneConstants((float)game->Time.Time() / 20.0f);

	RenderState::SetDepth(game->Graphics, RenderState::DepthMode::ReadWrite);
	RenderState::SetBlend(game->Graphics, RenderState::BlendMode::Disable);
	
	// Render the lights
	for (Light* light : lights) {
		if (light->RenderTarget) {
			if (light->Type == Light::Point) {
				float length = glm::length(camera->GetPosition() - light->GetPosition()) * tanf(light->GetFOV() / 2);
				light->SetTarget(camera->GetPosition() + camera->GetDirection() * length);
			} else if (light->Type == Light::Direction) {
				light->SetPosition(camera->GetPosition() - light->GetDirection() * light->GetFarClip() * 0.5f);
			}
			light->Apply(game->Graphics);
			light->RenderTarget->Clear(game->Graphics, 1.0f, 1.0f, 1.0f, 1.0f);
			light->RenderTarget->ClearDepth(game->Graphics, 1.0f);
			light->RenderTarget->Apply(game->Graphics);

			scene.Draw(game->Graphics, light, vertex, shadow);
		}
	}
	
	// Render the scene
	albedo->Clear(game->Graphics, 0.0f, 0.0f, 0.0f, 0.0f);
	albedo->ClearDepth(game->Graphics, 1.0f);
	normal->Clear(game->Graphics, 0.0f, 0.0f, 0.0f, 0.0f);
	properties->Clear(game->Graphics, 0.0f, 0.0f, 0.0f, 0.0f);

	ID3D11RenderTargetView* targets[] = {
		albedo->GetView(),
		normal->GetView(),
		properties->GetView()
	};
	game->Graphics->Context->OMSetRenderTargets(3, targets, albedo->GetDepthView());
	game->Graphics->Context->RSSetViewports(1, albedo->GetViewport());

	game->Graphics->SetView(camera);

	scene.Draw(game->Graphics, camera, vertex, buffer);

	scene.Clear();

	// Render the g-buffer
	RenderTarget2D* target = game->Graphics->RenderTarget;
	target->Apply(game->Graphics);
	target->ClearDepth(game->Graphics, 1.0f);

	RenderState::SetDepth(game->Graphics, RenderState::DepthMode::None);
	sky.Draw(game->Graphics);

	RenderState::SetBlend(game->Graphics, RenderState::BlendMode::Alpha);
	albedo->SetAsResource(game->Graphics, 5);
	normal->SetAsResource(game->Graphics, 6);
	properties->SetAsResource(game->Graphics, 7);

	game->Graphics->Draw(3, fullscreen, ambient, Transform());

	RenderState::SetBlend(game->Graphics, RenderState::BlendMode::Additive);
	for (Light* light : lights) {
		light->SetAsShadowMap(game->Graphics);
		light->Apply(game->Graphics);

		PixelShader* shader = nullptr;
		switch (light->Type) {
		case Light::Point:
			shader = point;
			break;
		case Light::Direction:
			shader = direction;
			break;
		}

		game->Graphics->Draw(3, fullscreen, shader, Transform()); // TODO: Add light geometry
	}

	ID3D11ShaderResourceView* null[3] = { nullptr };
	game->Graphics->Context->PSSetShaderResources(2, 1, null);
	game->Graphics->Context->PSSetShaderResources(5, 3, null);

	RenderState::SetDepth(game->Graphics, RenderState::DepthMode::ReadWrite);
	RenderState::SetBlend(game->Graphics, RenderState::BlendMode::Alpha);

	// Draw some name tags over the players
	std::unordered_map<PlayerID, PlayerInfo>& players = game->Players;
	for (auto& idInfo : players) {
		if (idInfo.second.ActorID != InvalidObject) {
			Actor* actor = dynamic_cast<Actor*>(game->GetObject(idInfo.second.ActorID));
			if (actor) {
				glm::vec3 dir = camera->GetPosition() - actor->Position();
				glm::vec4 color = (idInfo.second.Team == Red ? glm::vec4(1, 0, 0, 1) : glm::vec4(0, 0, 1, 1));
				float angle = float(M_PI) * 0.5f - std::atan2(dir.z, dir.x);
				font->Draw3D(game->Graphics, camera, idInfo.second.Name,
							 glm::translate(actor->Position() + glm::vec3(0, 0.8f, 0)) * glm::rotate(glm::mat4(), float(angle), glm::vec3(0, 1, 0)) * glm::scale(glm::vec3(0.25f, 0.25f, 0.25f)),
							 color);
			}
		}
	}
}