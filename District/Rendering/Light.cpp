#include "Light.hpp"


Light* Light::CreatePoint(Graphics* graphics, glm::vec3 position, glm::vec3 target, float strength, glm::vec3 color, int resolution) {
	Light* light = new Light;

	light->strength = strength;
	light->distance = strength;
	light->color = color;
	light->Type = Point;
	light->SetPosition(position);
	light->SetTarget(target);
	light->SetResolution(resolution, resolution);
	light->SetUp(glm::vec3(0, 1, 0));
	light->SetClipPlanes(1, strength);
	light->SetFOV((float)M_PI / 2.0f);

	if (resolution > 0) {
		light->RenderTarget = RenderTarget2D::Create(graphics, resolution, resolution, DXGI_FORMAT_R16G16_UNORM);
		light->RenderTarget->CreateDepthBuffer(graphics);
	}

	return light;
}

Light* Light::CreateDirection(Graphics* graphics, glm::vec3 direction, float strength, glm::vec3 color, int resolution, float shadowSize) {
	Light* light = new Light;

	light->strength = strength;
	light->distance = 100;
	light->color = color;
	light->Type = Direction;
	light->SetDirection(direction);
	light->SetResolution(resolution, resolution);
	light->SetUp(glm::vec3(0, 1, 0));
	light->SetClipPlanes(1, light->distance);
	light->MakeOrtho(shadowSize, shadowSize);

	if (resolution > 0) {
		light->RenderTarget = RenderTarget2D::Create(graphics, resolution, resolution, DXGI_FORMAT_R16G16_UNORM);
		light->RenderTarget->CreateDepthBuffer(graphics);
	}

	return light;
}


void Light::Release() {
	RELEASE_DELETE(RenderTarget);
}


void Light::Apply(Graphics* graphics) {
	graphics->SetLight(GetTransposeProjectionViewMatrix(), GetPosition(), GetDirection(), strength, color, distance);
}


void Light::SetAsShadowMap(Graphics* graphics) {
	if (RenderTarget) {
		RenderTarget->GenerateMips(graphics);
		RenderTarget->SetAsResource(graphics, 2);
	}
}