#pragma once

#include "../Includes.hpp"
#include "Light.hpp"
#include "RenderQueue.hpp"
#include "Skybox.hpp"
#include "../Logic/Game.hpp"
#include <Base/Graphics/Font/Font.hpp>
#include <Base/Graphics/RenderTarget2D.hpp>

class DeferredRenderer {
	std::vector<Light*> lights;
	RenderQueue scene;
	glm::vec3 flagPosition;

	RenderTarget2D *albedo, *normal, *properties;

	Skybox sky;

	Font *font;
	PixelShader *pixel, *shadow, *buffer, *point, *direction, *ambient;
	VertexShader *vertex, *fullscreen;
	Geometry *floor, *model, *capsule, *building, *flag;
	Material concrete, flagTex, redTex, blueTex;

public:

	void Initialize(Game* game);
	void Shutdown(Game* game);

	void SetFlagPosition(glm::vec3 pos);

	void AddLight(Light* light);
	void RemoveLight(Light* light);

	void Render(Game* game, Camera* camera);

};