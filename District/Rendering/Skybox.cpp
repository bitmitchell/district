#include "Skybox.hpp"
#include <Base/Graphics/RenderState.hpp>


void Skybox::Initialize(Graphics* graphics) {
	pixel = PixelShader::Create(graphics, "Shaders/sky.cso");
	vertex = VertexShader::Create(graphics, "Shaders/fullscreen.cso");
	gradient = { Texture2D::FromFile(graphics, "Materials/sky_gradient.bmp"), nullptr };
}


void Skybox::Draw(Graphics* graphics) {
	RenderState::SetDepth(graphics, RenderState::DepthMode::None);
	graphics->Draw(3, vertex, pixel, Transform(), &gradient);
	RenderState::SetDepth(graphics, RenderState::DepthMode::ReadWrite);
}