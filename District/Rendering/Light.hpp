#pragma once

#include "../Includes.hpp"

#include <Base/Graphics/Graphics.hpp>
#include <Base/Graphics/RenderTarget2D.hpp>
#include <Base/Graphics/Camera.hpp>

class Light : public Camera {
	float strength;
	float distance;
	glm::vec3 color;
public:
	enum Type {
		Point,
		Direction
	} Type;

	RenderTarget2D* RenderTarget;

	static Light* CreatePoint(Graphics* graphics, glm::vec3 position, glm::vec3 target, float strength, glm::vec3 color = glm::vec3(1, 1, 1), int resolution = 512);
	static Light* CreateDirection(Graphics* graphics, glm::vec3 direction, float strength, glm::vec3 color = glm::vec3(1, 1, 1), int resolution = 512, float shadowSize = 20);
	
	void Release();

	void Apply(Graphics* graphics);
	void SetAsShadowMap(Graphics* graphics);
};