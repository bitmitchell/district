#pragma once
#include "../Includes.hpp"
#include <Base/Graphics/Geometry.hpp>
#include <Base/Graphics/VertexShader.hpp>
#include <Base/Graphics/PixelShader.hpp>
#include <Base/Graphics/Material.hpp>
#include <Base/Graphics/Graphics.hpp>
#include <Base/Graphics/Camera.hpp>
#include <Base/Graphics/Transform.hpp>

class RenderQueue {
	struct Element {
		Geometry* Geometry;
		Material* Material;
		glm::mat4 WorldTransform;
	};

	std::deque<Element> renderQueue;

public:

	void Queue(Geometry* geometry, Material* material, const glm::mat4& transform);

	void Draw(Graphics* graphics, Camera* camera, VertexShader* vertex, PixelShader* pixel);

	void Clear();

};