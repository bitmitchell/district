#pragma once
#include "../Includes.hpp"

#include <Base/Graphics/PixelShader.hpp>
#include <Base/Graphics/VertexShader.hpp>
#include <Base/Graphics/Material.hpp>
#include <Base/Graphics/Graphics.hpp>

class Skybox {
	PixelShader* pixel;
	VertexShader* vertex;
	Material gradient;

public:

	Skybox() : pixel(nullptr), vertex(nullptr) {}

	void Initialize(Graphics* graphics);

	void Draw(Graphics* graphics);

};