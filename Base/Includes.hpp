#pragma once
#define _USE_MATH_DEFINES

// STD Library
#include <cstdint>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <cmath>
#include <cstdint>
#include <deque>
#include <cmath>
#include <map>
#include <sstream>
#include <algorithm>
#include <stack>


// GLM Maths
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/quaternion.hpp>


// Windows
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <windowsx.h>


// DirectX
#include <d3d11.h>
#include <dxgi.h>
#include <d3dcompiler.h>
#include <d3d11shader.h>

// Macros
#define GRAPHICS_RELEASE(obj) { if (obj) { obj->Release(); obj = nullptr; } }
#define RELEASE_DELETE(obj) { if (obj) { obj->Release(); delete obj; obj = nullptr; } }



#undef max
#undef min
