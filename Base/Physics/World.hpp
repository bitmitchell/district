#pragma once
#include "../Includes.hpp"

#include <btBulletDynamicsCommon.h>
#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>

class Body;
class Character;
class Projectile;

namespace Constants {
	const glm::vec3 Gravity(0, -10, 0);
}

class World {
	btBroadphaseInterface* broadphase;
	btDefaultCollisionConfiguration* collisionConfig;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btDiscreteDynamicsWorld* world;

	std::vector<Character*> characters;
	std::vector<Projectile*> projectiles;
	std::vector<Body*> bodies;

	float updateTime;

public:

	struct RayCast {
		Body* Object;
		glm::vec3 Intersection;
		glm::vec3 Normal;

		bool Hit() {
			return Object != nullptr;
		}
	};

	static World* Create();

	void Initialize();

	void Add(Body* body);
	void Add(Character* character);

	void Remove(Character* character);

	Projectile* FireProjectile(glm::vec3 position, glm::vec3 direction, float speed, float mass, float dragCoefficient, float caliber);

	void Update(float deltaTime);
	void Reset();

	std::vector<Projectile*> &GetProjectiles();

	RayCast CastRay(glm::vec3 start, glm::vec3 dir);

};