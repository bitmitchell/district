#pragma once
#include "../Includes.hpp"
#include "../Application/Log.hpp"

#include "World.hpp"

class Projectile{
	bool isAlive;

	glm::vec3 position, velocity;
	float mass, dragCoefficient, diameter;

public:
	Projectile::Projectile(glm::vec3 position, glm::vec3 direction, float velocity, float mass, float dragCoefficient, float diameter);

	void Update(World& world, float dT);
	bool IsAlive();

	glm::vec3 Position();
	glm::vec3 Velocity();

	float Energy();
};