#include "World.hpp"
#include "Body.hpp"
#include "Character.hpp"
#include "Projectile.hpp"
#include "../Application/Performance.hpp"

namespace{
	const float UpdateInterval = 1.0f / 60.0f;
}


World* World::Create() {
	World* world = new World();
	world->Initialize();
	return world;
}


void World::Initialize() {
	updateTime = 0.0f;
	broadphase = new btDbvtBroadphase();
	collisionConfig = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfig);
	btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher);
	solver = new btSequentialImpulseConstraintSolver();
	world = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfig);
	world->setGravity(btVector3(Constants::Gravity.x, Constants::Gravity.y, Constants::Gravity.z));
}


void World::Add(Body* body) {
	body->body->setUserPointer(body);
	world->addRigidBody(body->body);
	bodies.push_back(body);
}


void World::Add(Character* character) {
	character->body->setUserPointer(character);
	world->addRigidBody(character->body);
	characters.push_back(character);
}


void World::Remove(Character* character) {
	world->removeRigidBody(character->body);
	characters.erase(std::remove(characters.begin(), characters.end(), character), characters.end());
}

Projectile* World::FireProjectile(glm::vec3 pos, glm::vec3 dir, float speed, float mass, float dragCoeff, float caliber) {
	Projectile* projectile = new Projectile(pos, dir, speed, mass, dragCoeff, caliber);
	projectiles.push_back(projectile);
	return projectile;
}

void World::Update(float dT) {
	PerfFunction;
	world->stepSimulation(dT, 10);
	updateTime += dT;

	while (updateTime >= UpdateInterval) {
		updateTime -= UpdateInterval;

		for (auto& character : characters) {
			character->Update(*this, UpdateInterval);
		}

		for (auto& proj : projectiles) {
			proj->Update(*this, UpdateInterval);
		}

		projectiles.erase(std::remove_if(std::begin(projectiles), std::end(projectiles), [](Projectile* p) { return !p->IsAlive(); }), std::end(projectiles));
	}
}


World::RayCast World::CastRay(glm::vec3 start, glm::vec3 end) {
	PerfFunction;
	btVector3 startPosition(start.x, start.y, start.z);
	btVector3 endPosition(end.x, end.y, end.z);

	btCollisionWorld::ClosestRayResultCallback rayCallback(startPosition, endPosition);

	world->rayTest(startPosition, endPosition, rayCallback);

	if (rayCallback.hasHit()) {
		Body* body = (Body*)rayCallback.m_collisionObject->getUserPointer();

		glm::vec3 hitPos(
			(float)rayCallback.m_hitPointWorld.x(),
			(float)rayCallback.m_hitPointWorld.y(),
			(float)rayCallback.m_hitPointWorld.z()
			);

		glm::vec3 hitNorm(
			(float)rayCallback.m_hitNormalWorld.x(),
			(float)rayCallback.m_hitNormalWorld.y(),
			(float)rayCallback.m_hitNormalWorld.z()
			);

		if (body != nullptr) {
			return{ body, hitPos, hitNorm };
		}
	}

	return{ nullptr, glm::vec3(), glm::vec3() };
}


std::vector<Projectile*> &World::GetProjectiles() {
	return projectiles;
}


void World::Reset() {
	delete world;
	delete solver;
	delete dispatcher;
	delete collisionConfig;
	delete broadphase;

	for (Body* body : bodies) {
		delete body;
	}

	for (Character* character : characters) {
		delete character;
	}

	for (Projectile* projectile : projectiles) {
		delete projectile;
	}

	bodies.clear();
	characters.clear();
	projectiles.clear();

	Initialize();
}