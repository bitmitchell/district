#pragma once

#include "../Includes.hpp"
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include "../Application/Event.hpp"

class Projectile;

class Body {
protected:

	btRigidBody* body;
	btDefaultMotionState* motion;
	btCollisionShape* shape;

	Body(btRigidBody* body, btDefaultMotionState* motion, btCollisionShape* shape) : body(body), motion(motion), shape(shape) {}

	friend class World;

public:

	Event<Projectile*> Shot;

	virtual ~Body() {
		delete shape;
		delete body;
		delete motion;
	}

	static void Initialize();
	static void Shutdown();

	static Body* Plane(glm::vec3 normal, float w);
	static Body* Box(glm::vec3 halfWidths, glm::vec3 position, float mass);
	static Body* Sphere(float radius, glm::vec3 position, float mass);
	static Body* StaticMesh(const std::vector<glm::vec3>& triangleList, glm::vec3 position);

	glm::vec3 Position();
	glm::vec3 Velocity();
	glm::quat Rotation();
	float Mass();

	void ApplyForce(glm::vec3 force, glm::vec3 relPos);
	void ApplyImpulse(glm::vec3 impulse, glm::vec3 relPos);

	void SetFriction(float friction);
	void SetPosition(glm::vec3 position);
	void SetVelocity(glm::vec3 velocity);
	void SetRotation(glm::quat rotation);

	bool IsAwake();
};