#pragma once
#include "Body.hpp"
#include "World.hpp"

/*
	The character class represents a Bullet capsule object, which sits on
	top of a spring. This spring allows the character to slide around and
	travel up and down stairs.
*/



class Character : public Body {

	float previousSpringLength;
	bool isGrounded;
	World::RayCast groundContact;
	glm::vec2 targetVelocity;

	Character(btRigidBody* body, btDefaultMotionState* motion, btCollisionShape* shape);

	void Update(World& world, float dT);
	void UpdateSpring(World& world, float dT);
	void UpdateMovement(float dT);

	friend class World;

public:

	static Character* Create(glm::vec3 position);

	void Jump();
	void SetTargetVelocity(glm::vec2 velocity);

	bool IsGrounded();

	glm::vec3 GetEyePosition();

};