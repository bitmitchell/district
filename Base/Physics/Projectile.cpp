#include "Projectile.hpp"
#include "Body.hpp"

namespace {
	const float DragConstant = -0.481f;
}


Projectile::Projectile(glm::vec3 position, glm::vec3 direction, float velocity, float mass, float dragCoefficient, float diameter) :
position(position), velocity(glm::normalize(direction) * velocity), mass(mass), dragCoefficient(dragCoefficient), diameter(diameter), isAlive(true) {
}


void Projectile::Update(World& world, float dT) {
	float drag = DragConstant * glm::dot(velocity, velocity) * dragCoefficient * std::pow(diameter, 2) / mass;

	glm::vec3 acceleration = Constants::Gravity;
	if (glm::length2(velocity) > 0) {
		acceleration += glm::normalize(velocity) * drag;
	}
	velocity += acceleration * dT;
	glm::vec3 newPosition = position + velocity * dT;

	World::RayCast collision = world.CastRay(position, newPosition);

	if (collision.Hit()) {
		position = collision.Intersection;

		glm::vec3 momentum = velocity * mass;
		collision.Object->ApplyImpulse(momentum / collision.Object->Mass(), collision.Intersection - collision.Object->Position());
		collision.Object->Shot(this);

		isAlive = false;
	} else {
		position = newPosition;
	}
}


bool Projectile::IsAlive() {
	return isAlive;
}


glm::vec3 Projectile::Position() {
	return position;
}

glm::vec3 Projectile::Velocity() {
	return velocity;
}


float Projectile::Energy() {
	return mass * glm::dot(velocity, velocity) * 0.5f;
} 