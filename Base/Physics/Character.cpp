#include "Character.hpp"
#include "World.hpp"
#include "../Application/Log.hpp"


namespace {
	const float CharacterRideHeight = 0.3f;
	const float CharacterHeight = 1.7f;
	const float CharacterRadius = 0.3f;
	const float CharacterMass = 1;
	const float SpringConstant = 100;
	const float UpdateInterval = 1.0f / 60.0f;
	const float DampingFactor = 2 * sqrt(CharacterMass * SpringConstant);
	const float JumpSpeed = 5.5f;
	const float GroundedFriction = 0.2f;
	const float CharacterAcceleration = 0.13f;
}


Character::Character(btRigidBody* body, btDefaultMotionState* motion, btCollisionShape* shape) : 
	Body(body, motion, shape),
	previousSpringLength(CharacterRideHeight),
	isGrounded(false) {
}


Character* Character::Create(glm::vec3 pos) {
	btCapsuleShape* shape = new btCapsuleShape(CharacterRadius, CharacterHeight - CharacterRideHeight - CharacterRadius * 2);
	btDefaultMotionState* motion = new btDefaultMotionState(btTransform(btQuaternion::getIdentity(), btVector3(pos.x, pos.y, pos.z)));
	btRigidBody* body = new btRigidBody(CharacterMass, motion, shape);

	body->setInvInertiaDiagLocal(btVector3(0, 0, 0));
	body->updateInertiaTensor();
	body->setActivationState(DISABLE_DEACTIVATION);

	return new Character(body, motion, shape);
}


void Character::Update(World& world, float dT) {
	UpdateSpring(world, dT);
	UpdateMovement(dT);
	
}


void Character::UpdateMovement(float dT) {
	if (isGrounded) {
		btVector3 velocity = body->getLinearVelocity();
		float dx = (targetVelocity.x - velocity.x()) * CharacterAcceleration;
		float dz = (targetVelocity.y - velocity.z()) * CharacterAcceleration;
		body->applyCentralImpulse(btVector3(dx, 0, dz));
	}
}


void Character::UpdateSpring(World& world, float dT) {
	float capsuleHeight = CharacterHeight - CharacterRideHeight;
	float castLength = isGrounded ? CharacterRideHeight * 2 : CharacterRideHeight;

	glm::vec3 start = Position();
	glm::vec3 end = start - glm::vec3(0, castLength + capsuleHeight * 0.5f - 10.0f / SpringConstant, 0);

	groundContact = world.CastRay(start, end);

	isGrounded = groundContact.Hit();

	if (groundContact.Hit()) {
		float idealHitDistance = CharacterRideHeight + capsuleHeight * 0.5f;
		float currentSpringLength = glm::length(groundContact.Intersection - start);
		float displacement = currentSpringLength - idealHitDistance;

		float velocity = (currentSpringLength - previousSpringLength) / dT;

		float force = -SpringConstant * displacement - DampingFactor * velocity;

		body->applyCentralImpulse(btVector3(0, force * dT / CharacterMass, 0));
		groundContact.Object->ApplyImpulse(glm::vec3(0.0f, -force * dT / groundContact.Object->Mass(), 0.0f), groundContact.Intersection - groundContact.Object->Position());

		previousSpringLength = currentSpringLength;
	}
}


void Character::Jump() {
	if (isGrounded) {
		isGrounded = false;
		btVector3 velocity = body->getLinearVelocity();
		velocity.setY(JumpSpeed);
		body->setLinearVelocity(velocity);
		body->translate(btVector3(0, JumpSpeed * UpdateInterval, 0));
	}
}


bool Character::IsGrounded() {
	return isGrounded;
}


void Character::SetTargetVelocity(glm::vec2 velocity) {
	targetVelocity = velocity;
}


glm::vec3 Character::GetEyePosition() {
	return Position() + glm::vec3(0, (CharacterHeight - CharacterRideHeight) * 0.5f - 0.1f, 0);
}