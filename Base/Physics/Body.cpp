#include "Body.hpp"
#include <Base/Application/Log.hpp>

Body* Body::Plane(glm::vec3 normal, float w) {
	btStaticPlaneShape* shape = new btStaticPlaneShape(btVector3(normal.x, normal.y, normal.z), w);
	btDefaultMotionState* motion = new btDefaultMotionState;
	btRigidBody* body = new btRigidBody(0, motion, shape);
	body->setRollingFriction(0.1f);

	return new Body(body, motion, shape);
}


Body* Body::Box(glm::vec3 halfWidths, glm::vec3 position, float mass) {
	btBoxShape* shape = new btBoxShape(btVector3(halfWidths.x, halfWidths.y, halfWidths.z));
	btDefaultMotionState* motion = new btDefaultMotionState(btTransform(btQuaternion::getIdentity(), btVector3(position.x, position.y, position.z)));
	btVector3 inertia;
	shape->calculateLocalInertia(mass, inertia);
	btRigidBody* body = new btRigidBody(mass, motion, shape, inertia);
	body->setRollingFriction(0.1f);

	return new Body(body, motion, shape);
}


Body* Body::Sphere(float radius, glm::vec3 position, float mass) {
	btSphereShape* shape = new btSphereShape(radius);
	btDefaultMotionState* motion = new btDefaultMotionState(btTransform(btQuaternion::getIdentity(), btVector3(position.x, position.y, position.z)));
	btVector3 inertia;
	shape->calculateLocalInertia(mass, inertia);
	btRigidBody* body = new btRigidBody(mass, motion, shape, inertia);
	body->setRollingFriction(0.1f);

	return new Body(body, motion, shape);
}


Body* Body::StaticMesh(const std::vector<glm::vec3>& triangleList, glm::vec3 position) {
	btDefaultMotionState* motion = new btDefaultMotionState(btTransform(btQuaternion::getIdentity(), btVector3(position.x, position.y, position.z)));
	btTriangleMesh* mesh = new btTriangleMesh();
	for (unsigned int i = 0; i < triangleList.size(); i += 3) {
		mesh->addTriangle(btVector3(triangleList[i].x, triangleList[i].y, triangleList[i].z),
						   btVector3(triangleList[i+1].x, triangleList[i+1].y, triangleList[i+1].z),
						   btVector3(triangleList[i+2].x, triangleList[i+2].y, triangleList[i+2].z), true);
	}
	btBvhTriangleMeshShape* shape = new btBvhTriangleMeshShape(mesh, true);
	btRigidBody* body = new btRigidBody(0, motion, shape);
	return new Body(body, motion, shape);
}


glm::vec3 Body::Position() {
	btTransform transform;
	motion->getWorldTransform(transform);
	btVector3 pos = transform.getOrigin();
	return glm::vec3(pos.x(), pos.y(), pos.z());
}


glm::vec3 Body::Velocity() {
	btVector3 vel = body->getLinearVelocity();
	return glm::vec3(vel.x(), vel.y(), vel.z());
}


glm::quat Body::Rotation() {
	btTransform transform;
	motion->getWorldTransform(transform);
	btQuaternion rot = transform.getRotation();
	return glm::quat(rot.w(), rot.x(), rot.y(), rot.z());
}


float Body::Mass() {
	return 1.0f / body->getInvMass();
}


void Body::ApplyForce(glm::vec3 force, glm::vec3 relPos) {
	body->activate();
	body->applyForce(
		btVector3(force.x, force.y, force.z),
		btVector3(relPos.x, relPos.y, relPos.z)
		);
}


void Body::ApplyImpulse(glm::vec3 impulse, glm::vec3 relPos) {
	body->activate();
	body->applyImpulse(
		btVector3(impulse.x, impulse.y, impulse.z),
		btVector3(relPos.x, relPos.y, relPos.z)
		);
}


void Body::SetFriction(float friction) {
	body->activate(true);
	body->setFriction(friction);
}


void Body::SetPosition(glm::vec3 pos) {
	body->activate(true);
	btTransform transform = body->getCenterOfMassTransform();
	transform.setOrigin(btVector3(pos.x, pos.y, pos.z));
	body->setCenterOfMassTransform(transform);
}


void Body::SetVelocity(glm::vec3 vel) {
	body->activate(true);
	body->setLinearVelocity(btVector3(vel.x, vel.y, vel.z));
}


void Body::SetRotation(glm::quat rot) {
	body->activate(true);
	btTransform transform = body->getCenterOfMassTransform();
	transform.setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));
	body->setCenterOfMassTransform(transform);
}


bool Body::IsAwake() {
	return body->getActivationState() != ISLAND_SLEEPING;
}