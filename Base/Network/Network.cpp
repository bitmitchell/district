#include "Network.hpp"
#include "../Application/Log.hpp"
#include "../Application/Timer.hpp"

namespace {
	const double ConnectionTimeout = 10;
}


Network* Network::Create() {
	if (enet_initialize() != 0) {
		Log::Error("Failed to initialize ENet library");
	}
	Network* net = new Network;
	net->host = nullptr;
	net->state = Null;
	net->id = -1;
	return net;
}


void Network::Shutdown() {
	enet_deinitialize();
}


void Network::ClearEvents() {
	Connected.Clear();
	Disconnected.Clear();
	ConnectionTimedOut.Clear();
	TimedOut.Clear();
	IDSet.Clear();
	MessageReceived.Clear();
	ClientConnected.Clear();
	ClientDisconnected.Clear();
}


bool Network::Host(uint16_t port) {
	ENetAddress address;
	address.port = port;
	address.host = ENET_HOST_ANY;

	nextFreePlayerID = 1;
	id = 0;

	int clients = 32;

	host = enet_host_create(&address, clients, Channel::Count, 0, 0);
	if (host == nullptr) {
		Log::Warning("Failed to create network host");
		return false;
	}

	Log::Info("Network server initialized");
	state = Hosting;
	return true;
}


bool Network::Connect(const std::string& addr, uint16_t port) {
	host = enet_host_create(nullptr, 1, Channel::Count, 0, 0);
	if (host == nullptr) {
		Log::Warning("Failed to create network client");
		return false;
	}

	state = Connecting;

	ENetAddress address;
	enet_address_set_host(&address, addr.c_str());
	address.port = port;

	connectionTimeout.Reset();

	ENetPeer* peer = enet_host_connect(host, &address, Channel::Count, 0);
	if (peer == nullptr) {
		Log::Warning("Failed to connect to remote server: " + addr);
		return false;
	}

	connections.insert(std::make_pair(peer, Connection(0, peer)));
	return true;
}


void Network::Disconnect(DisconnectReason reason) {
	if (host == nullptr || state == Null) {
		return;
	}

	if (reason == User && state == Hosting) {
		reason = ServerShuttingDown;
	}

	for (auto& peer : connections) {
		enet_peer_disconnect(peer.first, (enet_uint32)reason);
	}

	enet_host_flush(host);

	for (auto& peer : connections) {
		enet_peer_reset(peer.first);
	}
	connections.clear();

	enet_host_destroy(host);
	host = nullptr;
	state = Null;
}


void Network::Update() {
	if (host == nullptr || state == Null) {
		return;
	}

	if (state == Connecting && connectionTimeout.Time() > ConnectionTimeout) {
		ConnectionTimedOut();
		Disconnect(TimeOut);
		return;
	}

	if (state == Client && connectionTimeout.Time() > ConnectionTimeout) {
		TimedOut();
		Disconnect(TimeOut);
	}

	if (state == Hosting) {
		for (auto it = connections.begin(); it != connections.end();) {
			if (it->second.Timeout.Time() > ConnectionTimeout) {
				Log::Info("Client timed out");
				ClientDisconnected(it->second.ID);
				it = connections.erase(it);
			} else {
				++it;
			}
		}
	}

	ENetEvent event;
	while (host && state != Null && enet_host_service(host, &event, 0) > 0) {
		if (state == Client) {
			connectionTimeout.Reset();
		}

		switch (event.type) {
		case ENET_EVENT_TYPE_CONNECT:
			if (state == Hosting) {
				Log::Info("Client connected");
				PlayerID newId = nextFreePlayerID++;
				connections.insert(std::make_pair(event.peer, Connection(newId, event.peer)));
				Message clientData(6);
				clientData << uint16_t(0);
				clientData << newId;
				Send(clientData, Events, connections[event.peer]);
				ClientConnected(newId);
			} else if (state == Connecting) {
				state = Client;
				Log::Info("Connected to server");
				Connected();
				connectionTimeout.Reset();
			}
			break;

		case ENET_EVENT_TYPE_DISCONNECT:
			switch (event.data) {
			case User:
				Log::Info("User Disconnect");
				break;
			case TimeOut:
				Log::Info("Client timed out");
				break;
			case ServerShuttingDown:
				Log::Info("Server shutting down");
				break;
			}

			if (state == Hosting) {
				ClientDisconnected(connections[event.peer].ID);
				connections.erase(event.peer);
			} else {
				Disconnected();
			}
			break;

		case ENET_EVENT_TYPE_RECEIVE:
			Message msg(event.packet);

			if (state == Hosting) {
				connections[event.peer].Timeout.Reset();
			}
			
			if (state == Client && msg.Size() > 2) {
				uint16_t messageType;
				msg >> messageType;

				if (messageType == 0) {
					msg >> id;
					Log::Info("Client ID set to %%%", id);
					IDSet();
				} else {
					msg.ResetReader();
					MessageReceived(msg);
				}
			} else {
				MessageReceived(msg);
			}

			enet_packet_destroy(event.packet);
			break;
		}
	}
}


void Network::Send(Message& msg, Channel channel) {
	if (state == Null) {
		return;
	}

	enet_uint32 flags;
	if (channel == Events) {
		flags = ENET_PACKET_FLAG_RELIABLE;
	} else if (channel == Updates) {
		flags = ENET_PACKET_FLAG_UNSEQUENCED;
	}
	ENetPacket* packet = enet_packet_create(msg.Data(), msg.Size(), flags);

	enet_host_broadcast(host, channel, packet);
}


void Network::Send(Message& msg, Channel channel, const Connection& connection) {
	enet_uint32 flags;
	if (channel == Events) {
		flags = ENET_PACKET_FLAG_RELIABLE;
	} else if (channel == Updates) {
		flags = ENET_PACKET_FLAG_UNSEQUENCED;
	}
	ENetPacket* packet = enet_packet_create(msg.Data(), msg.Size(), flags);

	enet_peer_send(connection.Peer, channel, packet);
}


PlayerID Network::ID() const {
	return id;
}


bool Network::IsHost() const {
	return state == Hosting;
}


bool Network::IsConnected() {
	return state == Client;
}


const Network::Connection* Network::GetConnection(PlayerID id) const {
	for (auto& peerConnection : connections) {
		if (peerConnection.second.ID == id) {
			return &peerConnection.second;
		}
	}
	return nullptr;
}