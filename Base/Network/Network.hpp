#pragma once
#include "../Includes.hpp"
#include "../Application/Event.hpp"
#include "../Application/Timer.hpp"
#include "Message.hpp"
#include <enet/enet.h>


typedef unsigned int PlayerID;
#define InvalidPlayer UINT_MAX

class Network {
public:
	struct Connection {
		PlayerID ID;
		Timer Timeout;
		ENetPeer* Peer;

		Connection(PlayerID id = -1, ENetPeer* peer = nullptr) : ID(id), Peer(peer) {}
	};

private:

	enum State {
		Hosting,
		Connecting,
		Client,
		Null
	} state;

	enum DisconnectReason {
		User,
		TimeOut,
		ServerShuttingDown,
	};

	Timer connectionTimeout;

	ENetHost* host;	
	std::unordered_map<ENetPeer*, Connection> connections;

	PlayerID id;
	PlayerID nextFreePlayerID;

public:

	enum Channel {
		Events,
		Updates,
		Count
	};

	Event<> Connected, Disconnected, ConnectionTimedOut, TimedOut, IDSet;
	Event<Message> MessageReceived;
	Event<PlayerID> ClientConnected, ClientDisconnected;

	static Network* Create();
	void Shutdown();

	void ClearEvents();

	bool Host(uint16_t port);
	bool Connect(const std::string& host, uint16_t port);
	bool IsConnected();
	void Disconnect(DisconnectReason reason = User);

	void Send(Message& msg, Channel channel);
	void Send(Message& msg, Channel channel, const Connection& connection);

	void Update();

	bool IsHost() const;

	PlayerID ID() const;

	const Connection* GetConnection(PlayerID id) const;

};