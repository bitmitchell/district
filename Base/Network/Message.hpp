#pragma once
#include "../Includes.hpp"
#include "../Application/Binary.hpp"
#include "../Application/Blob.hpp"
#include <enet/enet.h>

class Message : public BinaryWriter, public BinaryReader {
	Blob data;
	bool didAllocate;
public:
	Message(uint16_t size) : data(new uint8_t[size], size), didAllocate(true), BinaryWriter(&data), BinaryReader(&data) {
	}

	Message(uint8_t* ptr, uint16_t size) : data(ptr, size), didAllocate(false), BinaryWriter(&data), BinaryReader(&data) {
	}

	Message(ENetPacket* packet) : data(packet->data, packet->dataLength), didAllocate(false), BinaryWriter(&data), BinaryReader(&data) {
	}

	Message(const Message& rhs) : data(new uint8_t[rhs.data.Size], rhs.data.Size), didAllocate(true), BinaryWriter(&data), BinaryReader(&data) {
		memcpy_s(data.Data, data.Size, rhs.data.Data, rhs.data.Size);
	}

	~Message() {
		if (didAllocate) {
			//data.Release();
		}
	}

	const uint8_t* Data() {
		return data.Data;
	}

	uint16_t Size() {
		return data.Size;
	}
};