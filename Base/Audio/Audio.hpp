#pragma once
#include "../Includes.hpp"
#include <al.h>
#include <alc.h>
#include "Sound.hpp"

namespace Constants {
	const int SpeedOfSound = 340;
};

class Audio {
	bool valid;

	std::unordered_set<Sound*> sounds;
	std::unordered_map<std::string, ALuint> buffers;
	std::stack<ALuint> freeSources;

	ALuint GetBuffer(std::string& name, bool forceMono = false);
	ALuint GetFreeSource();

public:
	static Audio* Create();

	void Update();

	void SetListener(glm::vec3 position, glm::vec3 velocity, glm::vec3 up, glm::vec3 forward);
	void SetDopplerFactor(float factor);

	Sound* PlayEffect(std::string effectFile, glm::vec3 position, glm::vec3 velocity);
	void DeleteSound(Sound* sound);

	void PrecacheSound(std::string filename, bool forceMono = false);

	void StopAllSounds();
};