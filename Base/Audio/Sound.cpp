#include "Sound.hpp"


Sound::Sound(glm::vec3 pos, glm::vec3 vel, ALuint src) : Position(pos), Velocity(vel), Source(src) {
}


void Sound::Play() {
	alSourcePlay(Source);
}


void Sound::Pause() {
	alSourcePause(Source);
}


void Sound::Stop() {
	alSourceStop(Source);
}


void Sound::Restart() {
	alSourceRewind(Source);
}


void Sound::Gain(float gain) {
	alSourcef(Source, AL_GAIN, gain);
}


void Sound::Loop(bool loop) {
	alSourcei(Source, AL_LOOPING, loop ? AL_TRUE : AL_FALSE);
}


void Sound::PitchShift(float shift) {
	alSourcef(Source, AL_PITCH, shift);
}