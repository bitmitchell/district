#pragma once
#define STB_DEFINE
#include "Audio.hpp"
#include "../Application/Log.hpp"
#include "Sound.hpp"

#include <stb_vorbis.c>

Audio* Audio::Create() {
	Audio* engine = new Audio;

	ALCdevice* device = alcOpenDevice(nullptr);
	if (!device) {
		Log::Warning("Error opening audio device");
		engine->valid = false;
		return engine;
	}

	ALCcontext* context = alcCreateContext(device, nullptr);
	if (!context) {
		Log::Warning("Error creating OpenAL context");
		engine->valid = false;
		return engine;
	}

	alcMakeContextCurrent(context);

	engine->valid = true;
	return engine;
}


ALuint Audio::GetFreeSource() {
	if (freeSources.size() == 0) {
		ALuint sources[16];
		alGenSources(16, sources);
		for (int i = 0; i < 16; ++i) {
			freeSources.push(sources[i]);
		}
	}
	ALuint free = freeSources.top();
	freeSources.pop();
	return free;
}


void Audio::PrecacheSound(std::string name, bool forceMono) {
	GetBuffer(name, forceMono);
}


ALuint Audio::GetBuffer(std::string& name, bool forceMono) {
	if (buffers.count(name) == 0) {
		ALuint buf;
		alGenBuffers(1, &buf);

		int channels, sampleRate;
		short* data;
		int length = stb_vorbis_decode_filename(name.c_str(), &channels, &sampleRate, &data);

		if (forceMono && channels == 2) {
			channels = 1;
			for (int i = 0; i < length; ++i) {
				data[i] = data[i * 2];
			}
		}

		ALenum format = (channels == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16);
		alBufferData(buf, format, data, length * channels * 2, sampleRate);
		free(data);

		buffers[name] = buf;
	}
	return buffers[name];
}


void Audio::Update() {
	for (Sound* sound : sounds) {
		glm::vec3 pos = sound->Position;
		glm::vec3 vel = sound->Velocity;
		float posfv[] = { pos.x, pos.y, pos.z };
		float velfv[] = { vel.x, vel.y, vel.z };
		alSourcefv(sound->Source, AL_POSITION, posfv);
		alSourcefv(sound->Source, AL_VELOCITY, velfv);
	}
}


void Audio::SetListener(glm::vec3 position, glm::vec3 velocity, glm::vec3 up, glm::vec3 forward) {

	glm::vec3 s = glm::normalize(glm::cross(forward, up));
	up = glm::cross(s, forward);

	float pos[] = { position.x, position.y, position.z };
	float vel[] = { velocity.x, velocity.y, velocity.z };
	float ori[] = { forward.x, forward.y, forward.z, up.x, up.y, up.z };
	alListenerfv(AL_POSITION, pos);
	alListenerfv(AL_VELOCITY, vel);
	alListenerfv(AL_ORIENTATION, ori);
}


Sound* Audio::PlayEffect(std::string effect, glm::vec3 position, glm::vec3 velocity) {
	ALuint buf = GetBuffer(effect);
	ALuint src = GetFreeSource();
	alSourcei(src, AL_BUFFER, buf);

	Sound* sound = new Sound(position, velocity, src);
	sounds.insert(sound);
	sound->Play();
	return sound;
}


void Audio::DeleteSound(Sound* sound) {
	sound->Stop();
	freeSources.push(sound->Source);
	sounds.erase(sound);
	delete sound;
}


void Audio::SetDopplerFactor(float factor) {
	alDopplerFactor(factor);
}


void Audio::StopAllSounds() {
	for (Sound* sound : sounds) {
		sound->Stop();
		freeSources.push(sound->Source);
		delete sound;
	}

	sounds.clear();
}