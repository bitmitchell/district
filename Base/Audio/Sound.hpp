#pragma once
#include "../Includes.hpp"
#include <al.h>

class Sound {
public:
	ALuint Source;
	glm::vec3 Position, Velocity;

	Sound(glm::vec3 position, glm::vec3 velocity, ALuint source);

	void Play();
	void Pause();
	void Stop();
	void Restart();

	void Gain(float gain);
	void Loop(bool loop);
	void PitchShift(float shift);

};