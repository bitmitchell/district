#pragma once
#include "IComponent.hpp"


class VerticalStack : public IComponent {
	std::vector<IComponent*> collection;
	float nextComponentY, spacing;
public:

	static VerticalStack* Create(glm::vec2 position, float spacing) {
		VerticalStack* stack = new VerticalStack;
		stack->nextComponentY = 0.0f;
		stack->Position = position;
		stack->spacing = spacing;
		return stack;
	}

	virtual void Draw(Graphics* graphics) const {
		for (const auto component : collection) {
			component->Draw(graphics);
		}
	}

	virtual void TryClick(int x, int y) {
		for (const auto component : collection) {
			component->TryClick(x, y);
		}
	}

	virtual void TryRelease(int x, int y) {
		for (const auto component : collection) {
			component->TryRelease(x, y);
		}
	}

	virtual void TryMouseOver(int x, int y) {
		for (const auto component : collection) {
			component->TryMouseOver(x, y);
		}
	}

	void Add(IComponent* component) {
		collection.push_back(component);

		component->Position = glm::vec2(Position.x, Position.y + nextComponentY);
		AddSpacing();
	}

	void AddSpacing() {
		nextComponentY += spacing;
	}
};