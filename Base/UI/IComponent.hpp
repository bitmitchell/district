#pragma once
#include "../Graphics/Graphics.hpp"
#include "../Application/Event.hpp"

class IInputContext;

struct Style {
	enum State {
		Normal,
		Highlighted,
		Selected
	};

	glm::vec4 BackgroundNormal, BackgroundHighlighted, BackgroundSelected;
	glm::vec4 TextNormal, TextHighlighted, TextSelected;

	Style(glm::vec4 bg = glm::vec4(), glm::vec4 bgHighlight = glm::vec4(), glm::vec4 bgSelect = glm::vec4(),
		  glm::vec4 text = glm::vec4(), glm::vec4 textHighlight = glm::vec4(), glm::vec4 textSelect = glm::vec4()) :
		BackgroundNormal(bg), BackgroundHighlighted(bgHighlight), BackgroundSelected(bgSelect),
		TextNormal(text), TextHighlighted(textHighlight), TextSelected(textSelect) {
	}

	glm::vec4 BackgroundColor(State state) const {
		switch (state) {
		case Highlighted:
			return BackgroundHighlighted;
		case Selected:
			return BackgroundSelected;
		default:
			return BackgroundNormal;
		}
	}

	glm::vec4 TextColor(State state) const {
		switch (state) {
		case Highlighted:
			return TextHighlighted;
		case Selected:
			return TextSelected;
		default:
			return TextNormal;
		}
	}
};


class IComponent {
public:
	virtual void Draw(Graphics* graphics) const = 0;
	virtual void TryClick(int x, int y) {};
	virtual void TryRelease(int x, int y) {};
	virtual void TryMouseOver(int x, int y) {};
	virtual IInputContext* GetInputContext() { return nullptr; };

	Event<> HasClicked;
	Event<> OnSelected, DeSelected;

	glm::vec2 Position;
};