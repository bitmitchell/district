#include "Button.hpp"


Button* Button::Create(std::string text, Font* font, glm::vec2 position, glm::vec2 size, Style& style) {
	Button* button = new Button;
	button->text = text;
	button->Position = position;
	button->size = size;
	button->font = font;
	button->state = Style::Normal;
	button->style = style;

	if (button->size.y == 0) {
		button->size.y = font->GetSize();
	}

	if (button->size.x == 0) {
		button->size.x = font->MeasureText(text);
	}

	return button;
}


void Button::Draw(Graphics* graphics) const {
	glm::vec4 backgroundColor = style.BackgroundColor(state);
	glm::vec4 textColor = style.TextColor(state);

	RenderState::SetDepth(graphics, RenderState::DepthMode::None);
	ScreenSpace::Rectangle(graphics, Position, size, backgroundColor);

	int offsetX = int(size.x - font->MeasureText(text)) / 2;
	int offsetY = int(size.y - font->GetBearing()) / 2;

	font->Draw(graphics, text, Position.x + offsetX, Position.y + offsetY, textColor);
}


bool Button::IsInside(int x, int y) {
	return x > Position.x && x < size.x + Position.x && y > Position.y && y < size.y + Position.y;
}


void Button::TryMouseOver(int x, int y) {
	if (IsInside(x, y)) {
		if (state == Style::Normal) {
			state = Style::Highlighted;
		}
	} else {
		if (state == Style::Highlighted) {
			state = Style::Normal;
		}
	}
}


void Button::TryClick(int x, int y) {
	if (IsInside(x, y) && state == Style::Highlighted) {
		state = Style::Selected;
	}
}


void Button::TryRelease(int x, int y) {
	if (IsInside(x, y) && state == Style::Selected) {
		Clicked();
		state = Style::Highlighted;
	}
}