#pragma once
#include "../Graphics/ScreenSpace.hpp"
#include "../Graphics/Font/FontFactory.hpp"
#include "../Graphics/Graphics.hpp"
#include "../Application/Window.hpp"
#include "../Graphics/RenderState.hpp"
#include "../Application/Utilities.hpp"
#include "../Application/Timer.hpp"
#include "../Application/Math.hpp"
#include "IComponent.hpp"


class TextBox : public IComponent {
	class TextBoxInputContext : public IInputContext {
	public:
		Event<char> Character;
		Event<> Submit;
		Event<> Backspace;
		Event<int, int> MouseDown;
		Event<> Escape;

		void OnKeyDown(Keys::Key key) {
			switch (key) {
			case Keys::Enter:
				Submit();
				break;
			case Keys::Backspace:
			case Keys::Delete:
				Backspace();
				break;
			case Keys::Escape:
				Escape();
				break;
			}
		}

		void OnChar(char character) {
			Character(character);
		}

		void OnMouseDown(Mouse::Button button, int x, int y) {
			if (button == Mouse::LeftButton) {
				MouseDown(x, y);
			}
		}
	} input;

	std::string text;
	glm::vec2 size;
	Font* font;
	Timer blink;
	Style style;
	Style::State state;
	int characterLimit;

	bool IsInside(int x, int y);
	void SetupInput(Window* window);

public:

	static TextBox* Create(Font* newfont, glm::vec2 newposition, glm::vec2 newsize, Style& style, Window* window);

	void TryClick(int x, int y);
	void TryRelease(int x, int y);
	void TryMouseOver(int x, int y);
	void Draw(Graphics* graphics) const;
	IInputContext* GetInputContext();

	Event<> OnSelect, OnDeselect;
	Event<std::string> OnSubmit;

	std::string Text() const;
	void Text(std::string text);

	void SetCharacterLimit(int limit);
};