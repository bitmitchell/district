#pragma once
#include "../Graphics/ScreenSpace.hpp"
#include "../Graphics/Font/FontFactory.hpp"
#include "../Graphics/Graphics.hpp"
#include "../Application/Window.hpp"
#include "../Graphics/RenderState.hpp"
#include "IComponent.hpp"

class Button : public IComponent {
	glm::vec2 size;
	Font* font;
	std::string text;

	Style style;
	Style::State state;

	bool IsInside(int x, int y);

public:

	Event<> Clicked;

	static Button* Create(std::string text, Font* font, glm::vec2 position, glm::vec2 size, Style& style);
	void Draw(Graphics* graphics) const;

	void TryClick(int x, int y);
	void TryMouseOver(int x, int y);
	void TryRelease(int x, int y);
};