#include "TextBox.hpp"

namespace {
	const int CharacterInputMin = 32;
	const int CharacterInputMax = 126;
}


TextBox* TextBox::Create(Font* font, glm::vec2 position, glm::vec2 size, Style& style, Window* window) {
	TextBox* textbox = new TextBox;
	textbox->Position = position;
	textbox->size = size;
	textbox->font = font;
	textbox->style = style;
	textbox->state = Style::Normal;
	textbox->characterLimit = -1;

	textbox->SetupInput(window);

	return textbox;
}


void TextBox::Draw(Graphics* graphics) const {
	glm::vec4 backgroundColor = style.BackgroundColor(state);
	glm::vec4 textColor = style.TextColor(state);

	RenderState::SetDepth(graphics, RenderState::DepthMode::None);
	ScreenSpace::Rectangle(graphics, Position, size, backgroundColor);

	int offsetX = 3;
	int offsetY = int(size.y - font->GetBearing()) / 2;

	if (Wrap(blink.Time(), 0.0, 1.0) > 0.5 && state == Style::Selected) {
		font->Draw(graphics, text + "|", Position.x + offsetX, Position.y + offsetY, textColor);
	} else {
		font->Draw(graphics, text, Position.x + offsetX, Position.y + offsetY, textColor);
	}
}


bool TextBox::IsInside(int x, int y) {
	return x > Position.x && x < size.x + Position.x && y > Position.y && y < size.y + Position.y;
}


void TextBox::SetupInput(Window* window) {
	input.Character += [this](char input) {
		if (input >= CharacterInputMin && input <= CharacterInputMax &&
			state == Style::Selected && (characterLimit == -1 || (int)text.size() < characterLimit)) {
			text += input;
		}
	};

	input.Backspace += [this]() {
		if (text.size() > 0) {
			text.erase(text.end() - 1);
		}
	};

	input.MouseDown += [this](int x, int y) {
		TryClick(x, y);
	};

	input.Submit += [this]() {
		state = Style::Normal;
		OnSubmit(text);
		OnDeselect();
	};

	input.Escape += [this]() {
		state = Style::Normal;
		OnDeselect();
	};

	OnSelect += [window, this]() { window->PushInputContext(&input); };
	OnDeselect += [window, this]() { window->PopInputContext(&input); };
}


void TextBox::TryClick(int x, int y) {
	if (!IsInside(x, y) && state == Style::Selected) {
		state = Style::Normal;
		OnDeselect();
	}
}


void TextBox::TryRelease(int x, int y) {
	if (IsInside(x, y) && state == Style::Highlighted) {
		state = Style::Selected;
		OnSelect();
	}
}


void TextBox::TryMouseOver(int x, int y) {
	if (IsInside(x, y)) {
		if (state == Style::Normal) {
			state = Style::Highlighted;
		}
	} else {
		if (state == Style::Highlighted) {
			state = Style::Normal;
		}
	}
}


IInputContext* TextBox::GetInputContext() {
	return &input;
}


std::string TextBox::Text() const {
	return text;
}


void TextBox::Text(std::string t) {
	text = t;
}


void TextBox::SetCharacterLimit(int max) {
	characterLimit = max;
}