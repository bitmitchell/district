#pragma once
#include "../Graphics/ScreenSpace.hpp"
#include "../Graphics/Font/FontFactory.hpp"
#include "../Graphics/Graphics.hpp"
#include "../Application/Window.hpp"
#include "../Graphics/RenderState.hpp"
#include "IComponent.hpp"

class TickBox : public IComponent {
	Style style;
	Style::State state;
	glm::vec2 size;

	bool IsInside(int x, int y);

public:
	static TickBox* Create(glm::vec2 position, glm::vec2 size, Style& style);

	void TryClick(int x, int y);
	void TryMouseOver(int x, int y);
	void TryRelease(int x, int y);

	void Draw(Graphics* graphics) const;
};