#include "TickBox.hpp"


TickBox* TickBox::Create(glm::vec2 position, glm::vec2 size, Style& style) {
	TickBox* tickbox = new TickBox;
	tickbox->Position = position;
	tickbox->style = style;
	tickbox->state = Style::Normal;
	tickbox->size = size;

	return tickbox;
}


bool TickBox::IsInside(int x, int y) {
	return x >= Position.x && x <= Position.x + size.x && y >= Position.y && y <= Position.y + size.y;
}


void TickBox::TryClick(int x, int y) {
}


void TickBox::TryMouseOver(int x, int y) {
	if (IsInside(x, y)) {
		if (state == Style::Normal) {
			state = Style::Highlighted;
		}
	} else {
		if (state == Style::Highlighted) {
			state = Style::Normal;
		}
	}
}


void TickBox::TryRelease(int x, int y) {
	if (IsInside(x, y)) {
		if (state == Style::Highlighted) {
			state = Style::Selected;
		} else if (state == Style::Selected) {
			state = Style::Highlighted;
		}
	}
}


void TickBox::Draw(Graphics* graphics) const {
	RenderState::SetDepth(graphics, RenderState::DepthMode::None);

	glm::vec4 outerColor = style.BackgroundColor(state);
	glm::vec4 innerColor = style.TextColor(state);

	glm::vec2 innerPosition = Position + 2.0f;
	glm::vec2 innerSize = size - glm::vec2(4.0f);

	ScreenSpace::Rectangle(graphics, Position, size, outerColor);
	ScreenSpace::Rectangle(graphics, innerPosition, innerSize, innerColor);
}