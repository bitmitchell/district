#pragma once

#include "../Includes.hpp"
#include "Blob.hpp"
#include "../Graphics/Vertex.hpp"

#define BIT_SET(var, bit, value) if (value) { var |= (1 << bit); } else { var &= ~(1 << bit); }
#define BIT_ISSET(var, bit) ((var & (1 << bit)) == (1 << bit))

#define READER_STREAM(type) BinaryReader& operator>> (type& out)
#define WRITER_STREAM(type) BinaryWriter& operator<< (const type& in);

class BinaryReader {
	uint32_t position;
	const Blob* data;

	uint8_t ReadByte();
	uint16_t ReadShort();
	uint32_t ReadLong();
	uint64_t ReadLongLong();

public:
	BinaryReader(const Blob* data);

	READER_STREAM(uint8_t);
	READER_STREAM(uint16_t);
	READER_STREAM(uint32_t);
	READER_STREAM(uint64_t);

	READER_STREAM(int8_t);
	READER_STREAM(int16_t);
	READER_STREAM(int32_t);
	READER_STREAM(int64_t);

	READER_STREAM(float);
	READER_STREAM(double);

	READER_STREAM(std::string);

	READER_STREAM(glm::vec3);
	READER_STREAM(glm::vec2);
	READER_STREAM(glm::quat);

	READER_STREAM(Vertex);
	READER_STREAM(Triangle);

	void ResetReader() {
		position = 0;
	}
};


class BinaryWriter {
	uint32_t position;
	Blob* data;

	void WriteByte(uint8_t);
	void WriteShort(uint16_t);
	void WriteLong(uint32_t);
	void WriteLongLong(uint64_t);

public:
	BinaryWriter(Blob* data);

	WRITER_STREAM(uint8_t);
	WRITER_STREAM(uint16_t);
	WRITER_STREAM(uint32_t);
	WRITER_STREAM(uint64_t);

	WRITER_STREAM(int8_t);
	WRITER_STREAM(int16_t);
	WRITER_STREAM(int32_t);
	WRITER_STREAM(int64_t);

	WRITER_STREAM(float);
	WRITER_STREAM(double);

	WRITER_STREAM(std::string);

	WRITER_STREAM(glm::vec3);
	WRITER_STREAM(glm::vec2);
	WRITER_STREAM(glm::quat);

	WRITER_STREAM(Vertex);
	WRITER_STREAM(Triangle);
};


#undef READER_STREAM
#undef WRITER_STREAM