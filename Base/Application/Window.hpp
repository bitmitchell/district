#pragma once

#include "../Includes.hpp"
#include "Event.hpp"
#include "Inputs.hpp"

class Window {

	static HINSTANCE instance;

	HWND handle;

	std::vector<IInputContext*> inputContext;

	static LRESULT WINAPI WindowProc(HWND, UINT, WPARAM, LPARAM);
	LRESULT WindowProc(UINT, WPARAM, LPARAM);

	WINDOWPLACEMENT prevWindowPos;

	POINT Center();

	bool cursorVisible;

public:

	Event<> OnClose;
	Event<> LostFocus;
	Event<> GotFocus;


	static void Register(HINSTANCE instance);
	static void Unregister();

	struct Options {
		int PosX, PosY;
		int Width, Height;
	};

	Window();

	void Create(Options& opt);
	void Close();
	void MessagePump();
	HWND GetHandle();
	void Show();
	void FlashTaskBar();
	void SetFullscreen(bool state);
	bool IsFullscreen();	
	void SetCursorPos(int x, int y);
	void ShowCursor(bool show);
	bool HasFocus();
	void ClearEvents();

	void PushInputContext(IInputContext* context);
	void PopInputContext(IInputContext* context);
	void ClearInputContexts();
};