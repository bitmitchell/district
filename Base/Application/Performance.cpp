#include "Performance.hpp"

std::map<std::string, PerformanceTimer::TimingBlock> PerformanceTimer::blockTimes;
Timer PerformanceTimer::timer;

PerformanceTimer::PerformanceTimer(std::string blockName) : name(blockName) {
	if (blockTimes.count(name) == 0) {
		blockTimes[name] = { 0.0, 0 };
	}

	start = timer.Time();
}


PerformanceTimer::~PerformanceTimer() {
	TimingBlock& block = blockTimes[name];
	block.LastRun = timer.Time() - start;
	block.RunTime += block.LastRun;
	block.NumberOfRuns++;
}


void PerformanceTimer::ExportData(std::string filename) {
	std::ofstream file(filename);
	file << "name\truntime\truns\taverage" << std::endl;

	for (const auto& block : blockTimes) {
		file << block.first << "\t" <<
			block.second.RunTime << "\t" <<
			block.second.NumberOfRuns << "\t" <<
			block.second.RunTime / block.second.NumberOfRuns << std::endl;
	}

	file.close();
}


double PerformanceTimer::LastRunTime(std::string blockName) {
	auto blockIt = blockTimes.find(blockName);
	if (blockIt != blockTimes.end()) {
		return blockIt->second.LastRun;
	}
	return std::numeric_limits<double>::quiet_NaN();
}