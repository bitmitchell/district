#include "ThreadPool.hpp"


std::vector<std::thread> ThreadPool::workers;
std::queue<std::function<void()>> ThreadPool::tasks;

std::mutex ThreadPool::queueMutex;
std::condition_variable ThreadPool::condition, ThreadPool::finished;

std::atomic_bool ThreadPool::stop;
std::atomic_int ThreadPool::busy;