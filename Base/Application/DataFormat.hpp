#pragma once
#include "../Includes.hpp"



namespace Detail {

	template <class ItType>
	using Range = std::pair < ItType, ItType >;

	using StringRange = Range < std::string::iterator >;

	inline std::string::iterator SkipSpaces(std::string::iterator it) {
		while (isspace(*it)) it++;
		return it;
	}

	inline std::string::iterator FindSpace(std::string::iterator it) {
		while (!isspace(*it)) it++;
		return it;
	}

	inline bool MatchString(std::string::iterator it, std::string str) {
		std::string::iterator pos = std::begin(str);
		while (pos != std::end(str)) {
			if (*it != *pos) {
				return false;
			}

			it++;
			pos++;
		}
		return true;
	}

	inline void Error(std::string::iterator pos, StringRange& range, std::string reason) {
		int line = 1, column = 1;
		std::string::iterator it = range.first;
		while (it != pos && it != range.second) {
			if (*it == '\n') {
				line++;
				column = 1;
			}
			else {
				column++;
			}
			++it;
		}

		std::string::iterator space = FindSpace(pos);

		throw std::runtime_error("Error at (" + std::string(pos, space) + ") " + std::to_string(line) + "-" + std::to_string(column) + ": " + reason);
	}

}

class Object;

template <typename Type>
struct ConvertDDF {
	Type operator()(Object& obj);
};

class Object {

	static Object ParseMap(std::string::iterator& it, Detail::StringRange& range) {
		Object map(Map);

		if (*it != '{') {
			Detail::Error(it, range, "Expected { to start a map");
		}

		it = Detail::SkipSpaces(it + 1);

		while (it != range.second && *it != '}') {
			std::string::iterator end = Detail::FindSpace(it);
			std::string name(it, end);
			it = Detail::SkipSpaces(end);
			if (*it != '=') {
				Detail::Error(it, range, "Expected = after map key '" + name + "'");
			}
			it = Detail::SkipSpaces(it + 1);
			map.Children.emplace(name, ParseObject(it, range));
			it = Detail::SkipSpaces(it);
		}

		if (*it != '}') {
			Detail::Error(it, range, "Expected '}' to close off the map");
		}

		++it;

		return map;
	}

	static Object ParseList(std::string::iterator& it, Detail::StringRange& range) {
		Object list(List);

		if (*it != '[') {
			Detail::Error(it, range, "Expected [ to start a list");
		}

		it = Detail::SkipSpaces(it + 1);

		while (it != range.second) {
			list.ListValue.push_back(ParseObject(it, range));
			it = Detail::SkipSpaces(it);

			if (*it == ']') {
				break;
			}
			else if (*it != ',') {
				Detail::Error(it, range, "Expected ',' between list elements");
			}

			it = Detail::SkipSpaces(it + 1);
		}

		if (*it != ']') {
			Detail::Error(it, range, "Expected ']' to close off the list");
		}

		++it;

		return list;
	}

	static Object ParseObject(std::string::iterator& it, Detail::StringRange& range) {
		it = Detail::SkipSpaces(it);

		if (*it == '{') {
			return ParseMap(it, range);
		}
		else if (*it == '[') {
			return ParseList(it, range);
		}
		else {
			if (Detail::MatchString(it, "true")) {
				it += 4;
				return CreateBool(true);
			}
			else if (Detail::MatchString(it, "false")) {
				it += 5;
				return CreateBool(false);
			}
			else if (*it == '"') {
				std::string::iterator start = it + 1;
				std::string::iterator end = std::find(start, range.second, '"');
				it = end + 1;
				return CreateString(start, end);
			}
			else {
				std::string::iterator start = it;
				if (!isdigit(*start) && *it != '-') {
					Detail::Error(start, range, "Expected a number");
				}

				bool isFloat = false;
				while (it != range.second) {
					if (isspace(*it) || *it == 'f' || *it == ',' || *it == '}' || *it == ']') {
						std::string value(start, it);
						if (*it == 'f') { ++it; }
						if (isFloat) {
							return CreateFloat((float)::atof(value.c_str()));
						}
						else {
							return CreateInt(::atoi(value.c_str()));
						}
					}
					else if (*it == '.') {
						isFloat = true;
						++it;
					}
					else if (!isdigit(*it) && *it != 'e' && *it != '-') {
						Detail::Error(start, range, "Expected a valid number");
					}
					else {
						++it;
					}
				}

				Detail::Error(start, range, "Unknown value");
			}
		}
		throw std::runtime_error("How did you break it this bad?");
	}

	static Object CreateBool(bool value) {
		Object obj(Bool);
		obj.BoolValue = value;
		return obj;
	}

	static Object CreateString(std::string::iterator start, std::string::iterator end) {
		Object obj(String);
		obj.StringValue = std::string(start, end);
		return obj;
	}

	static Object CreateInt(int value) {
		Object obj(Int);
		obj.IntValue = value;
		return obj;
	}

	static Object CreateFloat(float value) {
		Object obj(Float);
		obj.FloatValue = value;
		return obj;
	}

	std::map<std::string, Object> Children;
	std::vector<Object> ListValue;
	std::string StringValue;

	union {
		bool BoolValue;
		int IntValue;
		float FloatValue;
	};

	template <typename T>
	friend struct ConvertDDF;

public:

	static Object FromFile(const std::string& filename) {
		std::ifstream file(filename);
		if (!file.good()) {
			throw std::runtime_error("Failed to open file " + filename);
		}

		std::string string, line;

		while (file.good()) {
			std::getline(file, line);
			string += line + "\n";
		}

		return FromString(string);
	}

	static Object FromString(std::string& string) {
		std::string::iterator token = std::begin(string);
		return ParseObject(token, std::make_pair(std::begin(string), std::end(string)));
	}

	enum ObjectType {
		Float,
		Int,
		Bool,
		String,
		List,
		Map,
		Null
	} Type;

	Object(ObjectType type = Null) : Type(type) {
	}

	Object(Object&& move) : Type(move.Type), FloatValue(move.FloatValue) {
		std::swap(Children, move.Children);
		std::swap(StringValue, move.StringValue);
		std::swap(ListValue, move.ListValue);
	}

	Object& operator=(Object&& rhs) {
		Type = rhs.Type;
		FloatValue = rhs.FloatValue;
		std::swap(Children, rhs.Children);
		std::swap(StringValue, rhs.StringValue);
		std::swap(ListValue, rhs.ListValue);
		return *this;
	}

	Object& operator[](std::string key) {
		if (Type != Map) {
			throw std::runtime_error("Object is not a map object");
		}
		return Children[key];
	}

	Object& operator[](int index) {
		if (Type != List) {
			throw std::runtime_error("Object is not a list object");
		}
		return ListValue[index];
	}

	template <typename Type>
	Type As() {
		ConvertDDF<Type> t;
		return t(*this);
	}

	std::map<std::string, Object>::iterator begin() {
		if (Type == ObjectType::Map) { return std::begin(Children); }
		throw std::runtime_error("Cannot iterate object");
	}

	std::map<std::string, Object>::iterator end() {
		if (Type == ObjectType::Map) { return std::end(Children); }
		throw std::runtime_error("Cannot iterate object");
	}

};



template <>
struct ConvertDDF<std::string> {
	std::string operator()(Object& obj) {
		if (obj.Type == Object::String) { return obj.StringValue; }
		throw std::runtime_error("Object is not a string");
	}
};

template <>
struct ConvertDDF<float> {
	float operator()(Object& obj) {
		if (obj.Type == Object::Float) { return obj.FloatValue; }
		if (obj.Type == Object::Int) { return (float)obj.IntValue; }
		throw std::runtime_error("Object is not a float");
	}
};

template <>
struct ConvertDDF<int> {
	int operator()(Object& obj) {
		if (obj.Type == Object::Int) { return obj.IntValue; }
		throw std::runtime_error("Object is not an integer");
	}
};

template <>
struct ConvertDDF<bool> {
	bool operator()(Object& obj) {
		if (obj.Type == Object::Bool) { return obj.BoolValue; }
		throw std::runtime_error("Object is not a boolean");
	}
};

template <>
struct ConvertDDF<glm::vec3> {
	glm::vec3 operator()(Object& obj) {
		if (obj.Type == Object::List && obj.ListValue.size() == 3) {
			return glm::vec3(obj.ListValue[0].As<float>(), obj.ListValue[1].As<float>(), obj.ListValue[2].As<float>());
		}
		throw std::runtime_error("Object is not a vec3");
	}
};

template <>
struct ConvertDDF<glm::vec2> {
	glm::vec2 operator()(Object& obj) {
		if (obj.Type == Object::List && obj.ListValue.size() == 2) {
			return glm::vec2(obj.ListValue[0].As<float>(), obj.ListValue[1].As<float>());
		}
		throw std::runtime_error("Object is not a vec2");
	}
};

template <typename ListType>
struct ConvertDDF<std::vector<ListType>> {
	std::vector<ListType> operator()(Object& obj) {
		if (obj.Type != Object::List) {
			throw std::runtime_error("Object is not a list");
		}
		std::vector<ListType> result;
		result.reserve(obj.ListValue.size());
		for (auto& element : obj.ListValue) {
			result.push_back(element.As<ListType>());
		}
		return result;
	}
};