#pragma once

#include "../Includes.hpp"
#include "Blob.hpp"

class File {

	// Get the files size and set the read pos to the begining of the file
	static uint32_t GetFileSize(std::ifstream& file);

public:

	static std::string Read(const std::string& filename);
	static Blob ReadBinary(const std::string& filename);

	static void WriteBinary(const std::string& filename, Blob& data);

	static std::string GetFilenameWithoutPath(const std::string& filename);
	static std::string ReplaceExtension(const std::string& filename, const std::string& newExtension);
	static std::string GetExtension(const std::string& filename);

	static bool Exists(const std::string& filename);

	static bool IsOlderThan(const std::string& first, const std::string& second);
};