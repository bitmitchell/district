#pragma once

#include "Log.hpp"
#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>


class ThreadPool {

	static std::vector<std::thread> workers;
	static std::queue<std::function<void()>> tasks;

	static std::mutex queueMutex;
	static std::condition_variable condition, finished;

	static std::atomic_bool stop;
	static std::atomic_int busy;

public:

	static void Initialize() {
		int numberOfThreads = std::thread::hardware_concurrency();
		if (numberOfThreads == 0) {
			Log::Error("Unable to determine number of hardware threads");
		}

		numberOfThreads = std::max(1, numberOfThreads - 1);
		stop = false;

		for (int i = 0; i < numberOfThreads; ++i) {
			workers.emplace_back(
				[] {
				while (true) {
					std::function<void()> task;

					{
						std::unique_lock<std::mutex> lock(queueMutex);
						condition.wait(lock, [] {
							return stop || !tasks.empty();
						});

						if (stop && tasks.empty()) {
							return;
						}

						busy++;
						task = std::move(tasks.front());
						tasks.pop();
					}

					task();

					{
						std::unique_lock<std::mutex> lock(queueMutex);
						busy--;
						if (tasks.empty() && busy == 0) {
							finished.notify_one();
						}
					}
					
				}
			});
		}
	}


	template <class F, class... Args>
	static std::future<typename std::result_of<F(Args...)>::type> Queue(F&& f, Args&&... args) {
		using return_type = typename std::result_of<F(Args...)>::type;

		auto task = std::make_shared<std::packaged_task<return_type()>>(std::bind(std::forward<F>(f), std::forward<Args>(args)...));

		std::future<return_type> result = task->get_future();
		{
			std::unique_lock<std::mutex> lock(queueMutex);

			if (stop) {
				Log::Error("Queue on stopped thread pool");
			}

			tasks.emplace([task] () { (*task)(); });
		}
		condition.notify_one();
		return result;
	}


	static void Wait() {
		std::unique_lock<std::mutex> lock(queueMutex);
		finished.wait(lock);
	}


	static void Shutdown() {
		{
			std::unique_lock<std::mutex> lock(queueMutex);
			stop = true;
		}
		condition.notify_all();
		for (auto& worker : workers) {
			worker.join();
		}
	}

};