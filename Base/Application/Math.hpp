#pragma once
#include "../Includes.hpp"


template <class FType>
FType Clamp(FType x, FType lower, FType upper) {
	return x < lower ? lower : (x > upper ? upper : x);
}


template <class FType>
FType Wrap(FType x, FType lower, FType upper) {
	FType diff = upper - lower;
	while (x >= upper) { x -= diff; }
	while (x < lower) { x += diff; }
	return x;
}


template <class FType, class FInterp>
FType Lerp(FType lower, FType upper, FInterp x) {
	return x * upper + (FInterp(1) - x) * lower;
}