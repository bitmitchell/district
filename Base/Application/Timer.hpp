#pragma once
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>


class Timer {
	LARGE_INTEGER time, freq;
	double paused, pausedAt;
	bool isRunning;

	double _time(void) const {
		LARGE_INTEGER now;
		QueryPerformanceCounter(&now);

		long long delta = now.QuadPart - time.QuadPart;
		return double(delta) / freq.QuadPart;
	}

public:
	Timer(void) {
		QueryPerformanceFrequency(&freq);
		Reset();
		pausedAt = 0.0f;
		isRunning = true;
	}


	void Reset(void) {
		QueryPerformanceCounter(&time);
		paused = 0.0f;
		pausedAt = 0.0f;
	}


	double Time(void) const {
		if (isRunning) {
			return _time() - paused;
		}

		return pausedAt - paused;
	}


	void Pause(void) {
		pausedAt = _time();
		isRunning = false;
	}


	void Resume(void) {
		isRunning = true;
		paused += _time() - pausedAt;
	}


	void SetTo(double time) {
		paused += Time() - time;
	}
};