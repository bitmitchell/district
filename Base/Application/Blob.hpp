#pragma once
#include "../Includes.hpp"

struct Blob {
	uint32_t Size;
	uint8_t* Data;

	Blob() : Data(nullptr), Size(0) {
	}

	Blob(uint8_t* data, uint32_t size) : Data(data), Size(size) {
	}

	void Release() {
		delete[] Data;
		Data = nullptr;
		Size = 0;
	}
};