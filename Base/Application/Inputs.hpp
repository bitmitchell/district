#pragma once

namespace Keys {
	enum Key {
		Backspace = 0x08,
		Tab,
		Clear = 0x0C,
		Enter,
		Shift = 0x10,
		Control,
		Alt,
		Pause,
		CapsLock,
		Escape = 0x1B,
		Space = 0x20,
		PageUp,
		PageDown,
		End,
		Home,
		Left,
		Up,
		Right,
		Down,
		Select,
		Insert = 0x2D,
		Delete,
		D0 = 0x30, D1, D2, D3, D4, D5, D6, D7, D8, D9,
		A = 0x41, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
		Num0 = 0x60, Num1, Num2, Num3, Num4, Num5, Num6, Num7, Num8, Num9,
		Multiply,
		Add,
		Separator,
		Subtract,
		Decimal,
		Divide,
		F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, F13, F14, F15, F16, F17, F18, F19, F20, F21, F22, F23, F24,
		NumLock = 0x90,
		ScrollLock,
		Tilde = 0xC0
	};
}

namespace Mouse {
	enum Button {
		LeftButton,
		RightButton,
		MiddleButton
	};
}


class IInputContext {
public:
	virtual void OnKeyDown(Keys::Key key) {};
	virtual void OnKeyUp(Keys::Key key) {};
	virtual void OnChar(char character) {};

	virtual void OnMouseDown(Mouse::Button button, int x, int y) {};
	virtual void OnMouseUp(Mouse::Button button, int x, int y) {};
	virtual void OnMouseMove(int x, int y) {};
};