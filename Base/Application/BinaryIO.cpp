#include "Binary.hpp"
#include "Log.hpp"

uint8_t BinaryReader::ReadByte() {
	if (position >= data->Size) {
		Log::Error("Reading too much out of buffer");
	}
	return data->Data[position++];
}


uint16_t BinaryReader::ReadShort() {
	uint16_t lo = ReadByte();
	uint16_t hi = ReadByte();
	return lo | (hi << 8);
}


uint32_t BinaryReader::ReadLong() {
	uint32_t lo = ReadShort();
	uint32_t hi = ReadShort();
	return lo | (hi << 16);
}


uint64_t BinaryReader::ReadLongLong() {
	uint64_t lo = ReadLong();
	uint64_t hi = ReadLong();
	return lo | (hi << 32);
}


BinaryReader::BinaryReader(const Blob* data) : position(0), data(data) {
}


BinaryReader& BinaryReader::operator>> (uint8_t& out) {
	out = ReadByte();
	return *this;
}


BinaryReader& BinaryReader::operator>> (uint16_t& out) {
	out = ReadShort();
	return *this;
}


BinaryReader& BinaryReader::operator>> (uint32_t& out) {
	out = ReadLong();
	return *this;
}


BinaryReader& BinaryReader::operator>> (uint64_t& out) {
	out = ReadLongLong();
	return *this;
}


BinaryReader& BinaryReader::operator>> (int8_t& out) {
	uint8_t value = ReadByte();
	out = *reinterpret_cast<int8_t*>(&value);
	return *this;
}


BinaryReader& BinaryReader::operator>> (int16_t& out) {
	uint16_t value = ReadShort();
	out = *reinterpret_cast<int16_t*>(&value);
	return *this;
}


BinaryReader& BinaryReader::operator>> (int32_t& out) {
	uint32_t value = ReadLong();
	out = *reinterpret_cast<int32_t*>(&value);
	return *this;
}


BinaryReader& BinaryReader::operator>> (int64_t& out) {
	uint64_t value = ReadLongLong();
	out = *reinterpret_cast<int64_t*>(&value);
	return *this;
}


BinaryReader& BinaryReader::operator>> (float& out) {
	uint32_t value = ReadLong();
	out = *reinterpret_cast<float*>(&value);
	return *this;
}


BinaryReader& BinaryReader::operator>> (double& out) {
	uint64_t value = ReadLongLong();
	out = *reinterpret_cast<double*>(&value);
	return *this;
}


BinaryReader& BinaryReader::operator>> (std::string& out) {
	uint16_t length = ReadShort();
	out.clear();
	out.resize(length);
	for (uint16_t i = 0; i < length; ++i) {
		out[i] = (char)ReadByte();
	}
	return *this;
}


BinaryReader& BinaryReader::operator>> (glm::vec3& out) {
	*this >> out.x;
	*this >> out.y;
	*this >> out.z;
	return *this;
}


BinaryReader& BinaryReader::operator>> (glm::quat& out) {
	return *this >> out.w >> out.x >> out.y >> out.z;
}


BinaryReader& BinaryReader::operator>> (glm::vec2& out) {
	*this >> out.x;
	*this >> out.y;
	return *this;
}


BinaryReader& BinaryReader::operator>> (Vertex& out) {
	*this >> out.Position;
	*this >> out.Normal;
	*this >> out.Texture;
	return *this;
}


BinaryReader& BinaryReader::operator>> (Triangle& out) {
	*this >> out.A;
	*this >> out.B;
	*this >> out.C;
	return *this;
}


// BinaryWriter

void BinaryWriter::WriteByte(uint8_t v) {
	if (position >= data->Size) {
		Log::Error("Writing too much into buffer");
	}
	data->Data[position++] = v;
}


void BinaryWriter::WriteShort(uint16_t v) {
	WriteByte(v & 0xff);
	WriteByte((v >> 8) & 0xff);
}


void BinaryWriter::WriteLong(uint32_t v) {
	WriteShort(v & 0xffff);
	WriteShort((v >> 16) & 0xffff);
}


void BinaryWriter::WriteLongLong(uint64_t v) {
	WriteLong(v & 0xffffffff);
	WriteLong((v >> 32) & 0xffffffff);
}


BinaryWriter::BinaryWriter(Blob* data) : position(0), data(data) {
}


BinaryWriter& BinaryWriter::operator<< (const uint8_t& v) {
	WriteByte(v);
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const uint16_t& v) {
	WriteShort(v);
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const uint32_t& v) {
	WriteLong(v);
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const uint64_t& v) {
	WriteLongLong(v);
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const int8_t& v) {
	WriteByte(*reinterpret_cast<const uint8_t*>(&v));
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const int16_t& v) {
	WriteShort(*reinterpret_cast<const uint16_t*>(&v));
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const int32_t& v) {
	WriteLong(*reinterpret_cast<const uint32_t*>(&v));
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const int64_t& v) {
	WriteLongLong(*reinterpret_cast<const uint64_t*>(&v));
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const float& v) {
	WriteLong(*reinterpret_cast<const uint32_t*>(&v));
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const double& v) {
	WriteLongLong(*reinterpret_cast<const uint64_t*>(&v));
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const std::string& str) {
	WriteShort(str.size());
	for (uint16_t i = 0; i < str.size(); ++i) {
		WriteByte(str[i]);
	}
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const glm::vec2& vec) {
	*this << vec.x;
	*this << vec.y;
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const glm::vec3& vec) {
	*this << vec.x;
	*this << vec.y;
	*this << vec.z;
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const glm::quat& rot) {
	return *this << rot.w << rot.x << rot.y << rot.z;
}


BinaryWriter& BinaryWriter::operator<< (const Vertex& vert) {
	*this << vert.Position;
	*this << vert.Normal;
	*this << vert.Texture;
	return *this;
}


BinaryWriter& BinaryWriter::operator<< (const Triangle& tri) {
	*this << tri.A;
	*this << tri.B;
	*this << tri.C;
	return *this;
}