#pragma once
#include "../Includes.hpp"
#include "Timer.hpp"


class PerformanceTimer {
	struct TimingBlock {
		double LastRun;
		double RunTime;
		int NumberOfRuns;
	};

	static std::map<std::string, TimingBlock> blockTimes;
	static Timer timer;

	std::string name;
	double start;

public:

	PerformanceTimer(std::string blockName);
	~PerformanceTimer();

	static void ExportData(std::string filename);
	static double LastRunTime(std::string blockName);
};


#define PerfFunction PerformanceTimer PERFTIMER_DONOTTOUCH(__FUNCTION__);