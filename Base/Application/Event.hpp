#pragma once

#include <functional>
#include <unordered_set>

template <typename ...Args>
using Delegate = std::function<void(Args...)>;

template <typename ...Args>
using DelegateFuncType = void(Args...);

namespace detail {
	template <typename ...Args>
	struct DelegateHash {
		std::size_t operator()(const Delegate<Args...>& func) {
			auto ptr = func.target<DelegateFuncType<Args...>>();
			if (ptr) {
				return reinterpret_cast<unsigned int>(*ptr);
			} else {
				return func.target_type().hash_code();
			}
		}
	};

	template <typename ...Args>
	struct DelegateEqual {
		bool operator()(const Delegate<Args...>& lhs, const Delegate<Args...>& rhs) {
			DelegateHash<Args...> hash;
			return hash(lhs) == hash(rhs);
		}
	};
}

template <typename ...Args>
class Event {
	std::unordered_set<Delegate<Args...>, detail::DelegateHash<Args...>, detail::DelegateEqual<Args...>> delegates;

public:

	void operator() (const Args& ...args) {
		for (auto& func : delegates) {
			func(args...);
		}
	}

	void operator+= (const Delegate<Args...>& func) {
		delegates.insert(func);
	}

	void operator-= (const Delegate<Args...>& func) {
		delegates.erase(func);
	}

	void Clear() {
		delegates.clear();
	}

};