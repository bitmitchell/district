#include "Window.hpp"

#define WINDOWS_CLASS_NAME	"districtWindow"


HINSTANCE Window::instance = nullptr;


LRESULT Window::WindowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	Window* window = (Window*)GetWindowLongPtr(hwnd, GWLP_USERDATA);

	if (window) {
		return window->WindowProc(msg, wparam, lparam);
	} else {
		return DefWindowProc(hwnd, msg, wparam, lparam);
	}
}


LRESULT Window::WindowProc(UINT msg, WPARAM wparam, LPARAM lparam)
{
	// Events to be handled regardless of focus
	switch (msg) {
	case WM_CLOSE:
		OnClose();
		break;
	case WM_KILLFOCUS:
		if (!cursorVisible) {
			while (::ShowCursor(true) < 0) ;
		}
		LostFocus();
		break;
	case WM_SETFOCUS:
		if (!cursorVisible) {
			while (::ShowCursor(false) >= 0) ;
		}
		GotFocus();
		break;
	case WM_SHOWWINDOW:
		SetCapture(handle);
		break;
	}

	// events to be handled if the window is focused
	if (HasFocus()) {
		switch (msg) {
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			if (!inputContext.empty()) {
				inputContext.back()->OnKeyDown((Keys::Key)wparam);
			}
			break;
		case WM_KEYUP:
		case WM_SYSKEYUP:
			if (!inputContext.empty()) {
				inputContext.back()->OnKeyUp((Keys::Key)wparam);
			}
			break;
		case WM_CHAR:
			if (!inputContext.empty()) {
				inputContext.back()->OnChar(wparam);
			}
			break;
		case WM_LBUTTONDOWN:
			if (!inputContext.empty()) {
				inputContext.back()->OnMouseDown(Mouse::LeftButton, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			}
			break;
		case WM_LBUTTONUP:
			if (!inputContext.empty()) {
				inputContext.back()->OnMouseUp(Mouse::LeftButton, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			}
			break;
		case WM_MBUTTONDOWN:
			if (!inputContext.empty()) {
				inputContext.back()->OnMouseDown(Mouse::MiddleButton, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			}
			break;
		case WM_MBUTTONUP:
			if (!inputContext.empty()) {
				inputContext.back()->OnMouseUp(Mouse::MiddleButton, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			}
			break;
		case WM_RBUTTONDOWN:
			if (!inputContext.empty()) {
				inputContext.back()->OnMouseDown(Mouse::RightButton, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			}
			break;
		case WM_RBUTTONUP:
			if (!inputContext.empty()) {
				inputContext.back()->OnMouseUp(Mouse::RightButton, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			}
			break;
		case WM_MOUSEMOVE:
			if (!inputContext.empty()) {
				inputContext.back()->OnMouseMove(GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			}
			break;
		}
	}

	return DefWindowProc(handle, msg, wparam, lparam);
}


Window::Window() : handle(nullptr), cursorVisible(true) {
}


void Window::Register(HINSTANCE instance) {
	WNDCLASS wndClass;
	wndClass.hInstance = instance;
	wndClass.lpszClassName = WINDOWS_CLASS_NAME;
	wndClass.style = 0;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wndClass.hCursor = LoadCursor(instance, IDC_ARROW);
	wndClass.hIcon = LoadIcon(instance, IDI_APPLICATION);
	wndClass.lpszMenuName = nullptr;

	wndClass.lpfnWndProc = Window::WindowProc;

	RegisterClass(&wndClass);
}


void Window::Unregister() {
	UnregisterClass(WINDOWS_CLASS_NAME, instance);
}


void Window::Create(Options& opt) {

	DWORD style = WS_SYSMENU | WS_CAPTION;

	RECT client = { 0, 0, opt.Width, opt.Height};
	AdjustWindowRect(&client, style, false);

	int width = client.right - client.left;
	int height = client.bottom - client.top;

	handle = CreateWindow(WINDOWS_CLASS_NAME, "District",
		style, opt.PosX, opt.PosY,
		width, height, nullptr, nullptr, instance, nullptr);

	SetWindowLongPtr(handle, GWLP_USERDATA, (LONG)this);
}


void Window::ClearEvents() {
	OnClose.Clear();
	LostFocus.Clear();
	GotFocus.Clear();
}


void Window::Close() {
	PostMessage(handle, WM_CLOSE, 0, 0);
}


void Window::MessagePump() {
	MSG msg = { 0 };
	while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}


HWND Window::GetHandle() {
	return handle;
}


void Window::Show() {
	ShowWindow(handle, SW_SHOW);
}


void Window::FlashTaskBar() {
	FlashWindow(handle, true);
}


void Window::SetFullscreen(bool state) {
	DWORD dwStyle = GetWindowLong(handle, GWL_STYLE);
	if (dwStyle & WS_OVERLAPPEDWINDOW && state) {
		MONITORINFO mi = { sizeof(mi) };
		if (GetWindowPlacement(handle, &prevWindowPos) && GetMonitorInfo(MonitorFromWindow(handle, MONITOR_DEFAULTTOPRIMARY), &mi)) {
			SetWindowLong(handle, GWL_STYLE,
				dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(handle, HWND_TOP,
				mi.rcMonitor.left, mi.rcMonitor.top,
				mi.rcMonitor.right - mi.rcMonitor.left,
				mi.rcMonitor.bottom - mi.rcMonitor.top,
				SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		}
	} else if ((dwStyle & WS_OVERLAPPEDWINDOW) == 0 && !state) {
		SetWindowLong(handle, GWL_STYLE,
			dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(handle, &prevWindowPos);
		SetWindowPos(handle, NULL, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
			SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
	}
}


bool Window::IsFullscreen() {
	DWORD dwStyle = GetWindowLong(handle, GWL_STYLE);
	return (dwStyle & WS_OVERLAPPEDWINDOW) == 0;
}


void Window::SetCursorPos(int x, int y) {
	if (HasFocus()) {
		POINT center = { x, y };
		ClientToScreen(handle, &center);
		::SetCursorPos(center.x, center.y);
	}
}


void Window::ShowCursor(bool show) {
	while (show && !cursorVisible) {
		cursorVisible = ::ShowCursor(true) >= 0;
	}
	while (!show && cursorVisible) {
		cursorVisible = ::ShowCursor(false) >= 0;
	}
}


bool Window::HasFocus() {
	return GetFocus() == handle;
}


void Window::PushInputContext(IInputContext* context) {
	inputContext.push_back(context);
}


void Window::PopInputContext(IInputContext* context) {
	inputContext.erase(std::find(inputContext.begin(), inputContext.end(), context));
}


void Window::ClearInputContexts() {
	inputContext.clear();
}