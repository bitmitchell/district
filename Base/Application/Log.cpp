#include "Log.hpp"

namespace {
	const int MaxConsoleLines = 10;
}

namespace std {
	ostream& operator<<(ostream& os, const glm::vec3& value) {
		os << "(" << value.x << ", " << value.y << ", " << value.z << ")";
		return os;
	}
}

enum Color {
	Red,
	Green,
	Blue,
	White
};

HANDLE stdOut;
CONSOLE_SCREEN_BUFFER_INFO csbi;
std::deque <std::string> consoleMessages;

void SetOutputColor(Color color) {
	WORD attribute = 0;

	switch (color) {
	case Red:
		attribute = 0xC;
		break;
	case Green:
		attribute = 0xA;
		break;
	case Blue:
		attribute = 0xA;
		break;
	case White:
		attribute = 0xF;
		break;
	}

	SetConsoleTextAttribute(stdOut, attribute);
}


void Log::Init() {
	stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(stdOut, &csbi);
}


void Log::Shutdown() {
	SetConsoleTextAttribute(stdOut, csbi.wAttributes);
	consoleMessages.clear();
}

const std::deque<std::string>& Log::GetConsoleMessages(){
	return consoleMessages;
}

void Log::Info(const std::string& str) {
	UpdateConsoleMessages(str);
#if defined(DEBUG)
	SetOutputColor(Color::White);
	printf("%s\n", str.c_str());
#endif
}

void Log::UpdateConsoleMessages(std::string str) {
	if (consoleMessages.size() == MaxConsoleLines){
		consoleMessages.pop_front();
	}
	consoleMessages.push_back(str);
}

void Log::Warning(const std::string& str) {
	UpdateConsoleMessages("WARNING: " + str);
#if defined(DEBUG)
	SetOutputColor(Color::Red);
	printf("WARNING: %s\n", str.c_str());
#endif
}


void Log::Error(const std::string& str) {
#if defined(DEBUG)
	Log::Info("ERROR: " + str);
	DebugBreak(); // Allows the dev to go back through the stack trace and examine the problem
#else
	MessageBoxA(nullptr, str.c_str(), "Fatal Error", MB_ICONERROR);
	abort();
#endif
}
