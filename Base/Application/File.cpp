#include "File.hpp"
#include "Log.hpp"

uint32_t File::GetFileSize(std::ifstream& file) {
	file.seekg(0, std::ios_base::end);
	uint32_t end = (uint32_t)file.tellg();
	file.seekg(0, std::ios_base::beg);
	uint32_t start = (uint32_t)file.tellg();

	return end - start;
}


std::string File::Read(const std::string& filename) {
	std::ifstream file(filename);

	if (!file.good()) {
		Log::Error("File not found: %%%", filename);
	}

	uint32_t size = GetFileSize(file);

	std::string out;
	out.reserve(size);

	out.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

	return out;
}


Blob File::ReadBinary(const std::string& filename) {
	std::ifstream file(filename, std::ios_base::binary);

	if (!file.good()) {
		Log::Error("File not found: %%%", filename);
	}

	uint32_t size = GetFileSize(file);

	Blob blob(new uint8_t[size], size);

	file.read(reinterpret_cast<char*>(blob.Data), blob.Size);

	file.close();

	return blob;
}


void File::WriteBinary(const std::string& filename, Blob& data) {
	std::ofstream file(filename, std::ios_base::binary);

	if (!file.good()) {
		Log::Error("Error writing file: %%%", filename);
	}

	file.write((const char*)data.Data, data.Size);

	file.close();
}


std::string File::GetFilenameWithoutPath(const std::string& filename) {
	char name[MAX_PATH] = { 0 };
	char ext[MAX_PATH] = { 0 };

	_splitpath_s(filename.c_str(), nullptr, 0, nullptr, 0, name, MAX_PATH, ext, MAX_PATH);

	return std::string(name) + std::string(ext);
}


std::string File::ReplaceExtension(const std::string& filename, const std::string& newExt) {
	char drive[MAX_PATH] = { 0 };
	char dir[MAX_PATH] = { 0 };
	char name[MAX_PATH] = { 0 };

	_splitpath_s(filename.c_str(), drive, MAX_PATH, dir, MAX_PATH, name, MAX_PATH, nullptr, 0);

	return std::string(drive) + std::string(dir) + std::string(name) + newExt;
}


std::string File::GetExtension(const std::string& filename) {
	char ext[MAX_PATH] = { 0 };

	_splitpath_s(filename.c_str(), nullptr, 0, nullptr, 0, nullptr, 0, ext, MAX_PATH);

	return std::string(ext);
}


bool File::Exists(const std::string& filename) {
	std::ifstream file(filename);
	bool exists = file.good();
	file.close();
	return exists;
}


bool File::IsOlderThan(const std::string& first, const std::string& second) {
	HANDLE firstFile = CreateFile(first.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, 0, NULL);
	HANDLE secondFile = CreateFile(second.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, 0, NULL);

	if (firstFile == INVALID_HANDLE_VALUE || secondFile == INVALID_HANDLE_VALUE) {
		Log::Warning("Invalid file handles");
		return false;
	}

	FILETIME firstTime, secondTime;
	if (!GetFileTime(firstFile, nullptr, nullptr, &firstTime)) {
		Log::Warning("Error getting file time");
		return false;
	}

	if (!GetFileTime(secondFile, nullptr, nullptr, &secondTime)) {
		Log::Warning("Error getting file time");
		return false;
	}

	bool isOlderThan = CompareFileTime(&firstTime, &secondTime) == -1;

	CloseHandle(firstFile);
	CloseHandle(secondFile);

	return isOlderThan;
}