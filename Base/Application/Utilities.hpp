#pragma once
#include "../Includes.hpp"
#include "Log.hpp"

namespace Utilites {

	inline bool CompareStart(std::string string, std::string compare) {
		return string.compare(0, compare.length(), compare) == 0;
	}

	inline std::vector<std::string> Split(const std::string& input, char delim = ' ') {
		std::stringstream ss(input);
		std::string item;
		std::vector<std::string> elems;
		while (std::getline(ss, item, delim)) {
			if (item.size()) {
				elems.push_back(item);
			}
		}
		return elems;
	}

	inline std::string Format(const std::string& str) {
		return str;
	}

	template <typename Arg1, typename ...Args>
	inline std::string Format(const std::string &fmt, const Arg1 &value, const Args&... args) {
		auto split = fmt.find("%%%");

		if (split == fmt.npos) {
			Log::Error("Not enough arguments for string formatting");
		}

		std::stringstream str;
		str << fmt.substr(0, split);
		str << value;
		str << fmt.substr(split + 3);
		return Format(str.str(), args...);
	}
};