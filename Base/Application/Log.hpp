#pragma once
#include "../Includes.hpp"

namespace Log {
	void Init();
	void Shutdown();

	const std::deque<std::string>& GetConsoleMessages();
	void UpdateConsoleMessages(std::string str);

	void Error(const std::string& str);
	

	template <typename Arg1, typename ...Args>
	inline void Error(const std::string &fmt, const Arg1 &value, const Args&... args) {
		auto split = fmt.find("%%%");

		if (split == fmt.npos) {
			Error("Badly formatted error, I give up!");
		}

		std::stringstream str;
		str << fmt.substr(0, split);
		str << value;
		str << fmt.substr(split + 3);
		Error(str.str(), args...);
	}

	void Info(const std::string& str);

	template <typename Arg1, typename ...Args>
	inline void Info(const std::string &fmt, const Arg1 &value, const Args&... args) {
		auto split = fmt.find("%%%");

		if (split == fmt.npos) {
			Error("Not enough arguments for Info formatting");
		}

		std::stringstream str;
		str << fmt.substr(0, split);
		str << value;
		str << fmt.substr(split + 3);
		Info(str.str(), args...);
	}


	void Warning(const std::string& str);

	template <typename Arg1, typename ...Args>
	inline void Warning(const std::string &fmt, const Arg1 &value, const Args&... args) {
		auto split = fmt.find("%%%");

		if (split == fmt.npos) {
			Error("Not enough arguments for Warning formatting");
		}

		std::stringstream str;
		str << fmt.substr(0, split);
		str << value;
		str << fmt.substr(split + 3);
		Warning(str.str(), args...);
	}
}

namespace std {
	ostream& operator<<(ostream& os, const glm::vec3& value);
}