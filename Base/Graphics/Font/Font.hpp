#pragma once

#include "../../Includes.hpp"

#include "../Material.hpp"
#include "../Buffer.hpp"

#if defined(FONT_FACTORY)
#include <ft2build.h>
#include FT_FREETYPE_H
#endif

class PixelShader;
class VertexShader;
class Geometry;
class Camera;

class Font {

	struct Character {
		int Code;
		float Left, Top, Bottom, Right;
		int Width;
		int Height;
		int Advance;
		int BearingX, BearingY;

		Character() {
		}

#if defined(FONT_FACTORY)
		Character(int code, FT_Glyph_Metrics& metric, int x, int y, int texWidth, int texHeight) :
			Code(code), Width(metric.width / 64), Height(metric.height / 64), Advance(metric.horiAdvance / 64),
			BearingX(metric.horiBearingX / 64), BearingY(metric.horiBearingY / 64) {
			Left = float(x) / float(texWidth);
			Top = float(y) / float(texHeight);
			Right = Left + float(Width) / float(texWidth);
			Bottom = Top + float(Height) / float(texHeight);
		}
#endif
	};

	struct KerningPair {
		uint32_t Left, Right;
		float Advance;

		KerningPair(uint32_t left = 0, uint32_t right = 0, float advance = 0) :
			Left(left), Right(right), Advance(advance) {
		}

		static uint64_t GetKey(uint32_t left, uint32_t right) {
			return uint64_t(right) << 32 | left;
		}

		uint64_t GetKey() {
			return GetKey(Left, Right);
		}
	};

	Texture2D* characterAtlas;
	Material characterMaterial;
	Buffer* textBuffer;
	Geometry* geometry;
	std::unordered_map<int, Character> character;
	std::unordered_map<uint64_t, KerningPair> kerning;

	PixelShader* pixelShader;
	VertexShader* vertexShader;

	float size, baseline;

	friend class FontFactory;

	void BufferText(Graphics* graphics, std::string text);

public:

	Font() : characterAtlas(nullptr) {
	}

	void Release();

	const Character& Get(int code) {
		return character[code];
	}

	bool HasKerning(int first, int second) {
		return kerning.count(KerningPair::GetKey(first, second)) > 0;
	}

	const KerningPair& GetKerning(int first, int second) {
		return kerning[KerningPair::GetKey(first, second)];
	}

	void Draw(Graphics* graphics, std::string text, float x, float y, const glm::vec4& color);
	void Draw3D(Graphics* graphics, Camera* camera, std::string text, glm::mat4& transform, const glm::vec4& color);

	float MeasureText(std::string text);
	float GetSize();
	float GetBearing();
};

