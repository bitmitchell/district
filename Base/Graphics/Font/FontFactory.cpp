#define FONT_FACTORY

#include "FontFactory.hpp"
#include "../../Application/Log.hpp"
#include "../Texture2D.hpp"
#include "../Graphics.hpp"
#include "../Vertex.hpp"
#include "../Geometry.hpp"
#include "../VertexShader.hpp"
#include "../PixelShader.hpp"



FT_Library FontFactory::library;
std::map<std::string, Font*> FontFactory::fontCache;


void FontFactory::Initialize() {
	FT_Error error = FT_Init_FreeType(&library);

	if (error) {
		Log::Error("Error creating freetype font library");
	}
}


void FontFactory::Shutdown() {
	for (auto& font : fontCache) {
		font.second->Release();
		delete font.second;
	}
	fontCache.clear();

	FT_Error error = FT_Done_FreeType(library);

	if (error) {
		Log::Error("Error releasing freetype libary");
	}
}


void FontFactory::LoadCharacterIntoTexture(uint32_t index, FT_Face face, uint8_t* data, int xOff, int yOff, int dataPitch) {
	FT_Error error = FT_Load_Glyph(face, index, FT_LOAD_DEFAULT);
	if (error) {
		Log::Error("Error loading character for font");
	}

	error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);
	if (error) {
		Log::Error("Error rendering character for font");
	}

	FT_Bitmap bitmap = face->glyph->bitmap;

	for (int y = 0; y < bitmap.rows; ++y) {
		for (int x = 0; x < bitmap.width; ++x) {
			data[(x + xOff) + (y + yOff) * dataPitch] = bitmap.buffer[x + y * bitmap.pitch];
		}
	}
}


Font* FontFactory::LoadFont(Graphics* graphics, const std::string& font, int size, int start, int end) {
	std::string key = font + std::to_string(size);
	if (fontCache.count(key)) {
		return fontCache[key];
	}

	FT_Error error;
	FT_Face face;
	error = FT_New_Face(library, font.c_str(), 0, &face);

	if (error) {
		Log::Error("Error loading new font face '%%%'", font);
	}

	Font* fontMap = new Font;
	fontMap->size = float(size);
	fontMap->baseline = float(size * face->ascender / face->units_per_EM);

	int validChars = 0;
	for (int character = start; character <= end; ++character) {
		FT_UInt charIndex = FT_Get_Char_Index(face, character);
		if (charIndex > 0) {
			validChars++;
		}
	}

	int cells = int(sqrt(validChars) + 1);
	int width = size * cells;
	int height = size * cells;

	FT_Set_Pixel_Sizes(face, size, size);
	
	uint8_t* texData = new uint8_t[width * height];
	memset(texData, 0, width * height);

	int index = 0;
	
	for (int character = start; character <= end; ++character) {
		FT_UInt charIndex = FT_Get_Char_Index(face, character);
		if (charIndex == 0) {
			continue;
		}

		int x = index % cells;
		int y = index / cells;

		LoadCharacterIntoTexture(charIndex, face, texData, x * size, y * size, width);
		++index;

		FT_Glyph_Metrics& metric = face->glyph->metrics;
		fontMap->character[character] = Font::Character(character, metric, x * size, y * size, width, height);

		if (FT_HAS_KERNING(face)) {
			for (int second = start; second <= end; ++second) {
				if (second == character) {
					continue;
				}
				FT_UInt secondIndex = FT_Get_Char_Index(face, second);

				FT_Vector advance;
				error = FT_Get_Kerning(face, charIndex, secondIndex, FT_KERNING_DEFAULT, &advance);
				if (error == 0 && advance.x != 0) {
					fontMap->kerning[Font::KerningPair::GetKey(character, second)] = Font::KerningPair(character, second, float(advance.x) / 64);
				}
			}
		}
	}
	
	fontMap->characterAtlas = Texture2D::Create(graphics, width, height, texData, width, DXGI_FORMAT_R8_UNORM);
	fontMap->characterMaterial = { fontMap->characterAtlas, nullptr };
	
	Log::Info("Loaded font '%%%'", font);

	fontMap->textBuffer = Buffer::Create(graphics, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, sizeof(Vertex)* 6 * 512);

	fontMap->geometry = Geometry::Create(fontMap->textBuffer, 0);

	fontMap->vertexShader = VertexShader::Create(graphics, "Shaders/vertex.cso");
	fontMap->pixelShader = PixelShader::Create(graphics, "Shaders/font.cso");

	fontCache[key] = fontMap;

	delete[] texData;

	return fontMap;
}