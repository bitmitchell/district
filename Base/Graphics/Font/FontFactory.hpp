#pragma once

#include "../../Includes.hpp"
#include "Font.hpp"

#if defined(FONT_FACTORY)
#include <ft2build.h>
#include FT_FREETYPE_H
#endif

class Graphics;
class Texture2D;

class FontFactory {
#if defined(FONT_FACTORY)
	static FT_Library library;

	static std::map<std::string, Font*> fontCache;

	static void LoadCharacterIntoTexture(uint32_t index, FT_Face face, uint8_t* data, int x, int y, int dataPitch);
#endif

public:

	static void Initialize();
	static void Shutdown();

	// Loads a font from the given font file using the given size.
	// Loads all characters in the given range (inclusive) with a valid glyph.
	static Font* LoadFont(Graphics* graphics, const std::string& font, int size, int start = 0, int end = 127);
};