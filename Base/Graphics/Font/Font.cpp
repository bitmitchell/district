#define FONT_FACTORY

#include "Font.hpp"
#include "../Vertex.hpp"
#include "../Graphics.hpp"
#include "../Geometry.hpp"
#include "../PixelShader.hpp"
#include "../VertexShader.hpp"
#include "../Camera.hpp"

void Font::BufferText(Graphics* graphics, std::string text) {
	float x = 0;
	int i = 0;
	Vertex* data = new Vertex[text.size() * 6];
	int prevChar = -1;
	for (char character : text) {
		const Character& c = Get(character);

		if (HasKerning(prevChar, character)) {
			x += GetKerning(prevChar, character).Advance;
		}

		float xStart = std::floor(x + c.BearingX);
		float xEnd = std::floor(x + c.Width + c.BearingX);
		float yStart = std::floor(2 * baseline - size - c.BearingY);
		float yEnd = std::floor(2 * baseline - size - c.BearingY + c.Height);

		data[i++] = Vertex(glm::vec3(xStart, yEnd, 0.0f), glm::vec3(), glm::vec2(c.Left, c.Bottom));
		data[i++] = Vertex(glm::vec3(xStart, yStart, 0.0f), glm::vec3(), glm::vec2(c.Left, c.Top));
		data[i++] = Vertex(glm::vec3(xEnd, yStart, 0.0f), glm::vec3(), glm::vec2(c.Right, c.Top));

		data[i++] = Vertex(glm::vec3(xStart, yEnd, 0.0f), glm::vec3(), glm::vec2(c.Left, c.Bottom));
		data[i++] = Vertex(glm::vec3(xEnd, yStart, 0.0f), glm::vec3(), glm::vec2(c.Right, c.Top));
		data[i++] = Vertex(glm::vec3(xEnd, yEnd, 0.0f), glm::vec3(), glm::vec2(c.Right, c.Bottom));

		x += c.Advance;
		prevChar = character;
	};

	textBuffer->SetData(graphics, i * sizeof(Vertex), data);
	geometry->SetVertexCount(i);

	delete[] data;
}

float Font::MeasureText(std::string text) {
	float x = 0;
	int prevChar = -1;
	for (char character : text) {
		const Character& c = Get(character);

		if (HasKerning(prevChar, character)) {
			x += GetKerning(prevChar, character).Advance;
		}

		x += c.Advance;
		prevChar = character;
	};
	return x;
}


float Font::GetSize(){
	return baseline + 1;
}


float Font::GetBearing() {
	return float(Get('A').BearingY);
}


void Font::Release() {
	characterAtlas->Release();
	textBuffer->Release();
}


void Font::Draw(Graphics* graphics, std::string text, float x, float y, const glm::vec4& color) {
	BufferText(graphics, text);

	Transform transform(glm::transpose(glm::ortho(0.0f, (float)graphics->Settings.Width, (float)graphics->Settings.Height, 0.0f)),
		glm::translate(glm::mat4(), glm::vec3(x, y + 1, 0)));
	graphics->SetFontColor(color);
	graphics->Draw(geometry, vertexShader, pixelShader, transform, &characterMaterial);
}


void Font::Draw3D(Graphics* graphics, Camera* camera, std::string text, glm::mat4& world, const glm::vec4& color) {
	BufferText(graphics, text);
	float s = 1.0f / GetSize();
	glm::mat4 adjustment = glm::scale(glm::vec3(s, -s, s)) * glm::translate(glm::vec3(MeasureText(text) * -0.5f, -GetBearing(), 0));
	Transform transform(camera->GetTransposeProjectionViewMatrix(), world * adjustment);
	graphics->SetFontColor(color);
	graphics->Draw(geometry, vertexShader, pixelShader, transform, &characterMaterial);
}