#include "RenderTarget2D.hpp"
#include "../Application/Log.hpp"
#include "Graphics.hpp"
#include <FreeImage.h>

RenderTarget2D* RenderTarget2D::FromTexture(Graphics* graphics, ID3D11Texture2D* texture) {
	RenderTarget2D* target = new RenderTarget2D;

	target->texture = texture;
	target->depthTexture = nullptr;

	const D3D11_TEXTURE2D_DESC& desc = target->GetDesc();

	target->viewport.TopLeftX = 0.0f;
	target->viewport.TopLeftY = 0.0f;
	target->viewport.MinDepth = 0.0f;
	target->viewport.MaxDepth = 1.0f;
	target->viewport.Width = float(desc.Width);
	target->viewport.Height = float(desc.Height);

	HRESULT res = graphics->Device->CreateRenderTargetView(texture, nullptr, &target->renderTarget);
	if (FAILED(res)) {
		Log::Error("Failed to create back buffer render target");
	}

	return target;
}


RenderTarget2D* RenderTarget2D::Create(Graphics* graphics, int width, int height, DXGI_FORMAT format) {
	RenderTarget2D* target = new RenderTarget2D;

	D3D11_TEXTURE2D_DESC desc;
	desc.Width = width;
	desc.Height = height;
	desc.ArraySize = 1;
	desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.Format = format;
	desc.MipLevels = 0;
	desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;

	Texture2D* tex = new Texture2D;

	HRESULT res = graphics->Device->CreateTexture2D(&desc, nullptr, &target->texture);

	if (FAILED(res)) {
		Log::Error("Failed to create texture 2D");
	}

	target->CreateShaderResource(graphics);

	target->depthTexture = nullptr;

	target->viewport.TopLeftX = 0.0f;
	target->viewport.TopLeftY = 0.0f;
	target->viewport.MinDepth = 0.0f;
	target->viewport.MaxDepth = 1.0f;
	target->viewport.Width = float(width);
	target->viewport.Height = float(height);

	res = graphics->Device->CreateRenderTargetView(target->texture, nullptr, &target->renderTarget);
	if (FAILED(res)) {
		Log::Error("Failed to create render target 2D");
	}

	return target;
}


void RenderTarget2D::CreateDepthBuffer(Graphics* graphics, DXGI_FORMAT format) {
	const D3D11_TEXTURE2D_DESC& desc = GetDesc();
	depthTexture = Texture2D::Create(graphics, desc.Width, desc.Height, format, D3D11_BIND_DEPTH_STENCIL, D3D11_USAGE_DEFAULT);
}


void RenderTarget2D::Clear(Graphics* graphics, float r, float g, float b, float a) {
	float clear[] = { r, g, b, a };
	graphics->Context->ClearRenderTargetView(renderTarget, clear);
}

void RenderTarget2D::ClearDepth(Graphics* graphics, float depth) {
	if (HasDepth()) {
		graphics->Context->ClearDepthStencilView(depthTexture->DepthStencil, D3D11_CLEAR_DEPTH, depth, 0);
	}
}


void RenderTarget2D::Release() {
	renderTarget->Release();
	if (HasDepth()) {
		depthTexture->Release();
	}
	Texture2D::Release();
}


void RenderTarget2D::Apply(Graphics* graphics) {
	graphics->Context->RSSetViewports(1, &viewport);
	graphics->Context->OMSetRenderTargets(1, &renderTarget, HasDepth() ? depthTexture->DepthStencil : nullptr);
}


bool RenderTarget2D::HasDepth() {
	return depthTexture != nullptr;
}


void RenderTarget2D::SaveAsPNG(Graphics* graphics, std::string filename) {
	ID3D11Texture2D* stagingTexture;
	D3D11_TEXTURE2D_DESC desc = GetDesc();
	desc.BindFlags = 0;
	desc.Usage = D3D11_USAGE_STAGING;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	desc.SampleDesc = { 1, 0 };
	desc.MiscFlags = 0;

	FREE_IMAGE_TYPE fit;

	switch (desc.Format) {
	case DXGI_FORMAT_R8G8B8A8_UNORM:
		fit = FIT_BITMAP;
		break;
	default:
		Log::Error("Error trying to save render target as unknown format");
	}

	HRESULT res = graphics->Device->CreateTexture2D(&desc, nullptr, &stagingTexture);
	if (FAILED(res)) {
		Log::Error("Error creating staging texture to save render target to PNG");
	}

	graphics->Context->CopyResource(stagingTexture, texture);

	unsigned char* data = new unsigned char[desc.Width * desc.Height * 4];

	D3D11_MAPPED_SUBRESOURCE map;
	graphics->Context->Map(stagingTexture, 0, D3D11_MAP_READ, 0, &map);
	memcpy_s(data, desc.Width * desc.Height * 4, map.pData, desc.Width * desc.Height * 4);
	graphics->Context->Unmap(stagingTexture, 0);

	// swap the R <-> B channels - FreeImage has a bit of an issue with windows *sigh*
	for (unsigned int i = 0; i < desc.Width * desc.Height * 4; i += 4) {
		unsigned char red = data[i];
		data[i] = data[i + 2];
		data[i + 2] = red;
	}

	FIBITMAP* image = FreeImage_ConvertFromRawBitsEx(true, data, fit, desc.Width,
		desc.Height, map.RowPitch, 32, 0, 0, 0, 1);

	DeleteFile(filename.c_str());
	BOOL saved = FreeImage_Save(FIF_PNG, image, filename.c_str());

	FreeImage_Unload(image);
	delete[] data;
	stagingTexture->Release();
}


void RenderTarget2D::GenerateMips(Graphics* graphics) {
	graphics->Context->GenerateMips(ShaderResource);
}