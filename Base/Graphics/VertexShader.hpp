#pragma once
#include "../Includes.hpp"
#include "../Application/Blob.hpp"

class Graphics;

class VertexShader {
	ID3D11VertexShader* shader;
	ID3D11InputLayout* layout;

	static std::map<std::string, VertexShader*> cache;

	void CreateInputLayout(Graphics* graphics, Blob& bytecode);
	DXGI_FORMAT GetInputElementFormat(D3D_REGISTER_COMPONENT_TYPE reg, BYTE mask, const std::string& semantic);

	void Release();

public:

	static VertexShader* Create(Graphics* graphics, const std::string& compiledShaderFile);

	static void ReleaseCache();

	void Apply(Graphics* graphics) const;
};