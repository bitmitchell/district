#include "Camera.hpp"


Camera::Camera() : 
isViewDirty(true),
isProjectionDirty(true),
isProjectionViewDirty(true),
isOrtho(false),
up(glm::vec3(0, 1, 0)) {
	SetResolution(1280, 720);
	SetFOV((float)M_PI_4);
	SetClipPlanes(0.05f, 1000.0f);
}


Camera::Camera(glm::vec3& position, glm::vec3& target, glm::vec3& up) :
position(position), direction(glm::normalize(target - position)), up(up), isViewDirty(true), isProjectionDirty(true), isProjectionViewDirty(true), isTransposeProjectionViewDirty(true), isOrtho(false) {
	SetResolution(1280, 720);
	SetFOV((float)M_PI_4);
	SetClipPlanes(0.05f, 1000.0f);
}


void Camera::MarkViewDirty() const {
	isViewDirty = isProjectionViewDirty = isTransposeProjectionViewDirty = true;
}


void Camera::MarkProjectionDirty() const {
	isProjectionDirty = isProjectionViewDirty = isTransposeProjectionViewDirty = true;
}


void Camera::SetPosition(glm::vec3& pos) {
	position = pos;
	MarkViewDirty();
}


void Camera::SetTarget(glm::vec3& tar) {
	direction = glm::normalize(tar - position);
	MarkViewDirty();
}


void Camera::SetDirection(glm::vec3& dir) {
	direction = glm::normalize(dir);
	MarkViewDirty();
}


void Camera::SetUp(glm::vec3& u) {
	up = u;
	MarkViewDirty();
}


glm::vec3 Camera::GetPosition() const {
	return position;
}


glm::vec3 Camera::GetDirection() const {
	return glm::normalize(direction);
}


glm::vec3 Camera::GetUp() const {
	return up;
}


const glm::mat4& Camera::GetViewMatrix() const {
	if (isViewDirty) {
		isViewDirty = false;
		view = glm::lookAt(position, position + direction, up);
	}
	return view;
}


const glm::mat4& Camera::GetProjectionMatrix() const {
	if (isProjectionDirty) {
		isProjectionDirty = false;
		if (isOrtho) {
			projection = glm::ortho(-orthoX / 2, orthoX / 2, -orthoY / 2, orthoY / 2, -clipFar, clipFar);
		} else {
			projection = glm::perspectiveFov(fov, viewport.Width, viewport.Height, clipNear, clipFar);
		}
	}
	return projection;
}


const glm::mat4& Camera::GetProjectionViewMatrix() const {
	if (isProjectionViewDirty) {
		isProjectionViewDirty = false;
		projectionView = GetProjectionMatrix() * GetViewMatrix();
	}
	return projectionView;
}


const glm::mat4& Camera::GetTransposeProjectionViewMatrix() const {
	if (isTransposeProjectionViewDirty) {
		isTransposeProjectionViewDirty = false;
		transposeProjectionView = glm::transpose(GetProjectionViewMatrix());
	}
	return transposeProjectionView;
}


void Camera::SetResolution(int width, int height) {
	viewport.Width = (float)width;
	viewport.Height = (float)height;
	viewport.TopLeftX = viewport.TopLeftY = 0;
	viewport.MinDepth = 0;
	viewport.MaxDepth = 1;
	MarkProjectionDirty();
}


void Camera::SetFOV(float f) {
	fov = f;
	MarkProjectionDirty();
}


void Camera::SetClipPlanes(float nc, float fc) {
	clipNear = nc;
	clipFar = fc;
	MarkProjectionDirty();
}


void* Camera::operator new(size_t size){
	return _aligned_malloc(size, 16);
}


void Camera::operator delete(void* ptr) {
	_aligned_free(ptr);
}


glm::vec3 Camera::Unproject(glm::vec2 coord) {
	return glm::normalize(glm::unProject(glm::vec3(coord.x, viewport.Height - coord.y, 0.0f), glm::mat4(), GetProjectionViewMatrix(), glm::vec4(0, 0, viewport.Width, viewport.Height)) - position);
}


void Camera::MakeOrtho(float width, float height) {
	isOrtho = true;
	orthoX = width;
	orthoY = height;
	MarkProjectionDirty();
}