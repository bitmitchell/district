#include "RenderState.hpp"
#include "Graphics.hpp"
#include "../Application/Log.hpp"


ID3D11DepthStencilState* RenderState::depthState[3] = { nullptr, nullptr, nullptr };
ID3D11SamplerState* RenderState::samplerState[2] = { nullptr, nullptr };
ID3D11BlendState* RenderState::blendState[3] = { nullptr, nullptr, nullptr };


void RenderState::Initialize(Graphics* graphics) {
	CreateDepthStates(graphics);
	CreateSamplerStates(graphics);
	CreateBlendStates(graphics);
}


void RenderState::CreateDepthStates(Graphics* graphics) {

	D3D11_DEPTH_STENCIL_DESC depth;
	ZeroMemory(&depth, sizeof(depth));

	depth.StencilEnable = false;
	depth.DepthEnable = true;
	depth.DepthFunc = D3D11_COMPARISON_LESS;
	depth.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	HRESULT res = graphics->Device->CreateDepthStencilState(&depth, &depthState[DepthMode::ReadWrite]);
	if (FAILED(res)) {
		Log::Error("Failed to create depth read/write state");
	}

	depth.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	res = graphics->Device->CreateDepthStencilState(&depth, &depthState[DepthMode::Read]);
	if (FAILED(res)) {
		Log::Error("Failed to create depth read state");
	}

	depth.DepthFunc = D3D11_COMPARISON_ALWAYS;

	res = graphics->Device->CreateDepthStencilState(&depth, &depthState[DepthMode::None]);
	if (FAILED(res)) {
		Log::Error("Failed to create no depth state");
	}
}


void RenderState::CreateSamplerStates(Graphics* graphics) {
	D3D11_SAMPLER_DESC desc;
	desc.BorderColor[0] = desc.BorderColor[1] = desc.BorderColor[2] = desc.BorderColor[3] = 0.0f;
	desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	desc.Filter = D3D11_FILTER_ANISOTROPIC;
	desc.MaxAnisotropy = 16;
	desc.MaxLOD = std::numeric_limits<float>::max();
	desc.MinLOD = -std::numeric_limits<float>::max();
	desc.MipLODBias = 0.0f;

	desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

	HRESULT res = graphics->Device->CreateSamplerState(&desc, &samplerState[0]);
	if (FAILED(res)) {
		Log::Error("Failed to create clamp texture sampler");
	}

	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	res = graphics->Device->CreateSamplerState(&desc, &samplerState[1]);
	if (FAILED(res)) {
		Log::Error("Failed to create wrap texture sampler");
	}

	graphics->Context->PSSetSamplers(0, 2, samplerState);
}


void RenderState::CreateBlendStates(Graphics* graphics) {
	D3D11_BLEND_DESC desc;
	desc.AlphaToCoverageEnable = false;
	desc.IndependentBlendEnable = false;
	desc.RenderTarget[0].BlendEnable = true;
	desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

	desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;

	desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	HRESULT res = graphics->Device->CreateBlendState(&desc, &blendState[Alpha]);
	if (FAILED(res)) {
		Log::Error("Failed to create alpha blend state");
	}

	desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	desc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;

	desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;

	res = graphics->Device->CreateBlendState(&desc, &blendState[Additive]);
	if (FAILED(res)) {
		Log::Error("Failed to create alpha additive state");
	}

	desc.RenderTarget[0].BlendEnable = false;
	res = graphics->Device->CreateBlendState(&desc, &blendState[Disable]);
	if (FAILED(res)) {
		Log::Error("Failed to create alpha blend disabled state");
	}

	float factor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	graphics->Context->OMSetBlendState(blendState[Alpha], factor, 0xf);
}


void RenderState::Shutdown() {
	for (int i = 0; i < 3; ++i) {
		depthState[i]->Release();
		blendState[i]->Release();
	}

	for (int i = 0; i < 2; ++i) {
		samplerState[i]->Release();
	}
}


void RenderState::SetDepth(Graphics* graphics, DepthMode mode) {
	graphics->Context->OMSetDepthStencilState(depthState[mode], 0);
}


void RenderState::SetBlend(Graphics* graphics, BlendMode mode) {
	float blend[] = { 1, 1, 1, 1 };
	graphics->Context->OMSetBlendState(blendState[mode], blend, 0xf);
}