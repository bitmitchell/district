#include "ScreenSpace.hpp"
#include "Graphics.hpp"
#include "VertexShader.hpp"
#include "PixelShader.hpp"
#include "Vertex.hpp"
#include "Buffer.hpp"

Geometry *ScreenSpace::circle, *ScreenSpace::square;
glm::mat4 ScreenSpace::orthoProjection;
VertexShader* ScreenSpace::vertex = nullptr;
PixelShader* ScreenSpace::pixel = nullptr;

namespace {
	const int CircleResolution = 32;
}


void ScreenSpace::Initialize(Graphics* graphics) {
	Vertex squarePoints[] = {
		Vertex(glm::vec3(0, 0, 0), glm::vec3(), glm::vec2(0, 0)),
		Vertex(glm::vec3(1, 0, 0), glm::vec3(), glm::vec2(1, 0)),
		Vertex(glm::vec3(0, 1, 0), glm::vec3(), glm::vec2(0, 1)),

		Vertex(glm::vec3(1, 0, 0), glm::vec3(), glm::vec2(1, 0)),
		Vertex(glm::vec3(1, 1, 0), glm::vec3(), glm::vec2(1, 1)),
		Vertex(glm::vec3(0, 1, 0), glm::vec3(), glm::vec2(0, 1))
	};

	Buffer* squareBuffer = Buffer::Create(graphics, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, sizeof(squarePoints), squarePoints);
	square = Geometry::Create(squareBuffer, 6);

	uint16_t circleIndices[(CircleResolution - 1) * 3];
	Vertex circlePoints[CircleResolution + 1];
	for (int i = 0; i <= CircleResolution; ++i) {
		float angle = (float)i / (float)CircleResolution * 2.0f * (float)M_PI;
		float x = cos(angle);
		float y = sin(angle);
		circlePoints[i].Position = glm::vec3(x, y, 0);
		circlePoints[i].Normal = glm::vec3();
		circlePoints[i].Texture = glm::vec2(x, y);

		if (i > 1) {
			int index = (i - 2) * 3;
			circleIndices[index + 0] = 0;
			circleIndices[index + 1] = i - 2;
			circleIndices[index + 2] = i - 1;
		}
	}

	Buffer* circleBuffer = Buffer::Create(graphics, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, sizeof(circlePoints), circlePoints);
	Buffer* circleIndexBuffer = Buffer::Create(graphics, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0, sizeof(circleIndices), circleIndices);
	circle = Geometry::Create(circleBuffer, CircleResolution + 1, circleIndexBuffer, (CircleResolution - 1) * 3);
}


void ScreenSpace::Shutdown() {
	circle->Release();
	square->Release();
}


void ScreenSpace::SetShaders(VertexShader* vs, PixelShader* ps) {
	vertex = vs;
	pixel = ps;
}


void ScreenSpace::SetResolution(glm::vec2 resolution) {
	orthoProjection = glm::transpose(glm::ortho(0.0f, resolution.x, resolution.y, 0.0f));
}


void ScreenSpace::Circle(Graphics* graphics, glm::vec2 position, float radius, const glm::vec4& color) {
	graphics->SetFontColor(color);
	Transform transform(orthoProjection, glm::translate(glm::vec3(position, 0.0f)) * glm::scale(glm::vec3(radius)));
	graphics->Draw(circle, vertex, pixel, transform);
}


void ScreenSpace::Rectangle(Graphics* graphics, glm::vec2 position, glm::vec2 size, const glm::vec4& color) {
	graphics->SetFontColor(color);
	Transform transform(orthoProjection, glm::translate(glm::vec3(position, 0.0f)) * glm::scale(glm::vec3(size, 0.0f)));
	graphics->Draw(square, vertex, pixel, transform);
}