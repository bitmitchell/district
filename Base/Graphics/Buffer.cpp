#include "Buffer.hpp"
#include "../Application/Log.hpp"
#include "Graphics.hpp"


Buffer* Buffer::Create(Graphics* graphics, UINT bind, D3D11_USAGE usage, UINT readWrite, int size, void* data) {
	if (bind == D3D11_BIND_CONSTANT_BUFFER && size % 16 != 0) {
		Log::Warning("Constant buffer size should be a multiple of 16! Automatically padding out.");
		while (size % 16 != 0) {
			++size;
		}
	}

	Buffer* buffer = new Buffer;

	D3D11_BUFFER_DESC desc;
	desc.BindFlags = bind;
	desc.ByteWidth = size;
	desc.CPUAccessFlags = readWrite;
	desc.MiscFlags = 0;
	desc.StructureByteStride = 0;
	desc.Usage = usage;

	D3D11_SUBRESOURCE_DATA subresource;
	subresource.pSysMem = data;

	HRESULT res = graphics->Device->CreateBuffer(&desc, data ? &subresource : nullptr, &buffer->Data);

	if (FAILED(res)) {
		Log::Error("Failed to create buffer");
	}

	buffer->Size = size;

	return buffer;
}


void Buffer::Release() {
	Data->Release();
}


void Buffer::SetData(Graphics* graphics, int size, void* data) {
	if (size > Size) {
		Log::Error("Buffer not big enough");
	}
	D3D11_MAPPED_SUBRESOURCE map;
	graphics->Context->Map(Data, 0, D3D11_MAP_WRITE_DISCARD, 0, &map);
	memcpy_s(map.pData, Size, data, size);
	graphics->Context->Unmap(Data, 0);
}