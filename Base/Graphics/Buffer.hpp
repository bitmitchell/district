#pragma once
#include "../Includes.hpp"
#include "../Application/Log.hpp"

class Graphics;

class Buffer {
public:

	ID3D11Buffer* Data;
	int Size;

	static Buffer* Create(Graphics* graphics, UINT bind, D3D11_USAGE usage, UINT readWrite, int size, void* data = nullptr);

	void Release();

	void SetData(Graphics* graphics, int size, void* data);
};