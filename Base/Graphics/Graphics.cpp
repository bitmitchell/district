#define FONT_FACTORY
#include "Graphics.hpp"
#include "../Application/Log.hpp"
#include "RenderTarget2D.hpp"
#include "Geometry.hpp"
#include "PixelShader.hpp"
#include "VertexShader.hpp"
#include "Buffer.hpp"
#include "Camera.hpp"
#include "Font/FontFactory.hpp"
#include "RenderState.hpp"
#include "ScreenSpace.hpp"
#include "Material.hpp"
#include "../Application/Math.hpp"
#include "../Application/Performance.hpp"

Graphics* Graphics::Create(HWND window) {
	Graphics* g = new Graphics;

	HRESULT r;
	r = CreateDXGIFactory(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&g->Factory));

	if (FAILED(r)) {
		Log::Error("Failed to create DXGI Factory");
	}

	IDXGIAdapter* adaptor = nullptr;
	r = g->Factory->EnumAdapters(0, &adaptor);
	if (FAILED(r)) {
		Log::Error("Failed to enumerate graphics adaptor");
	}

	unsigned int debugFlag = 0;
#if defined(DEBUG)
	debugFlag = D3D11_CREATE_DEVICE_DEBUG;
#endif

	DXGI_SWAP_CHAIN_DESC swapDesc;
	swapDesc.BufferCount = 2;
	swapDesc.BufferDesc.Width = 0;
	swapDesc.BufferDesc.Height = 0;
	swapDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapDesc.BufferDesc.RefreshRate.Numerator = 0;
	swapDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapDesc.Flags = 0;
	swapDesc.OutputWindow = window;
	swapDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapDesc.SampleDesc.Count = 1;
	swapDesc.SampleDesc.Quality = 0;
	swapDesc.Windowed = true;

	D3D_FEATURE_LEVEL levels[] = {
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_0
	};

	r = D3D11CreateDeviceAndSwapChain(adaptor, D3D_DRIVER_TYPE_UNKNOWN, nullptr, debugFlag,
		levels, 2, D3D11_SDK_VERSION, &swapDesc, &g->SwapChain, &g->Device, nullptr, &g->Context);

	if (FAILED(r)) {
		Log::Error("Failed to create D3D11 device and swap chain");
	}

#if defined(DEBUG)
	r = g->Device->QueryInterface(__uuidof(ID3D11Debug), (void**)&g->Debug);
	if (FAILED(r)) {
		Log::Error("Failed to acquire the debugging interface");
	}
#endif

	adaptor->Release();

	g->frameTime = 0.0;

	g->CreateRenderTarget();
	g->CreateConstantBuffers();

	FontFactory::Initialize();
	RenderState::Initialize(g);
	ScreenSpace::Initialize(g);

	g->Context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return g;
}



Graphics* Graphics::Create() {
	Graphics* g = new Graphics;

	HRESULT r;
	r = CreateDXGIFactory(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&g->Factory));

	if (FAILED(r)) {
		Log::Error("Failed to create DXGI Factory");
	}

	IDXGIAdapter* adaptor = nullptr;
	r = g->Factory->EnumAdapters(0, &adaptor);
	if (FAILED(r)) {
		Log::Error("Failed to enumerate graphics adaptor");
	}

	unsigned int debugFlag = 0;
#if defined(DEBUG)
	debugFlag = D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_FEATURE_LEVEL levels = {
		D3D_FEATURE_LEVEL_11_0
	};

	r = D3D11CreateDevice(adaptor, D3D_DRIVER_TYPE_UNKNOWN, nullptr, debugFlag,
		&levels, 1, D3D11_SDK_VERSION, &g->Device, nullptr, &g->Context);

	g->SwapChain = nullptr;

	if (FAILED(r)) {
		Log::Error("Failed to create D3D11 device");
	}

#if defined(DEBUG)
	r = g->Device->QueryInterface(__uuidof(ID3D11Debug), (void**)&g->Debug);
	if (FAILED(r)) {
		Log::Error("Failed to acquire the debugging interface");
	}
#endif

	adaptor->Release();

	g->RenderTarget = nullptr;
	g->CreateConstantBuffers();

	FontFactory::Initialize();
	RenderState::Initialize(g);

	g->Context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return g;
}


void Graphics::CreateRenderTarget() {
	HRESULT res;

	ID3D11Texture2D* texture;
	res = SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&texture));

	if (FAILED(res)) {
		Log::Error("Failed to acquire back buffer");
	}

	RenderTarget = RenderTarget2D::FromTexture(this, texture);
	//RenderTarget->CreateDepthBuffer(this);
	const D3D11_TEXTURE2D_DESC& desc = RenderTarget->GetDesc();

	Settings.Width = desc.Width;
	Settings.Height = desc.Height;
}


void Graphics::CreateConstantBuffers() {

	constantPerDraw = Buffer::Create(this, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, sizeof(ConstantPerDrawLayout));

	Context->VSSetConstantBuffers(0, 1, &constantPerDraw->Data);
	Context->PSSetConstantBuffers(0, 1, &constantPerDraw->Data);

	cornerPerCamera = Buffer::Create(this, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, sizeof(CornerPerCameraLayout));
	
	Context->VSSetConstantBuffers(1, 1, &cornerPerCamera->Data);
	Context->PSSetConstantBuffers(1, 1, &cornerPerCamera->Data);

	constantPerFrame = Buffer::Create(this, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, sizeof(ConstantPerFrameLayout));

	Context->VSSetConstantBuffers(2, 1, &constantPerFrame->Data);
	Context->PSSetConstantBuffers(2, 1, &constantPerFrame->Data);

	constantPerLight = Buffer::Create(this, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, sizeof(ConstantPerLightLayout));

	Context->VSSetConstantBuffers(3, 1, &constantPerLight->Data);
	Context->PSSetConstantBuffers(3, 1, &constantPerLight->Data);

	constantPerFont = Buffer::Create(this, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, sizeof(ConstantPerFontLayout));

	Context->VSSetConstantBuffers(4, 1, &constantPerFont->Data);
	Context->PSSetConstantBuffers(4, 1, &constantPerFont->Data);

}


void Graphics::Shutdown() {
	FontFactory::Shutdown();
	VertexShader::ReleaseCache();
	PixelShader::ReleaseCache();
	Texture2D::ReleaseCache();
	Geometry::ReleaseCache();
	RenderState::Shutdown();
	ScreenSpace::Shutdown();

	frameTimers.clear();
	constantPerDraw->Release();
	constantPerFrame->Release();
	constantPerFont->Release();
	cornerPerCamera->Release();
	constantPerLight->Release();

	GRAPHICS_RELEASE(RenderTarget);

	GRAPHICS_RELEASE(SwapChain);
	GRAPHICS_RELEASE(Factory);
	GRAPHICS_RELEASE(Device);
	GRAPHICS_RELEASE(Context);
#if defined(DEBUG)
	Debug->ReportLiveDeviceObjects(D3D11_RLDO_SUMMARY | D3D11_RLDO_DETAIL);
	GRAPHICS_RELEASE(Debug);
#endif
}


void Graphics::Present(bool vsync) {
	if (SwapChain) {
		SwapChain->Present(vsync ? 1 : 0, 0);
	}
}


void Graphics::Draw(const Geometry* geometry, const VertexShader* vertex, const PixelShader* pixel, const Transform& transform, const Material* material) {
	vertex->Apply(this);
	pixel->Apply(this);
	if (geometry) {
		geometry->Apply(this);
	}
	if (material) {
		material->SetAsResource(this, 0);
	}

	SetConstantPerDrawBuffer(transform.ProjectionView, transform.World);

	if (geometry->IsIndexed()) {
		Context->DrawIndexed(geometry->GetIndexCount(), 0, 0);
	} else {
		Context->Draw(geometry->GetVertexCount(), 0);
	}
}


void Graphics::Draw(uint32_t vertexCount, const VertexShader* vertex, const PixelShader* pixel, const Transform& transform, const Material* material) {
	vertex->Apply(this);
	pixel->Apply(this);
	if (material) {
		material->SetAsResource(this, 0);
	}
	SetConstantPerDrawBuffer(transform.ProjectionView, transform.World);

	Context->IASetInputLayout(nullptr);
	Context->Draw(vertexCount, 0);
}


void Graphics::SetConstantPerDrawBuffer(const glm::mat4& projView, const glm::mat4& world) {
	ConstantPerDrawLayout layout = { 
		projView,
		glm::transpose(world)
	};
	constantPerDraw->SetData(this, sizeof(layout), &layout);
}


void Graphics::SetFontColor(const glm::vec4& color) {
	ConstantPerFontLayout layout = {color};
	constantPerFont->SetData(this, sizeof(layout), &layout);
}


void Graphics::SetView(Camera* camera) {
	CornerPerCameraLayout layout;
	layout.Corner[0] = glm::vec4(camera->Unproject(glm::vec2(0, 0)), 0);
	layout.Corner[1] = glm::vec4(camera->Unproject(glm::vec2(float(Settings.Width), 0)), 0);
	layout.Corner[2] = glm::vec4(camera->Unproject(glm::vec2(0, float(Settings.Height))), 0);
	layout.Corner[3] = glm::vec4(camera->Unproject(glm::vec2(float(Settings.Width), float(Settings.Height))), 0);

	layout.Position = glm::vec4(camera->GetPosition(), 0);
	layout.Direction = glm::vec4(camera->GetDirection(), 0);

	cornerPerCamera->SetData(this, sizeof(layout), &layout);
}


void Graphics::SetSceneConstants(float timeOfDay) {
	ConstantPerFrameLayout layout = {
		Wrap(timeOfDay, 0.0f, 1.0f)
	};

	constantPerFrame->SetData(this, sizeof(layout), &layout);
}


void Graphics::SetLight(const glm::mat4& matrix, const glm::vec3& position, const glm::vec3& direction, float strength, const glm::vec3& color, float distance) {
	ConstantPerLightLayout layout = {
		matrix,
		glm::vec4(position, 0),
		direction,
		strength,
		color,
		distance
	};

	constantPerLight->SetData(this, sizeof(layout), &layout);
}


void Graphics::StartTiming() {
	PerfFunction;
	if (frameTimers.size() > 0 && frameTimers.front().Ready(Context)) {
		double time = frameTimers.front().Result(Context);
		if (!std::isnan(time)) {
			frameTime = Lerp(frameTime, time, 0.2);
		}
		frameTimers.pop_front();
	}

	frameTimers.emplace_back(Device, Context);
}


void Graphics::EndTiming() {
	frameTimers.back().End(Context);
}


double Graphics::FrameTime() {
	return frameTime;
}