#pragma once
#include "../Includes.hpp"
#include "../Application/DataFormat.hpp"

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 Texture;

	Vertex() {
	}

	Vertex(glm::vec3 pos, glm::vec3 nor, glm::vec2 tex) : Position(pos), Normal(nor), Texture(tex) {
	}
};


struct Triangle {
	uint16_t A, B, C;

	Triangle() : A(0), B(0), C(0) {
	}
	
	Triangle(uint16_t a, uint16_t b, uint16_t c) : A(a), B(b), C(c) {
	}
};


template<>
struct ConvertDDF<Vertex> {
	Vertex operator()(Object& obj) {
		Vertex vert;
		vert.Position = obj["pos"].As<glm::vec3>();
		vert.Normal = obj["nor"].As<glm::vec3>();
		vert.Texture = obj["uv"].As<glm::vec2>();
		return vert;
	}
};


template<>
struct ConvertDDF<Triangle> {
	Triangle operator()(Object& obj) {
		Triangle tri;
		tri.A = obj[0].As<int>();
		tri.B = obj[1].As<int>();
		tri.C = obj[2].As<int>();
		return tri;
	}
};