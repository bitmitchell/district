#pragma once

#include "../Includes.hpp"

class Camera {

	glm::vec3 position, direction, up;

	float clipNear, clipFar, fov;
	float orthoX, orthoY;
	bool isOrtho;

	D3D11_VIEWPORT viewport;

	mutable glm::mat4 view, projection, projectionView, transposeProjectionView;
	mutable bool isViewDirty, isProjectionDirty, isProjectionViewDirty, isTransposeProjectionViewDirty;

	void MarkViewDirty() const;
	void MarkProjectionDirty() const;

public:

	Camera();
	Camera(glm::vec3& position, glm::vec3& target, glm::vec3& up);

	static void* operator new(size_t size);
	static void operator delete(void* ptr);

	void SetPosition(glm::vec3& position);
	void SetTarget(glm::vec3& target);
	void SetDirection(glm::vec3& direction);
	void SetUp(glm::vec3& up);

	glm::vec3 GetPosition() const;
	glm::vec3 GetDirection() const;
	glm::vec3 GetUp() const;

	const glm::mat4& GetViewMatrix() const;
	const glm::mat4& GetProjectionMatrix() const;
	const glm::mat4& GetProjectionViewMatrix() const;
	const glm::mat4& GetTransposeProjectionViewMatrix() const;

	float GetFarClip() const { return clipFar; }
	float GetNearClip() const { return clipNear; }
	float GetFOV() const { return fov; }
	float GetOrthoSize() const { return std::min(orthoX, orthoY); }

	void SetResolution(int width, int height);
	void SetFOV(float fov);
	void SetClipPlanes(float nearClip, float farClip);
	void MakeOrtho(float width, float height);

	glm::vec3 Unproject(glm::vec2 coord);
	

};