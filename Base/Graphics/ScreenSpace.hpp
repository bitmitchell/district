#pragma once
#include "../Includes.hpp"
#include "Geometry.hpp"

class Graphics;
class VertexShader;
class PixelShader;


class ScreenSpace {

	static Geometry *circle, *square;
	static glm::mat4 orthoProjection;
	static VertexShader* vertex;
	static PixelShader* pixel;

public:

	static void Initialize(Graphics* graphics);
	static void Shutdown();

	static void SetShaders(VertexShader* vertex, PixelShader* pixel);
	static void SetResolution(glm::vec2 resolution);

	static void Circle(Graphics* graphics, glm::vec2 position, float radius, const glm::vec4& color);
	static void Rectangle(Graphics* graphics, glm::vec2 position, glm::vec2 size, const glm::vec4& color);

};