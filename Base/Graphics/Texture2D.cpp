#include "Texture2D.hpp"
#include "Graphics.hpp"

#include "../Application/Log.hpp"
#include "../Application/File.hpp"

#include <FreeImage.h>


std::map<std::string, Texture2D*> Texture2D::cache;


Texture2D* Texture2D::Create(Graphics* graphics, int width, int height, DXGI_FORMAT format, UINT bind, D3D11_USAGE usage) {
	D3D11_TEXTURE2D_DESC desc;
	desc.Width = width;
	desc.Height = height;
	desc.ArraySize = 1;
	desc.BindFlags = bind;
	desc.CPUAccessFlags = 0;
	desc.Format = format;
	desc.MipLevels = 1;
	desc.MiscFlags = 0;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = usage;

	Texture2D* tex = new Texture2D;

	HRESULT res = graphics->Device->CreateTexture2D(&desc, nullptr, &tex->texture);

	if (FAILED(res)) {
		Log::Error("Failed to create texture 2D");
	}

	if (bind & D3D11_BIND_SHADER_RESOURCE) {
		tex->CreateShaderResource(graphics);
	} else if (bind & D3D11_BIND_DEPTH_STENCIL) {
		tex->CreateDepthStencil(graphics);
	}

	return tex;
}


Texture2D* Texture2D::Create(Graphics* graphics, int width, int height, void* data, int dataPitch, DXGI_FORMAT format, UINT bind, D3D11_USAGE usage) {
	D3D11_TEXTURE2D_DESC desc;
	desc.Width = width;
	desc.Height = height;
	desc.ArraySize = 1;
	desc.BindFlags = bind;
	desc.CPUAccessFlags = 0;
	desc.Format = format;
	desc.MipLevels = 1;
	desc.MiscFlags = 0;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = usage;

	Texture2D* tex = new Texture2D;
	tex->ShaderResource = nullptr;

	D3D11_SUBRESOURCE_DATA subresource;
	subresource.pSysMem = data;
	subresource.SysMemPitch = dataPitch;
	subresource.SysMemSlicePitch = dataPitch * height;

	HRESULT res = graphics->Device->CreateTexture2D(&desc, &subresource, &tex->texture);

	if (FAILED(res)) {
		Log::Error("Failed to create texture 2D");
	}

	if (bind & D3D11_BIND_SHADER_RESOURCE) {
		tex->CreateShaderResource(graphics);
	}

	return tex;
}


Texture2D* Texture2D::FromFile(Graphics* graphics, const std::string& filename) {
	if (cache.count(filename)) {
		return cache[filename];
	}

	if (!File::Exists(filename)) {
		Log::Error("Error loading texture, file doesn't exist");
	}

	FREE_IMAGE_FORMAT format = FIF_UNKNOWN;
	std::string ext = File::GetExtension(filename);

	if (ext == ".bmp") {
		format = FIF_BMP;
	} else if (ext == ".png") {
		format = FIF_PNG;
	}

	if (format == FIF_UNKNOWN) {
		Log::Error("Unsupported image format '%%%' from '%%%'", ext, filename);
	}

	FIBITMAP* image = FreeImage_Load(format, filename.c_str());
	//FreeImage_FlipVertical(image);
	FIBITMAP* converted = FreeImage_ConvertTo32Bits(image);
	BYTE* data = reinterpret_cast<BYTE*>(FreeImage_GetBits(converted));

	Texture2D* tex = Create(graphics, FreeImage_GetWidth(converted), FreeImage_GetHeight(converted), data, FreeImage_GetPitch(converted), DXGI_FORMAT_B8G8R8A8_UNORM);

	FreeImage_Unload(image);
	FreeImage_Unload(converted);

	cache[filename] = tex;
	Log::Info("Loaded texture '%%%'", filename);

	return tex;
}


void Texture2D::CreateShaderResource(Graphics* graphics) {
	HRESULT res = graphics->Device->CreateShaderResourceView(texture, nullptr, &ShaderResource);
	if (FAILED(res)) {
		Log::Error("Failed to create shader resource view");
	}
}


void Texture2D::CreateDepthStencil(Graphics* graphics) {
	HRESULT res = graphics->Device->CreateDepthStencilView(texture, nullptr, &DepthStencil);
	if (FAILED(res)) {
		Log::Error("Failed to create depth stencil view");
	}
}


const D3D11_TEXTURE2D_DESC& Texture2D::GetDesc() {
	if (!hasDesc) {
		hasDesc = true;
		texture->GetDesc(&desc);
	}
	return desc;
}


void Texture2D::Release() {
	texture->Release();
	if (ShaderResource) {
		ShaderResource->Release();
	}
	if (DepthStencil) {
		DepthStencil->Release();
	}
}


void Texture2D::ReleaseCache() {
	for (auto& tex : cache) {
		tex.second->Release();
		delete tex.second;
	}
	cache.clear();
}


void Texture2D::SetAsResource(Graphics* graphics, int index) const {
	if (ShaderResource) {
		graphics->Context->PSSetShaderResources(index, 1, &ShaderResource);
	}
}