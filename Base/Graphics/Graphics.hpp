#pragma once
#include "../Includes.hpp"
#include "Transform.hpp"

class Texture2D;
class RenderTarget2D;
class PixelShader;
class VertexShader;
class Geometry;
class Buffer;
struct Material;

class Graphics {

	struct TimingBlock {
		ID3D11Query *disjoint, *start, *end;

		TimingBlock(ID3D11Device* device, ID3D11DeviceContext* context) {
			D3D11_QUERY_DESC desc;

			desc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
			desc.MiscFlags = 0;
			HRESULT res = device->CreateQuery(&desc, &disjoint);

			desc.Query = D3D11_QUERY_TIMESTAMP;
			res = device->CreateQuery(&desc, &start);
			res = device->CreateQuery(&desc, &end);

			context->Begin(disjoint);
			context->End(start);
		}

		~TimingBlock() {
			GRAPHICS_RELEASE(disjoint);
			GRAPHICS_RELEASE(start);
			GRAPHICS_RELEASE(end);
		}

		void End(ID3D11DeviceContext* context) {
			context->End(end);
			context->End(disjoint);
		}

		bool Ready(ID3D11DeviceContext* context) {
			return context->GetData(disjoint, nullptr, 0, 0) == S_OK &&
				context->GetData(start, nullptr, 0, 0) == S_OK &&
				context->GetData(end, nullptr, 0, 0) == S_OK;
		}

		double Result(ID3D11DeviceContext* context) {
			UINT64 startTime, endTime;
			D3D11_QUERY_DATA_TIMESTAMP_DISJOINT disjointTimestamp;
			context->GetData(disjoint, &disjointTimestamp, sizeof(disjointTimestamp), 0);
			context->GetData(start, &startTime, sizeof(startTime), 0);
			context->GetData(end, &endTime, sizeof(endTime), 0);

			if (disjointTimestamp.Disjoint) {
				return std::numeric_limits<double>::quiet_NaN();
			} else {
				UINT64 delta = endTime - startTime;
				return double(delta) / double(disjointTimestamp.Frequency);
			}
		}
	};
	std::deque<TimingBlock> frameTimers;
	double frameTime;

	Buffer* constantPerDraw;
	struct ConstantPerDrawLayout {
		glm::mat4 ProjectionView, World;
	};

	Buffer* cornerPerCamera;
	struct CornerPerCameraLayout {
		glm::vec4 Corner[4];
		glm::vec4 Position;
		glm::vec4 Direction;
	};
	
	Buffer *constantPerFont;
	struct ConstantPerFontLayout {
		glm::vec4 Color;
	};

	Buffer* constantPerFrame;
	struct ConstantPerFrameLayout {
		float TimeOfDay;
		float Padding[3];
	};

	Buffer* constantPerLight;
	struct ConstantPerLightLayout {
		glm::mat4 ProjectionView;
		glm::vec4 LightPosition;
		glm::vec3 LightForward;
		float LightStrength;
		glm::vec3 LightColor;
		float LightDistance;
	};

	void CreateRenderTarget();

	void SetConstantPerDrawBuffer(const glm::mat4& projectionView, const glm::mat4& world);

	void CreateConstantBuffers();

public:

	struct Settings {
		int Width, Height;
	} Settings;

	IDXGISwapChain* SwapChain;
	IDXGIFactory* Factory;
	ID3D11Device* Device;
	ID3D11DeviceContext* Context;

#if defined(DEBUG)
	ID3D11Debug* Debug;
#endif

	RenderTarget2D* RenderTarget;

	static Graphics* Create(HWND windowHandle);
	static Graphics* Create();
	
	void Shutdown();

	void StartTiming();
	void EndTiming();

	double FrameTime();

	void Present(bool vsync = false);

	void SetView(Camera* camera);
	void SetSceneConstants(float timeOfDay);
	void SetLight(const glm::mat4& projection, const glm::vec3& position, const glm::vec3& forward, float strength, const glm::vec3& color, float distance);
	void SetFontColor(const glm::vec4& color);

	void Draw(const Geometry* geometry, const VertexShader* vertex, const PixelShader* pixel, const Transform& transform, const Material* material = nullptr);
	void Draw(uint32_t vertices, const VertexShader* vertex, const PixelShader* pixel, const Transform& transform, const Material* material = nullptr);	

};