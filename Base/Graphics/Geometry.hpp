#pragma once
#include "../Includes.hpp"

class Buffer;
class Graphics;

class Geometry {
	Buffer *vertex, *index;
	unsigned int vertexCount, indexCount;

	static std::unordered_map<std::string, Geometry*> cache;

public:
	Geometry();

	static Geometry* Create(Buffer* vertex, unsigned int vertexCount);
	static Geometry* Create(Buffer* vertex, unsigned int vertexCount, Buffer* index, unsigned int indexCount);

	static Geometry* CreateFromBDM(Graphics* graphics, std::string file);

	static void ReleaseCache();

	void Release();
	void Apply(Graphics* graphics) const;

	unsigned int GetVertexCount() const;
	unsigned int GetIndexCount() const;

	void SetVertexCount(unsigned int count);

	bool IsIndexed() const;
};