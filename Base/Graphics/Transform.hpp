#pragma once
#include "../Includes.hpp"

class Camera;

struct Transform {
	glm::mat4 World, ProjectionView;

	Transform(	const glm::mat4& projView = glm::mat4(), 
				const glm::mat4& world = glm::mat4()) : ProjectionView(projView), World(world) {
	}

	Transform(	const glm::mat4& projView, 
				const glm::vec3& position,
				const glm::quat& rotation) : 
				ProjectionView(projView), 
				World(glm::translate(glm::mat4(), position) * glm::mat4_cast(rotation)) {
	}

	void Scale(glm::vec3 scale) {
		World = glm::scale(World, scale);
	}
};