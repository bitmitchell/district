#include "PixelShader.hpp"
#include "Graphics.hpp"
#include "../Application/Log.hpp"
#include "../Application/File.hpp"


std::map<std::string, PixelShader*> PixelShader::cache;


PixelShader* PixelShader::Create(Graphics* graphics, const std::string& file) {
	if (cache.count(file)) {
		return cache[file];
	}

	PixelShader* shader = new PixelShader;
	Blob bytecode = File::ReadBinary(file);

	HRESULT res = graphics->Device->CreatePixelShader(bytecode.Data, bytecode.Size, nullptr, &shader->shader);

	if (FAILED(res)) {
		Log::Error("Error creating pixel shader %%%", file);
	}

	bytecode.Release();

	Log::Info("Loaded pixel shader %%%", file);

	cache[file] = shader;

	return shader;
}


void PixelShader::ReleaseCache() {
	for (auto& shader : cache) {
		shader.second->Release();
		delete shader.second;
	}
	cache.clear();
}


void PixelShader::Release() {
	shader->Release();
}


void PixelShader::Apply(Graphics* graphics) const {
	graphics->Context->PSSetShader(shader, nullptr, 0);
}