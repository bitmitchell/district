#pragma once

#include "../Includes.hpp"

class Graphics;

class RenderState {

	static ID3D11DepthStencilState* depthState[3];
	static ID3D11SamplerState* samplerState[2];
	static ID3D11BlendState* blendState[3];

	static void CreateDepthStates(Graphics* graphics);
	static void CreateSamplerStates(Graphics* graphics);
	static void CreateBlendStates(Graphics* graphics);

public:

	enum DepthMode {
		None,
		Read,
		ReadWrite
	};

	enum BlendMode {
		Alpha,
		Additive,
		Disable
	};

	static void Initialize(Graphics* graphics);
	static void Shutdown();

	static void SetDepth(Graphics* graphics, DepthMode mode);
	static void SetBlend(Graphics* graphics, BlendMode mode);

};