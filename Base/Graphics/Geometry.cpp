#include "Geometry.hpp"
#include "Graphics.hpp"
#include "Buffer.hpp"
#include "Vertex.hpp"
#include "../Application/File.hpp"
#include "../Application/Binary.hpp"
#include "../Application/Timer.hpp"


std::unordered_map<std::string, Geometry*> Geometry::cache;


void Geometry::ReleaseCache() {
	for (auto& pair : cache) {
		pair.second->Release();
		delete pair.second;
	}
	cache.clear();
}


Geometry::Geometry() : vertex(nullptr), index(nullptr), vertexCount(0), indexCount(0) {
}


Geometry* Geometry::Create(Buffer* vertex, unsigned int vertexCount) {
	Geometry* geometry = new Geometry;
	geometry->vertex = vertex;
	geometry->vertexCount = vertexCount;
	return geometry;
}


Geometry* Geometry::Create(Buffer* vertex, unsigned int vertexCount, Buffer* index, unsigned int indexCount) {
	Geometry* geometry = new Geometry;
	geometry->vertex = vertex;
	geometry->vertexCount = vertexCount;
	geometry->index = index;
	geometry->indexCount = indexCount;
	return geometry;
}


Geometry* Geometry::CreateFromBDM(Graphics* graphics, std::string filename) {
	if (cache.count(filename)) {
		return cache[filename];
	}

	Timer time;
	Blob binary = File::ReadBinary(filename);
	BinaryReader reader(&binary);

	std::vector<Vertex> verts;
	std::vector<Triangle> tris;

	uint8_t version;
	uint16_t vertexCount, triangleCount;

	reader >> version >> vertexCount >> triangleCount;

	if (version != 0) {
		Log::Error("BDM mesh has invalid version number %%%, '%%%'", version, filename);
	}

	for (int i = 0; i < vertexCount; ++i) {
		Vertex v;
		reader >> v;
		verts.push_back(v);
	}

	for (int i = 0; i < triangleCount; ++i) {
		Triangle t;
		reader >> t;
		tris.push_back(t);
	}


	Buffer* vertexBuffer = Buffer::Create(graphics, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0,
		sizeof(Vertex)* verts.size(), &verts[0]);
	Buffer* indexBuffer = Buffer::Create(graphics, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0,
		sizeof(Triangle)* tris.size(), &tris[0]);

	binary.Release();

	Log::Info("Loaded mesh %%% in %%%s", filename, time.Time());

	Geometry* geom = Geometry::Create(vertexBuffer, verts.size(), indexBuffer, tris.size() * 3);
	cache.emplace(filename, geom);

	return geom;
}


void Geometry::Release() {
	vertex->Release();
	delete vertex;
	if (index) {
		index->Release();
		delete index;
	}

	vertexCount = indexCount = 0;
}


void Geometry::Apply(Graphics* graphics) const {
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	graphics->Context->IASetVertexBuffers(0, 1, &vertex->Data, &stride, &offset);

	if (index) {
		graphics->Context->IASetIndexBuffer(index->Data, DXGI_FORMAT_R16_UINT, 0);
	}
}


unsigned int Geometry::GetVertexCount() const {
	return vertexCount;
}


unsigned int Geometry::GetIndexCount() const {
	return indexCount;
}


void Geometry::SetVertexCount(unsigned int count) {
	vertexCount = count;
}


bool Geometry::IsIndexed() const {
	return index != nullptr;
}