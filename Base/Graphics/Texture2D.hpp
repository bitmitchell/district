#pragma once
#include "../Includes.hpp"
class Graphics;

class Texture2D {
	static std::map<std::string, Texture2D*> cache;

protected:
	ID3D11Texture2D* texture;

	D3D11_TEXTURE2D_DESC desc;
	bool hasDesc;

	void CreateShaderResource(Graphics* graphics);
	void CreateDepthStencil(Graphics* graphics);

public:

	ID3D11ShaderResourceView* ShaderResource;
	ID3D11DepthStencilView* DepthStencil;

	static Texture2D* FromTexture(ID3D11Texture2D* texture);
	static Texture2D* Create(Graphics* graphics, int width, int height, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM, UINT bind = D3D11_BIND_SHADER_RESOURCE, D3D11_USAGE usage = D3D11_USAGE_DEFAULT);
	static Texture2D* Create(Graphics* graphics, int width, int height, void* data, int dataPitch, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM, UINT bind = D3D11_BIND_SHADER_RESOURCE, D3D11_USAGE usage = D3D11_USAGE_DEFAULT);
	static Texture2D* FromFile(Graphics* graphics, const std::string& filename);

	static void ReleaseCache();

	Texture2D() : texture(nullptr), ShaderResource(nullptr), DepthStencil(nullptr) {}

	const D3D11_TEXTURE2D_DESC& GetDesc();

	virtual void Release();

	void SetAsResource(Graphics* graphics, int index) const;

};