#pragma once
#include "../Includes.hpp"
#include "Texture2D.hpp"

class Graphics;

class RenderTarget2D : public Texture2D {
	ID3D11RenderTargetView* renderTarget;

	Texture2D* depthTexture;

	D3D11_VIEWPORT viewport;

public:

	static RenderTarget2D* FromTexture(Graphics* graphics, ID3D11Texture2D* texture);
	static RenderTarget2D* Create(Graphics* graphics, int width, int height, DXGI_FORMAT format);

	RenderTarget2D() : renderTarget(nullptr), depthTexture(nullptr) {}

	void CreateDepthBuffer(Graphics* graphics, DXGI_FORMAT format = DXGI_FORMAT_D24_UNORM_S8_UINT);

	void Clear(Graphics* graphics, float r, float g, float b, float a);
	void ClearDepth(Graphics* graphics, float depth);
	void Apply(Graphics* graphics);

	void GenerateMips(Graphics* graphics);

	bool HasDepth();

	void SaveAsPNG(Graphics* graphics, std::string filename);

	ID3D11RenderTargetView* GetView() const { return renderTarget; }
	ID3D11DepthStencilView* GetDepthView() const {
		return (depthTexture ? depthTexture->DepthStencil : nullptr);
	}
	D3D11_VIEWPORT* GetViewport() { return &viewport; }

	virtual void Release();
};