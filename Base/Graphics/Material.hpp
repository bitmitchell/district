#pragma once

#include "Texture2D.hpp"

class Graphics;

struct Material {
	Texture2D *Color, *Properties;
	Material() : Color(nullptr), Properties(nullptr) {}
	Material(Texture2D* color, Texture2D* properties) : Color(color), Properties(properties) {}

	void SetAsResource(Graphics* graphics, int slot) const;
};