#include "Material.hpp"
#include "Graphics.hpp"
#include "Texture2D.hpp"


void Material::SetAsResource(Graphics* graphics, int slot) const {
	ID3D11ShaderResourceView* views[2] = {
		(Color ? Color->ShaderResource : nullptr),
		(Properties ? Properties->ShaderResource : nullptr)
	};
	graphics->Context->PSSetShaderResources(slot, 2, views);
}