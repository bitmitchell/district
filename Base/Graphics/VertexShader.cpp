#include "VertexShader.hpp"
#include "Graphics.hpp"
#include "../Application/Log.hpp"
#include "../Application/File.hpp"

std::map<std::string, VertexShader*> VertexShader::cache;


VertexShader* VertexShader::Create(Graphics* graphics, const std::string& file) {
	if (cache.count(file)) {
		return cache[file];
	}

	VertexShader* shader = new VertexShader;

	Blob bytecode = File::ReadBinary(file);

	HRESULT res = graphics->Device->CreateVertexShader(bytecode.Data, bytecode.Size, nullptr, &shader->shader);

	if (FAILED(res)) {
		Log::Error("Error creating vertex shader");
	}

	shader->CreateInputLayout(graphics, bytecode);

	bytecode.Release();

	Log::Info("Loaded vertex shader %%%", file);

	cache[file] = shader;

	return shader;
}


void VertexShader::ReleaseCache() {
	for (auto& shader : cache) {
		shader.second->Release();
		delete shader.second;
	}
	cache.clear();
}


void VertexShader::CreateInputLayout(Graphics* graphics, Blob& bytecode) {
	ID3D11ShaderReflection* reflection;
	D3DReflect(bytecode.Data, bytecode.Size, IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&reflection));
	D3D11_SHADER_DESC desc;
	reflection->GetDesc(&desc);

	D3D11_INPUT_ELEMENT_DESC* elements = new D3D11_INPUT_ELEMENT_DESC[desc.InputParameters];

	for (uint32_t i = 0; i < desc.InputParameters; ++i) {
		D3D11_SIGNATURE_PARAMETER_DESC input;
		reflection->GetInputParameterDesc(i, &input);

		elements[i].SemanticName = input.SemanticName;
		elements[i].SemanticIndex = input.SemanticIndex;
		elements[i].InputSlot = 0;
		elements[i].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		elements[i].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
		elements[i].InstanceDataStepRate = 0;
		elements[i].Format = GetInputElementFormat(input.ComponentType, input.Mask, input.SemanticName);
	}

	HRESULT res = graphics->Device->CreateInputLayout(elements, desc.InputParameters, bytecode.Data, bytecode.Size, &layout);

	if (FAILED(res)) {
		Log::Error("Error creating input layout");
	}

	reflection->Release();
}


DXGI_FORMAT VertexShader::GetInputElementFormat(D3D_REGISTER_COMPONENT_TYPE reg, BYTE mask, const std::string& semantic) {
	switch (reg) {
	case D3D_REGISTER_COMPONENT_FLOAT32:
		switch (mask) {
		case 0x1:
			return DXGI_FORMAT_R32_FLOAT;
		case 0x3:
			return DXGI_FORMAT_R32G32_FLOAT;
		case 0x7:
			return DXGI_FORMAT_R32G32B32_FLOAT;
		case 0xf:
			if (semantic == "POSITION") {
				return DXGI_FORMAT_R32G32B32_FLOAT;
			} else {
				return DXGI_FORMAT_R32G32B32A32_FLOAT;
			}
		}
		break;
	case D3D_REGISTER_COMPONENT_UINT32:
		switch (mask) {
		case 0x1:
			return DXGI_FORMAT_R32_UINT;
		}
	}

	Log::Error("Error, unknown input element type!");
	return DXGI_FORMAT_UNKNOWN;
}


void VertexShader::Release() {
	shader->Release();
	layout->Release();
}


void VertexShader::Apply(Graphics* graphics) const  {
	graphics->Context->VSSetShader(shader, nullptr, 0);
	graphics->Context->IASetInputLayout(layout);
}