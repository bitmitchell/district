#pragma once
#include "../Includes.hpp"
#include "../Application/Blob.hpp"

class Graphics;

class PixelShader {
	ID3D11PixelShader* shader;

	static std::map<std::string, PixelShader*> cache;

	void Release();

public:

	static PixelShader* Create(Graphics* graphics, const std::string& compiledShaderFile);
	static void ReleaseCache();

	void Apply(Graphics* graphics) const;
};