District model data formats: .dmf and .bdm

Contains:
	-	List of unique verticies in the model
	-	List of indicies that makeup the model

DMF File: (District Model Format)

YAML descriptor

version: DMF0
verts:
	- vert0
	- vert1
	- vert2
tris:
	- tri0
	- tri1
	- tri2
	
vert = { pos: [x, y, z], nor: [x, y, z], uv: [x, y] }
tri = [a, b, c]


BDM File: (Binary Distric Model)

uint16(4)
string(BDM0)

uint16 numberOfVerts
[for each vert]
float x, y, z, nx, ny, nz, tx, ty
uint16 numberOfFaces
[for each face]
uint16 a, b, c